#include "stm32f10x.h"
#include "./sdio/bsp_sdio_sdcard.h"
#include "ff.h"
#include "bsp_ili9341_lcd.h"


/**
  ******************************************************************************
  *                              定义变量
  ******************************************************************************
  */
unsigned char name[9]={"/new.bin"};

extern  SD_CardInfo SDCardInfo;
	

int main(void)
{
	
	ILI9341_Init ();         //LCD 初始化
	
	ILI9341_GramScan(3);
	
	ILI9341_OpenWindow ( 0,0,320,240 );
	ILI9341_Write_Cmd ( CMD_SetPixel );	
  
	Lcd_show_bin(name);
}

	  

