/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "spi_sd.h"
/* Definitions of physical drive number for each drive */

DWORD get_fattime (void)
{
    return 0;
}
/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;

	switch (pdrv) {
	case SD_V2_HC:
		stat = SD_Init();
		break;
	default:stat=1;

	}
	if(stat) return STA_NOINIT;
	else return 0;
	
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	u8 res=0;
	if(!count)return RES_PARERR;  //count 不能等于 0，否则返回参数错误
	switch (pdrv) {
	
	case SD_V2_HC:
		   
		res = SD_ReadDisk(buff,sector,count);

		while(res)	//读出错
		{
			SD_Init();
			res=SD_ReadDisk(buff,sector,count);		
		}
		break;
	default:res=1;
	}
 	if(res==0x00)return RES_OK;
 	else return RES_ERROR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	u8 res=0;
	if(!count)return RES_PARERR; //count 不能等于 0，否则返回参数错误

	switch (pdrv) {
	case SD_V2_HC:
		   
		res=SD_WriteDisk((u8*)buff,sector,count);
		while(res)	//写出错
		{
			SD_Init();
			SD_WriteDisk((u8*)buff,sector,count);		
		}
		break;
	default:res=1;
	}
	if(res == 0x00)return RES_OK;
	else return RES_ERROR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;


	switch (pdrv) {
	case SD_V2_HC:
		switch(cmd)
		{
			case CTRL_SYNC:
				GPIO_ResetBits(GPIOB,SPI2_NSS);
				if(SD_WaitReady()==0)res = RES_OK;
				else res = RES_ERROR;
				GPIO_SetBits(GPIOB,SPI2_NSS);
			 	break;
			case GET_SECTOR_SIZE:
				*(WORD*)buff = 512;
				res = RES_OK;
				break;
			case GET_BLOCK_SIZE:
				*(WORD*)buff = 8;
				res = RES_OK;
				break;
			case GET_SECTOR_COUNT:
				*(DWORD*)buff = SD_GetSectorCount();
				res = RES_OK;
				break;
			default:
				res = RES_PARERR;
				break;
		}

	}
	return res;
}

