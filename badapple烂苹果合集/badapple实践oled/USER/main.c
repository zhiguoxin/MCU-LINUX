#include "stm32f10x.h"
#include "systick.h"
#include "stdio.h"
#include "spi_sd.h"
#include "diskio.h"
#include "usart.h"
#include "string.h"
#include "oled.h"
#include "timer.h"
#include "key.h"
//用到了文件系统和spi读SD卡的东西，读图片在定时器中断里面
extern u8 sdtype;

BYTE work[4096];   //不懂为什么这个必须放到main之外，否则程序错误

int main(void)
{
	FATFS fs;    //文件系统结构体
	extern FIL fp;	     //文件指针结构体
	
	FRESULT res; //检测状态，可以看它的定义得到错误类型
	u8 device=0;	 //检测sd卡
	extern int i;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	TIM_Int_Init(59993,39);	  //应该是最接近30帧的了，0.03333*72MHZ
	SysTickInit();
	OLED_Init();
	USART_Config();
	LED_Init();
	device=disk_initialize(SD_V2_HC);

	if(device==0) {printf("ok");}
	else {printf("no");}

	res=f_mount(&fs,"1:",0);	//挂载SD卡为第二个fatfs文件系统(前面还有0:)
	if(res==FR_NO_FILESYSTEM)  //文件系统没挂载成功
    {
      	printf("\r\n-------SD卡格式化中------:\r\n");
		res=f_mkfs("1:",0,work,sizeof work);	//用默认参数格式化文件系统
		if(res!= FR_OK)printf("f_mkfs_error:%d\r\n",res); 
		else
		{
			res=f_mount(&fs,"1:",1);   //尝试重新挂载
			printf("\r\n-------SD卡初始化完成------:\r\n");
		}
    }
	else{printf("\r\n-------SD卡初始化完成------:\r\n");}

	OLED_Clear();
	OLED_ShowString(10,1,"Bad Apple Test",16);


	while(1)
	{
		if(KEY_Scan()==1)
		{
			res=f_open(&fp,"1:test/1.bin",FA_READ|FA_OPEN_ALWAYS);
			TIM_Cmd(TIM3,ENABLE);
			OLED_Clear();
			while(i);
			f_close(&fp);
		}	
	}

}

