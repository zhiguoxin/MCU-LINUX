#!/bin/bash

echo "1.取图片帧 2.数组文件合并"
read choice

if [ $choice -eq 1 ];then
	tmp=$(dpkg -l|grep "ffmpeg")
        if [ ! $? -eq 0 ];then
                echo "请先安装需要的ffmpeg工具！"
        else
	       	echo "输入文件名(加上后缀):"
                read name
		mkdir ${name%.*}
		ffmpeg -i $name -q:v 2 -s 85x64 -r 30 ${name%.*}/%d.bmp -f image2
		ffmpeg -i $name -vn ${name%.*}.mp3
	fi

elif [ $choice = "2" ];then
	path=$(pwd)

	cd batch
	num=$(ls -l|grep "bin"|wc -l)
	echo $num

	touch aaa.bin
	for i in $( seq 1 $num)
	do
		cat $i.bin >> aaa.bin
	
	done
	echo "合并完成"
	cd ..
	mv batch/aaa.bin $path/aaa.bin

	#sed -i 's/const.*/s/g' badapple.txt
	#sed -i 's/};//g' badapple.txt

else
	echo "输入错误"
fi
