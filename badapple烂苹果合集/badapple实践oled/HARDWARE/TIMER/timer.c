#include "stm32f10x.h"
#include "timer.h"
#include "oled.h"
#include "diskio.h"

void TIM_Int_Init(u16 arr,u16 psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	//使能TIM3时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	
	//初始化TIM定时器，主要关心前三个参数
	TIM_TimeBaseInitStruct.TIM_Prescaler=psc;   //预分频系数
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;   //向上计数
	TIM_TimeBaseInitStruct.TIM_Period=arr;     //自动预装载值
	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1; //对定时器的时钟进行预分频,分频以后的时钟送定时器时钟输入,目的就是达到更长定时的效果,这里直接默认1

	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStruct);
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);    //使能中断（TIM3，注意是中断不是使能定时器）,模式是更新中断
	
	//初始化NVIC优先级分组
	NVIC_InitStruct.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;

	NVIC_Init(&NVIC_InitStruct);
	
	//使能定时器
	//TIM_Cmd(TIM3,ENABLE);	
}
unsigned char read_buf[680]="";
UINT br;
FIL fp;
int i=6574;
void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==SET) //其实这个判断更新中断的标志是否位为1不是必要的
	{											 //因为每隔一段时间（自定义）系统都会进入该中断函数
		f_read(&fp,read_buf,680,&br);
		OLED_DrawBMP(20,0,105,8,read_buf);
		i--;
				
		TIM_ClearITPendingBit(TIM3,TIM_IT_Update); //手动清除TIM3更新中断的标志
	}	
}

