#ifndef __SPI_SD_H
#define __SPI_SD_H

#include "stm32f10x.h"
void SPI2_Init(void);
u8 SPI_ReadWrite_Byte(u8 byte);
u8 SD_Init(void);
void SD_DisSelect(void);
u8 SD_Select(void);
u8 SD_WaitReady(void);
u8 SD_SendCmd(u8 cmd,u32 arg,u8 crc);
void SPI2_SetSpeed(u8 SPI_BaudRatePrescaler);
u8 SD_ReadDisk(u8*buff,u32 sector,u8 cnt);
u8 SD_GetResponse(u8 Response);
u8 SD_RecvData(u8*buff,u16 len);
u8 SD_SendBlock(u8*buff,u8 cmd);
u8 SD_GetCSD(u8 *csd_data);
u32 SD_GetSectorCount(void);
u8 SD_WriteDisk(u8*buff,u32 sector,u8 cnt);

#define SD_V1orMMC_V3 0x00
#define SD_V2_HC 0x01
#define SD_V2_STANDAR 0x02
#define SD_V1 0x03
#define SD_MMC 0x04
#define SD_ERR 0x05

#define CMD0 0   //����λ
#define CMD1 1
#define CMD8 8   //���� 8 ��SEND_IF_COND
#define CMD9 9   //���� 9 ���� CSD ����
#define CMD10 10 //���� 10���� CID ����
#define CMD12 12 //���� 12��ֹͣ���ݴ���
#define CMD16 16 //���� 16������ SectorSize Ӧ���� 0x00
#define CMD17 17 //���� 17���� sector
#define CMD18 18 //���� 18���� Multi sector
#define CMD23 23 //���� 23�����ö� sector д��ǰԤ�Ȳ��� N �� block
#define CMD24 24 //���� 24��д sector
#define CMD25 25 //���� 25��д Multi sector
#define CMD41 41 //���� 41��Ӧ���� 0x00
#define CMD55 55 //���� 55��Ӧ���� 0x01
#define CMD58 58 //���� 58���� OCR ��Ϣ
#define CMD59 59 //���� 59��ʹ��/��ֹ CRC��Ӧ���� 0x00

#define SPI2_NSS    GPIO_Pin_12
#define SPI2_CLK    GPIO_Pin_13
#define SPI2_MISO   GPIO_Pin_14
#define SPI2_MOSI   GPIO_Pin_15

#endif
