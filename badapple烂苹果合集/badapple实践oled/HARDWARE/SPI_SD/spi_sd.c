#include "stm32f10x.h"
#include "spi_sd.h"

u8 sdtype=0;


u8 SD_Init(void)
{
	u8 i;	
	u16 retry;
	u8 r1;
	u8 buff[4];
	u8 device=1;

	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);	

	GPIO_InitStruct.GPIO_Pin=SPI2_NSS;	   //片选
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	
	SPI2_Init();	//SPI2初始化函数（在下面）

	
	GPIO_SetBits(GPIOB,SPI2_NSS);  //先不选中SD卡

	//然后上电延迟74个时钟
	for(i=0;i<10;i++)
	{
		SPI_ReadWrite_Byte(0XFF);	//发送一个数据需要8个时钟，8*10=80>74
	}
	
	//发送CMD0指令，尝试进入idle状态
	retry=20;
	do
	{
		r1=SD_SendCmd(CMD0,0,0x95);  //进入idle状态（spi模式）
	}while((r1!=0x01)&&retry--); //若SD卡进入该状态（spi模式），r1被返回0x01，证明检测到SD卡	
	
	if(r1==0x01)   //检测到SD卡
	{
		device=0;

		if(SD_SendCmd(CMD8,0x1AA,0x87)==1)   //只有SD V2.0才能用cmd8,别问，问就是固定参数
		{									 //注意执行了cmd8会返回R7，而上面判断默认已经
			//SD V2.0						//读过了R7包含的R1，只需再读后面的四个字节即可			
			for(i=0;i<4;i++)
			{
				buff[i]=SPI_ReadWrite_Byte(0XFF); //用四个oxff交换四个字节	
			}
			if(buff[2]==0x01&&buff[3]==0xAA)  //看是不是返回0x1AA，是否支持2.7~3.6v,具体看图
			{
				retry=0xFFFE;
				do
				{
					SD_SendCmd(CMD55,0,0x01);	 //cmd5表示下一条是应用指令
					r1=SD_SendCmd(CMD41,0x40000000,0x01); //在发送ACMD41命令时必须将HCS位置1,数字足够大就能表示1了
				}while(r1&&retry--);	//r1为0是表示空闲状态，表示已经成功执行完成了cmd41指令
				
				if(retry&&SD_SendCmd(CMD58,0,0x01)==0)	//r1为0,表示接收了该命令
				{
					for(i=0;i<4;i++)
					{
						buff[i]=SPI_ReadWrite_Byte(0XFF); //得到OCR的值	
					}
					if(buff[0]&0x40) sdtype=SD_V2_HC; //OCR第30位为1，表示高容量卡
					else sdtype=SD_V2_STANDAR;  //否则为标准SD卡		
				}

			}
		}
		else  //SD V1.x/MMC V3
		{
			SD_SendCmd(CMD55,0,0X01); //发送 CMD55
			r1=SD_SendCmd(CMD41,0,0X01); //发送 CMD41
			
			if(r1<=1)
			{
				sdtype=SD_V1;
				retry=0XFFFE;
				do //等待退出 IDLE 模式
				{
					SD_SendCmd(CMD55,0,0X01); //发送 CMD55
					r1=SD_SendCmd(CMD41,0,0X01);//发送 CMD41
				}while(r1&&retry--);
			}
			else//MMC 卡不支持 CMD55+CMD41 识别
			{
				sdtype=SD_MMC;//MMC V3
				retry=0XFFFE;
				do //等待退出 IDLE 模式
				{
					r1=SD_SendCmd(CMD1,0,0X01);//发送 CMD1
				}while(r1&&retry--); 
			}

			if(retry==0||SD_SendCmd(CMD16,512,0X01)!=0)sdtype=SD_ERR; //设置块大小512字节，如果不行表示卡错误
		}
	}
	
	SD_DisSelect();  //取消片选
	SPI2_SetSpeed(SPI_BaudRatePrescaler_4);	//设置高速
	return device;
}

//发送cmd命令函数
u8 SD_SendCmd(u8 cmd,u32 arg,u8 crc)   //cmd命令，参数，校验和（必不可少）
{
	u8 r1;
	u8 Retry=0;
	SD_DisSelect();	 //先取消SD卡的片选
	if(SD_Select()) return 0XFF;	//进行片选，1表示片选失败，返回ri=0xff
	
	//片选成功,开始发送，命令格式为cmd+参数+crc
	SPI_ReadWrite_Byte(cmd|0x40); //cmd指令只有6位有效
	SPI_ReadWrite_Byte(arg>>24);  //32位参数分4次发送，从高位到低位，每次8位以上的bit都被忽略了
    SPI_ReadWrite_Byte(arg>>16);
	SPI_ReadWrite_Byte(arg>>8);
	SPI_ReadWrite_Byte(arg);
	SPI_ReadWrite_Byte(crc);

	if(cmd==CMD12) SPI_ReadWrite_Byte(0xff); //具体作用不明，针对CMD12命令（强制结束数据传输命令）
	
	//发送该cmd0后正常sd卡移位寄存器回应0x01
	Retry=0x1F;		//尝试次数
	do
	{
		r1=SPI_ReadWrite_Byte(0xFF);
	}while((r1&0x80)&&Retry--);		//等待返回非0XFF的数据
	return r1;
}

//取消片选
void SD_DisSelect(void)
{
	GPIO_SetBits(GPIOB,SPI2_NSS);
	SPI_ReadWrite_Byte(0XFF);	//发送8个时钟
}

u8 SD_Select(void)
{
	GPIO_ResetBits(GPIOB,SPI2_NSS);
	if(SD_WaitReady()==0) return 0;	 //片选成功
	//片选失败，返回1
	SD_DisSelect();
	return 1;
}

u8 SD_WaitReady(void)
{
	u32 t=0;
	do
	{
		if(SPI_ReadWrite_Byte(0xFF)==0XFF) return 0;
	}while(t<0xFFFFFF);
	return 1;
}

u8 SD_ReadDisk(u8*buff,u32 sector,u8 cnt)   //数据缓存区，扇区地址，扇区数（u8最多255）
{
	u8 r1;
	if(sdtype!=SD_V2_HC) sector<<=9; //扇区转换为字节地址,从block->byte,非高容量卡是以字节作为读写单位的,左移9位等于*512
									 //高容量SD卡读写块大小固定为512字节
	if(cnt==1) //单个扇区读
	{	
		r1=SD_SendCmd(CMD17,sector,0x01);
		if(r1==0) //指令发送成功
		{
			r1=SD_RecvData(buff,512);  //读512个字节	
		}
	}
	else   //多个扇区读
	{
		 r1=SD_SendCmd(CMD18,sector,0x01);
		 if(r1==0) //指令发送成功
		 {
			 do
			 {
			 	r1=SD_RecvData(buff,512);
				buff+=512; //指针类型
			 }while(--cnt&&r1==0);
			 SD_SendCmd(CMD12,0,0x01); //发送停止传输命令	
		 }

	}
	SD_DisSelect(); //取消片选
	return r1;		
}

u8 SD_RecvData(u8*buff,u16 len)
{
	if(SD_GetResponse(0xFE)) return 1;  //SD卡处理命令需要一些时间，SD卡处理命令时SPI读得0xff，需要判断接受到0xfe时才将数据写进buff
	//回应成功							//回应失败返回1
	while(len--)
	{
		*buff=SPI_ReadWrite_Byte(0xFF);
		buff++;
	}
	//2个伪CRC
	SPI_ReadWrite_Byte(0xFF);
	SPI_ReadWrite_Byte(0xFF);
	return 0;	//读取成功
}

u8 SD_GetResponse(u8 Response)
{
	u16 count=0xFFFF;
	while(SPI_ReadWrite_Byte(0xFF)!=0xFE&&count)count--; //等待回应或计时结束
	if(count==0) return 1;	//回应失败
	else return 0;		   //回应成功
}

u8 SD_WriteDisk(u8*buff,u32 sector,u8 cnt)   //数据缓存区，扇区地址，扇区数（u8最多255）
{
	u8 r1;
	if(sdtype!=SD_V2_HC) sector<<=9; //扇区转换为字节地址,从block->byte,非高容量卡是以字节作为读写单位的,左移9位等于*512
									 //高容量SD卡读写块大小固定为512字节
	if(cnt==1) //单个扇区写
	{	
		r1=SD_SendCmd(CMD24,sector,0x01);	//发送写扇区命令
		if(r1==0) //指令发送成功
		{
			r1=SD_SendBlock(buff,0xFE);  //发送写数据起始令牌	
		}
	}
	else   //多个扇区写
	{
		 if(sdtype!=SD_MMC)
		 {
		 	SD_SendCmd(CMD55,0,0x01);	  //acmd指令发送前需发送cmd55指令
			SD_SendCmd(CMD23,cnt,0x01);	  //不是mmc卡可以预先擦除数据块，提高写入速度
		 }
		 r1=SD_SendCmd(CMD25,sector,0x01);
		 if(r1==0) //指令发送成功
		 {
			 do
			 {
			 	r1=SD_SendBlock(buff,0xFC);	 //连续写数据起始令牌
				buff+=512; //指针类型
			 }while(--cnt&&r1==0);
			 r1=SD_SendBlock(0,0xFD); //发送停止传输命令	
		 }

	}
	SD_DisSelect(); //取消片选
	return r1;		
}

u8 SD_SendBlock(u8*buff,u8 cmd)
{
	u16 t;
	if(SD_WaitReady()) return 1;  //失败返回1
	//回应成功
	SPI_ReadWrite_Byte(cmd);						
	if(cmd!=0xFD) //不是结束命令
	{
		for(t=0;t<512;t++)SPI_ReadWrite_Byte(buff[t]);
			//2个伪CRC
			SPI_ReadWrite_Byte(0xFF);
			SPI_ReadWrite_Byte(0xFF);
		t=SPI_ReadWrite_Byte(0xFF); //接收响应
		if((t&0x1F)!=0x05) return 2; //响应失败，具体看协议		 
	}
	return 0;	//写成功
}

//获取扇区数函数
u32 SD_GetSectorCount(void)
{
	u8 csd[16];
	u32 Capacity=0;
	u8 n;
	u16 csize;
	if(SD_GetCSD(csd)!=0) return 0;
	if((csd[0]&0xC0)==0x40)  //V2.00的卡
	{
		csize=csd[9]+((u16)csd[8]<<8)+1;  
		Capacity=(u32)csize<<10;  
	}
	else   //V1.XX的卡
	{
		n=(csd[5]&15)+((csd[10]&125)>>7)+((csd[9]&3)<<1)+2;
		csize=(csd[8]>>6)+((u16)csd[7]<<2)+((u16)(csd[6]&3)<<10)+1;
		Capacity=(u32)csize<<(n-9);
	}
	return Capacity;
}

u8 SD_GetCSD(u8 *csd_data)
{
	u8 r1;
	r1=SD_SendCmd(CMD9,0,0x01);	 //读CSD命令
	if(r1==0)
	{
		r1=SD_RecvData(csd_data,16); //接收16个字节
	}
	SD_DisSelect();
	if(r1) return 1;
	else return 0;
}










void SPI2_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_InitTypeDef SPI_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2,ENABLE);

	GPIO_InitStruct.GPIO_Pin=SPI2_CLK|SPI2_MISO|SPI2_MOSI;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB,&GPIO_InitStruct);

	GPIO_SetBits(GPIOB,SPI2_CLK|SPI2_MISO|SPI2_MOSI);

	SPI_InitStruct.SPI_Direction=SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode=SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize=SPI_DataSize_8b;
	SPI_InitStruct.SPI_CPOL=SPI_CPOL_High;
	SPI_InitStruct.SPI_CPHA=SPI_CPHA_2Edge;
	SPI_InitStruct.SPI_NSS=SPI_NSS_Soft;
	SPI_InitStruct.SPI_BaudRatePrescaler=SPI_BaudRatePrescaler_256;	  //初始化时钟不能大于400K：72M/256<400K
	SPI_InitStruct.SPI_FirstBit=SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_CRCPolynomial=7;
	
	SPI_Init(SPI2,&SPI_InitStruct);

	SPI_Cmd(SPI2,ENABLE);
}

u8 SPI_ReadWrite_Byte(u8 byte)
{
	while(SPI_I2S_GetFlagStatus(SPI2,SPI_I2S_FLAG_TXE)==RESET);

	SPI_I2S_SendData(SPI2,byte);

	while (SPI_I2S_GetFlagStatus(SPI2,SPI_I2S_FLAG_RXNE)==RESET);

	return SPI_I2S_ReceiveData(SPI2);
}
//设置波特率分频系数以设置速度
void SPI2_SetSpeed(u8 SPI_BaudRatePrescaler)
{
	SPI2->CR1&=0XFFC7;
	SPI2->CR1|=SPI_BaudRatePrescaler;	
	SPI_Cmd(SPI2,ENABLE); 
}
//SPI_BaudRatePrescaler_2     
//SPI_BaudRatePrescaler_8      
//SPI_BaudRatePrescaler_16   
//SPI_BaudRatePrescaler_256
