#include "stm32f10x.h"
#include "exti.h"
#include "systick.h"
#include "lcd.h"
#include "led.h"

void Key_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_8;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_10MHz;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
		
}

void EXTIx_Init(void)
{	
	EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	Key_Init();	//配置好按键
	//开启IO口复用时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	//配置io口与中断线的映射
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource0);
	

	//初始化EXTI
	//EXTI0
	EXTI_InitStruct.EXTI_Line=EXTI_Line0;	//PX0对应EXTI0
	EXTI_InitStruct.EXTI_Mode=EXTI_Mode_Interrupt;	   //模式为中断
	EXTI_InitStruct.EXTI_Trigger=EXTI_Trigger_Rising;  //上升沿触发
	EXTI_InitStruct.EXTI_LineCmd=ENABLE;			   //使能
	EXTI_Init(&EXTI_InitStruct);
	//EXTI8
	EXTI_InitStruct.EXTI_Line=EXTI_Line8;
	EXTI_Init(&EXTI_InitStruct);
	

	//配置NViC分组，中断优先级
	//EXTI0
	NVIC_InitStruct.NVIC_IRQChannel=EXTI0_IRQn;   //中断的通道，在stm32f10x.h的180+行有定义
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;			//抢占优先级
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=1;					//相应优先级
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;	   //开启中断通道
	NVIC_Init(&NVIC_InitStruct);
	//EXTI8
	NVIC_InitStruct.NVIC_IRQChannel=EXTI9_5_IRQn;   //EXTI8不存在，pin8用的是EXTI9_5这个中断
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;			
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=0;			
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;	   
	NVIC_Init(&NVIC_InitStruct);
			
}

//在md启动文件里定义了EXTI0的中断函数
void EXTI0_IRQHandler(void)
{
	//这里不要加防抖（delay函数），否则中断无法执行
	if(EXTI_GetITStatus(EXTI_Line0)!=0) //pin0引脚外部中断状态，1中断开启
	{									//也可以用GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)==0(按键输入)
		LCD_SetBackColor(Cyan);
		LCD_DisplayStringLine(Line1,"B1 pressed");
		//需要手动清除中断标志位,否则下次不会进入中断
		EXTI_ClearITPendingBit(EXTI_Line0);
	}

}

void EXTI9_5_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line8)!=0)
	{
		LCD_SetBackColor(Cyan);
		LCD_DisplayStringLine(Line1,"B2 pressed");
		EXTI_ClearITPendingBit(EXTI_Line8);
	}	
}

