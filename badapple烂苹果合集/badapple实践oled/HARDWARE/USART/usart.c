#include "stm32f10x.h"
#include "usart.h"
#include "stdio.h"

//以下代码是为了能实现printf函数
#if 1
#pragma import(__use_no_semihosting)             
               
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
   
_sys_exit(int x) 
{ 
	x = x; 
} 

int fputc(int ch, FILE *f)
{      
	while((USART2->SR&0X40)==0);   
    USART2->DR = (u8) ch;      
	return ch;
}
#endif
//以上代码是为了能实现printf函数

void USART_Config(void)
{	
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	//使能USART2串口时钟，CT117E板子的USB线对应可直接在PC使用的串口是USART2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	//使能GPIOA（USART2串口发送和接受对应PA2、PA3）
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);

	//USART_TX(发送端)
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_2;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AF_PP; //复用推挽输出：可以理解为GPIO口被用作第二功能时的配置情况（即并非作为通用IO口使用）
	GPIO_Init(GPIOA,&GPIO_InitStruct);

	//USART_RX(接收端)
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_IN_FLOATING;//浮空输入，IO的电平状态是不确定的，完全由外部输入决定
	GPIO_Init(GPIOA,&GPIO_InitStruct);

	//TX和RX配置
	USART_InitStruct.USART_BaudRate=460800;		//波特率大小
	USART_InitStruct.USART_WordLength=USART_WordLength_8b;		  //字长大小
	USART_InitStruct.USART_StopBits=USART_StopBits_1;			//1位停止位
	USART_InitStruct.USART_Parity=USART_Parity_No;			   //无奇偶校验
	USART_InitStruct.USART_Mode=USART_Mode_Tx | USART_Mode_Rx;		   //使能接收端和发送端
	USART_InitStruct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;	  //无硬件数据流控制
	USART_Init(USART2,&USART_InitStruct);

	//使能usart2串口
	USART_Cmd(USART2,ENABLE);

	//开启USART中断(usart2串口的接收缓存区非空执行中断)
	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
	//设置USART中断
	NVIC_InitStruct.NVIC_IRQChannel=USART2_IRQn;   //中断的通道，在stm32f10x.h的180+行有定义
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;			//抢占优先级
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=1;					//相应优先级
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;	   //开启中断通道
	NVIC_Init(&NVIC_InitStruct);
}
//发送数据函数
void Send_string(int8_t *string)
{
	uint8_t index=0;
	do
	{
		USART_SendData(USART2,string[index]);
		while(USART_GetFlagStatus(USART2,USART_FLAG_TXE)==RESET);//TXE->发送数据寄存器标志位,表示此时该寄存器为空
		index++;												//或者(USART2,USART_FLAG_TXE)!=SET
		
	}
	while(string[index]!=0);	
}

u16 USART_RX_STA=0;
char USART_RX_BUF[USART_REC_LEN];


//中断符函数,在startup_stm32f10x_md.s启动文件里有定义,里面执行了串口传输数据的协议
void USART2_IRQHandler(void)
{
	u8 res;
	//判断是什么中断,USART2串口接收中断
	if(USART_GetITStatus(USART2,USART_IT_RXNE))
	{
		res=USART_ReceiveData(USART2);	//串口接收电脑发送的数据(res是1位1位的)
		
		//以下是串口协议
		//USART_RX_STA、USART_RX_BUF在上面定义，在头文件引用
		//USART_REC_LEN在头文件定义
		
		//USART_RX_STA是接收状态标志(在主函数定义，在头文件引用)，0~13位位数据的个数，14位是接收到回车符状态，15位是接收完成状态
		if((USART_RX_STA&0x8000)==0)   //接收状态标志15位为0表示接收未完成,如果接收完成（为1）										//表示接受到了0x0d和0x0a（回车符和换行符），就不用往下分析数据了
		{
			if(USART_RX_STA&0x4000)	   //接收状态标志14位为1表示接受到了0x0d（回车符）->上一位就收到的
			{
				if(res!=0x0a)USART_RX_STA=0;//这一位没接收到换行符，数据有误，重新开始
				else USART_RX_STA|=0x8000; //接收完成	
			}
			else	//没接收到0x0d
			{
				if(res==0x0d)USART_RX_STA|=0x4000;//这一位收到0x0d，接收状态标志14位设为1
				else
				{
					USART_RX_BUF[USART_RX_STA&0X3FFF]=res;	  //缓存区写入读到的数据
					USART_RX_STA++;		 //表示接收到数据个数+1
					if(USART_RX_STA>(USART_REC_LEN-1))USART_RX_STA=0;//接收到的数据个数超过了规定范围，重新开始
				}
					
			}

		}

	}
}
