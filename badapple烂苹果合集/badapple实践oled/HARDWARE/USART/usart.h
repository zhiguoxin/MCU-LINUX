#ifndef __USART_H
#define __USART_H
void USART_Config(void);
void Send_string(int8_t *string);
#define USART_REC_LEN	200

extern char  USART_RX_BUF[USART_REC_LEN];  
extern u16 USART_RX_STA;
#endif
