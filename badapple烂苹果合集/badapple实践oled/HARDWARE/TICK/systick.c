#include "stm32f10x.h"
#include "systick.h"

static __IO uint32_t TimingDelay;

void SysTickInit(void)
{
	SysTick_Config(SystemCoreClock / 1000);
}

void Delay_Ms(__IO uint32_t nTime)
{ 
  TimingDelay = nTime;

  while(TimingDelay != 0);
}

void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  { 
    TimingDelay--;
  }
}





