#ifndef _SYSTICK_H
#define _SYSTICK_H
#include "stm32f10x.h"

void SysTickInit(void);
void TimingDelay_Decrement(void);
void Delay_Ms(__IO uint32_t nTime);

#endif



