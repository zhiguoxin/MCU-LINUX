//
//  oled.h
//  electronic_clock
//  Copyright © 2018年 赛博智能车实验室. All rights reserved.
//

#ifndef oled_h
#define oled_h

//定义OLED的GPIO
#define	SCL_GPIO_RCC	RCC_APB2Periph_GPIOB
#define	SCL_GPIO_PORT	GPIOB
#define	SCL_GPIO_PIN	GPIO_Pin_12

#define	SDA_GPIO_RCC	RCC_APB2Periph_GPIOB
#define	SDA_GPIO_PORT	GPIOB
#define	SDA_GPIO_PIN	GPIO_Pin_13

#define	RES_GPIO_RCC	RCC_APB2Periph_GPIOB
#define	RES_GPIO_PORT	GPIOB
#define	RES_GPIO_PIN	GPIO_Pin_14

#define	DC_GPIO_RCC		RCC_APB2Periph_GPIOB
#define	DC_GPIO_PORT	GPIOB
#define	DC_GPIO_PIN		GPIO_Pin_15

#define OLED_GPIO_RCC 	SCL_GPIO_RCC|SDA_GPIO_RCC|RES_GPIO_RCC|DC_GPIO_RCC

//模式
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//OLED端口定义
//OLED的时钟引脚(SCL)
#define OLED_SCL_L() GPIO_ResetBits(SCL_GPIO_PORT,SCL_GPIO_PIN)
#define OLED_SCL_H() GPIO_SetBits(SCL_GPIO_PORT,SCL_GPIO_PIN)
//OLED的数据引脚(SDA)
#define OLED_SDA_L() GPIO_ResetBits(SDA_GPIO_PORT,SDA_GPIO_PIN)
#define OLED_SDA_H() GPIO_SetBits(SDA_GPIO_PORT,SDA_GPIO_PIN)
//OLED的复位引脚(RES)
#define OLED_RST_L() GPIO_ResetBits(RES_GPIO_PORT,RES_GPIO_PIN)
#define OLED_RST_H() GPIO_SetBits(RES_GPIO_PORT,RES_GPIO_PIN)
//OLED的数据/命令控制引脚(DC)
#define OLED_DC_L() GPIO_ResetBits(DC_GPIO_PORT,DC_GPIO_PIN)
#define OLED_DC_H() GPIO_SetBits(DC_GPIO_PORT,DC_GPIO_PIN)

//模式
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据


//函数声明
void OledCls(void);//清屏函数
void OledInit(void);//OLED初始化

void xy(long i,unsigned char *x,unsigned char *y);
void time(int t);
void badapple(unsigned char x, unsigned char y);

#endif
