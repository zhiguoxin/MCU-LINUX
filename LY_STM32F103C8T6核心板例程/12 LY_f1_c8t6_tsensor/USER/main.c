/*************************************************************************************
STM32F103有两个ADC  ADC1和ADC2
		通道0	通道1	通道2	通道3	通道4	通道5	通道6	通道7	通道8	通道9
ADC1     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
ADC2     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
**************************************************************************************/
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "oled.h"
#include "adc.h"
#include "tsensor.h"

int main(void)
{	
	u16 adcx;
	float temperate;	
	delay_init();//延时函数初始化	  
	uart_init(115200);//串口初始化为9600
	OLED_Init()	;
	OLED_Clear() ;
 	LED_Init();//LED端口初始化
	T_Adc_Init();//ADC初始化	 
	while(1)
	{		   
		adcx=T_Get_Adc_Average(ADC_CH_TEMP,10);
		temperate=(float)adcx*(3.3/4096);//保存温度传感器的电压值
		printf("ADC1_adcx=%dmV\t", adcx);		
		printf("temperate=%f\n", temperate);
		delay_s(1);
	}
}


