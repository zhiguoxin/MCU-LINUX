#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 

void uart_init(u32 bound);
void USART1_Send_byte(uint8_t val);
uint8_t USART1_Recv_byte(void);

#endif
