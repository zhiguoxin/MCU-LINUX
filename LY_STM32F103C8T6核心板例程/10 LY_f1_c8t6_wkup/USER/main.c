#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h" 
#include "wkup.h"

int main(void)
{	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
	uart_init(115200);
	delay_init();//延时函数初始化
	WKUP_Init();//初始化WK_UP按键，同时检测是否正常开机		    								    
	while(1)
	{								    
	}									    	
}


