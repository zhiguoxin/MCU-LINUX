#include "timer.h"

//定时器2通道1输入捕获配置
void TIM2_Input_Capture_Init(void)
{	 
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_ICInitTypeDef  TIM2_ICInitStructure;
 	NVIC_InitTypeDef NVIC_InitStructure;
	

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	//使能TIM2时钟
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);  //使能GPIOA时钟

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;//PA0 输入  
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOA,GPIO_Pin_0);//PA0下拉

	//初始化定时器2 TIM2	 
	TIM_TimeBaseStructure.TIM_Period = 0xffff; //当计数到65536个数时，产生一次中断，中断一次时间为65536us=65.536ms
	TIM_TimeBaseStructure.TIM_Prescaler =72-1; 	//预分频器  说白了就是计数器的频率 72/72=1M 则计数一次的时间为1us
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
  
/*
同时通过设置TIM2_Cap_Init(0XFFFF,72-1)，将 TIM2_CH1 的捕获计数器设计为 1us 计数一次，
并设置重装载值为最大，所以我们的捕获时间精度为 1us。既然1us计数一个，那么我能到一个
脉冲周期的计数个数，就知道了计数时间，比如两个上升沿之间的数时count个，那么周期既是
count(us)=count*0.000 000s 那么频率就是 1/count*0.000 000s =1000 000/count(HZ)
而count就是两个相邻上升沿之间的计数个数。

程序在TIM2定时器使能之后，也就是在执行完TIM2_Cap_Init();函数之后开始计数。1us计一个数，
计65535个数后才溢出，重新计数。假设两个相邻上升沿之间的计数为65535 那么输出来的频率就为1 000 000 / 65535= 16HZ。
也就是说 TIM2_Cap_Init(0XFFFF,72-1) 可以测得的最下频率为十几个HZ，一般来说实际不可能有这么低的待测波形，所以我们
设置重装载值为0xffff=65535是完全没有问题的，而且这个设置这个值可测的待测频率范围很大，这也就是为什么我们在这里
设置0xffff的原因。

另外我们这里设置分频系数为(72-1)的目的为了使计算更加简单，这里的计数器频率为1M，就是1us计数一个值，那我我们假设
待测频率的周期为1us，也就是1/1us=1000KHZ的频率我都可以测，这就大大的增加测量的范围。同时也容易计算。
*/

	TIM2_ICInitStructure.TIM_Channel = TIM_Channel_1; //选择输入端IC1映射到TI1上 有4个通道TIM_Channel_1/2/3/4
  	TIM2_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;//上升沿捕获
  	TIM2_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;//映射到TI1上
  	TIM2_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;//配置输入分频,不分频 
  	TIM2_ICInitStructure.TIM_ICFilter = 0x00;//IC1F=0000 配置输入滤波器不滤波
  	TIM_ICInit(TIM2, &TIM2_ICInitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;//TIM2中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;//先占优先级2级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;//从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;//IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);//根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器
	
	TIM_ITConfig(TIM2,TIM_IT_Update|TIM_IT_CC1,ENABLE);//允许更新中断和通道1CC1IE捕获中断		
    TIM_Cmd(TIM2,ENABLE); 	//使能定时器2
}
//方法1
uint16_t Edge_Flag,test;
uint16_t Rising_fisrt,Falling_first,Rising_second; 
uint32_t Capture,Htime,Ltime;
float TIM2Freq,Duty;

#if EN_method1
void TIM2_IRQHandler(void)
{
   if(TIM_GetITStatus(TIM2,TIM_IT_CC1)!=RESET)//判断TIM2的通道1是否发生捕获事件
   {
		if(Edge_Flag==0)    //捕获第一个上升沿
		{			
			Rising_fisrt=TIM_GetCapture1(TIM2); //记录第一次上升沿的CNT值
			Edge_Flag=1;
		}
	    else if(Edge_Flag==1)//捕获第二个上升沿
	    {
			Edge_Flag=0;
			Rising_second=TIM_GetCapture1(TIM2);      //记录第二次上升沿的CNT值
			if(Rising_fisrt<Rising_second)
			{
				test=Rising_second-Rising_fisrt;           //两次上升沿的差值
			}
			else if(Rising_fisrt>Rising_second)
			{
				test=(0xffff-Rising_fisrt)+Rising_second;  //两次上升沿的差值
			}
			else
				test=0;
	    }       
   }
   TIM_ClearITPendingBit(TIM2,TIM_IT_CC1|TIM_IT_Update); //清除TIM2更新中断和输入捕获中断的标志位    
}
#endif	

#if EN_method2
void TIM2_IRQHandler(void)
{	
	if(TIM_GetITStatus(TIM2,TIM_IT_CC1)!=RESET) //判断TIM2的通道1是否发生捕获事件
	{
		if(Edge_Flag==0)//第一次检测到上升沿
		{
			Rising_fisrt=TIM_GetCapture1(TIM2);  
			TIM_OC1PolarityConfig(TIM2,TIM_ICPolarity_Falling); //将上升沿触发改为下降沿触发
			Edge_Flag++;
		}
		else if(Edge_Flag==1)//第一次检测到下降沿
		{
			Falling_first=TIM_GetCapture1(TIM2);    
			TIM_OC1PolarityConfig(TIM2,TIM_ICPolarity_Rising);  //再次改为上升沿触发
			Edge_Flag++; 
		}
		else if(Edge_Flag==2)//第二次检测到下降沿
		{
			Rising_second=TIM_GetCapture1(TIM2);//第二次检测到上升沿
			TIM_OC1PolarityConfig(TIM2,TIM_ICPolarity_Rising);
			
			Capture= 1000000/(Rising_second - Rising_fisrt);//单位Hz
			TIM2Freq=Capture /1000;//单位KHz
			Duty=(float)(Falling_first-Rising_fisrt)/(Rising_second - Rising_fisrt)*100;
			Htime=(Falling_first-Rising_fisrt);
			Ltime=(Rising_second-Rising_fisrt);
			
			Edge_Flag=0;            //标志位清0
			TIM_SetCounter(TIM2,0); //定时器清0
		}    
	}
	TIM_ClearITPendingBit(TIM2,TIM_IT_CC1|TIM_IT_Update);//清除TIM2更新中断和输入捕获中断的标志位
}



#endif	

#if EN_method3
int Rising,Falling,Rising_Last; 
void TIM2_IRQHandler(void)
{                     
	if(TIM_GetITStatus(TIM2, TIM_IT_CC1)!= 0)//判断TIM2的通道1是否发生捕获事件
	{
		if(Edge_Flag==0)//上升沿
		{
			Rising=TIM_GetCapture1(TIM2)-Rising_Last;
			Rising_Last=TIM_GetCapture1(TIM2);
			TIM_OC1PolarityConfig(TIM2,TIM_ICPolarity_Falling); //CC1P=0 设置为上升沿捕获
			Edge_Flag=1;
		}
		else
		{
			Falling=TIM_GetCapture1(TIM2)-Rising_Last;
			TIM_OC1PolarityConfig(TIM2,TIM_ICPolarity_Rising); //CC1P=0 设置为上升沿捕获
			Edge_Flag=0;
		}
	}
	TIM_ClearITPendingBit(TIM2, TIM_IT_CC1|TIM_IT_Update);  //清除TIM2更新中断和输入捕获中断的标志位
}
#endif	