//实验现象：led每一秒闪烁一次
#include "sys.h"
#include "led.h"
#include "delay.h"

int main(void)
{	
	delay_init();
    LED_Init();	
    while(1)
	{	
	   LED=!LED;
	   delay_ms(500);
	}	
}


