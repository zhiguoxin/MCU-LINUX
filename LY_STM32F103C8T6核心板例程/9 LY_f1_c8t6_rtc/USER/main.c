/*
	1、STM32 的实时时钟（RTC）是一个独立的定时器。STM32 的 RTC 模块拥有一组连续计数的计数器，在相\应软件配置下，可提供时钟日历的功能。修改计数器的值可以重新设置系统当
前的时间和日期。

	2、RTC模块和时钟配置系统是在后备区域，即在系统复位或从待机模式唤醒后 RTC 的设置和时间维持不变。
但是在系统复位后，会自动禁止访问后备寄存器和 RTC，以防止对后备区域(BKP)的意外写操作。所以你要想操
作RTC模块的时间，在要设置时间之前， 先要取消备份区域（BKP）写保护。不然默认情况下，
他的值是不会改变的。

	3、RTC核心由一组可编程计数器组成， RTC的预分频模块+一个32位的可编程计数器
		3.1、RTC的预分频模块：包含了一个20位的可编程分频器(RTC 预分频器)，如果在RTC_CR寄存器中设置了
			相应的允许位，则在每个TR_CLK 周期中 RTC 产生一个中断(秒中断)
		3.2、一个32位的可编程计数器：可被初始化为当前的系统时间，一个 32 位的时钟计数器，按秒钟计算，
			可以记录 4294967296 秒，约合 136 年左右。	
	
	4、RTC 还有一个闹钟寄存器RTC_ALR，用于产生闹钟。RTC内核完全独立于RTC APB1 接口，而软件是通过APB1 
接口访问RTC的预分频值、计数器值和闹钟值的。

	5、备份寄存器是42个16位的寄存器（RCT6就是大容量的），可用来存储84个字节的用户应用程序数据。他们
处在备份域里，当VDD电源被切断，他们仍然由VBAT维持供电。即使系统在待机模式下被唤醒，或系统复位或电源
复位时，他们也不会被复位。

*/
#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h" 

int main(void)
{	
	u8 t;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
	uart_init(115200);
	delay_init();	    	 //延时函数初始化
	printf("RTC ERROR!\n");	
	while(RTC_Init())		//RTC初始化	，一定要初始化成功
	{ 
		printf("RTC ERROR!\n");	
		delay_ms(800);
		printf("RTC Trying...\n");	
	}		    								    
	while(1)
	{								    
		if(t!=calendar.sec)
		{
			t=calendar.sec;
			printf("%d年%d月%d日\t",calendar.w_year,calendar.w_month,calendar.w_date);	
			switch(calendar.week)
			{
				case 0:
					printf("Sunday\t");
					break;
				case 1:
					printf("Monday\t");
					break;
				case 2:
					printf("Tuesday\t");
					break;
				case 3:
					printf("Wednesday\t");
					break;
				case 4:
					printf("Thursday\t");
					break;
				case 5:
					printf("Friday\t");
					break;
				case 6:
					printf("Saturday\t");
					break;  
			}
			printf("%d时%d分%d秒\n\n",calendar.hour,calendar.min,calendar.sec);	
		}	
		delay_ms(1000);								  
	};  											    	
}


