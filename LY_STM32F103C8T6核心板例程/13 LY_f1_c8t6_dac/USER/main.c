/*************************************************************************************
STM32F103有两个ADC  ADC1和ADC2
		通道0	通道1	通道2	通道3	通道4	通道5	通道6	通道7	通道8	通道9
ADC1     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
ADC2     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
**************************************************************************************/
#include "key.h"
#include "led.h"
#include "sys.h"
#include "adc.h"
#include "dac.h"
#include "delay.h"
#include "usart.h"

int main(void)
{	
	u16 adcx;
	float temp;
 	u8 t=0;	 
	u16 dacval=0;
	u8 key;
	
	delay_init();//延时函数初始化	  
	uart_init(115200);//串口初始化为9600
	KEY_Init();	//按键初始化	
 	LED_Init();//LED端口初始化
	ADC1_Init();//ADC初始化
	Dac1_Init();//DAC通道1初始化		 
	while(1)
	{		   
		t++;
		key=KEY_Scan(0);			  
		if(key==WKUP_PRES)
		{		 
			if(dacval<4000)dacval+=200;
			DAC_SetChannel1Data(DAC_Align_12b_R, dacval);
		}else if(key==KEY0_PRES)	
		{
			if(dacval>200)dacval-=200;
			else dacval=0;
			DAC_SetChannel1Data(DAC_Align_12b_R, dacval);
		}	 
		if(t==10||key==KEY0_PRES||key==WKUP_PRES) 	//WKUP/KEY1按下了,或者定时时间到了
		{	  
 			adcx=DAC_GetDataOutputValue(DAC_Channel_1);
			printf("adc=%d\n",adcx);
			temp=(float)adcx*(3.3/4096);			//得到DAC电压值
			adcx=temp;
			printf("adc=%f\n",temp);
 			temp-=adcx;
			temp*=1000;
 			adcx=Get_Adc_Average(ADC_Channel_1,10);	//得到ADC转换值	  
			temp=(float)adcx*(3.3/4096);			//得到ADC电压值
			adcx=temp;
 			temp-=adcx;
			temp*=1000;
			LED=!LED;	   
			t=0;
		}	    
		delay_ms(10);
	}
}


