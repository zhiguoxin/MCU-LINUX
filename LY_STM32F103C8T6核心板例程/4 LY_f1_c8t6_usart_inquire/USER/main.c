// 串口1、2、3例程
// 实验现象：打开串口调试助手，选择正确的串口号，波特率115200
//           选择十六进制接收发送，或者文本模式接收发送，注意：
//           接收模式和发送模式必须一样。
//           发送区写入你要发送的字节，点击发送，就可以在接收区
//           收到一样的字节。
#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"

int main(void)
{	
	uart_init(115200);
    while(1)
	{	
		USART1_Send_byte(USART1_Recv_byte());
	}	
}


