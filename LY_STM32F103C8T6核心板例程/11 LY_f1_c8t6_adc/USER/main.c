/**************************************************************************
STM32F103有两个ADC  ADC1和ADC2
		通道0	通道1	通道2	通道3	通道4	通道5	通道6	通道7	通道8	通道9
ADC1     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
ADC2     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1

根据采样定理：采样频率要大于信号最高频率的2倍，才能无失真的保留信号的完整信息。
ADC转换就是输入模拟的信号量，单片机转换成数字量。读取数字量必须等转换完成后，完成一个通道的读取叫做采样周期。
	采样周期一般来说 = 转换时间 + 读取时间
	转换时间 = 采样时间 + 12.5个时钟周期。采样时间是通过寄存器告诉stm32采样模拟量的时间，设置越长越精确
	采样频率是可以设置固定值的,这里设置PLCK2为6分频，那么ADCCLK为 72M/6=12MHz。
	最大的采样周期是239.5个周期，那么最小采样频率：12M/(239.5+12.5)=12M/252=48KHz。
	对于ADC来说，采样频率越小，采样越精确。
本实验通过ADC1的通道0、1、2、3来采样外部电压。（电压值不能高于3.3V，所有的STM32芯片都一样）
**************************************************************************************/
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "oled.h"
#include "adc.h"

//mian函数中注释的函数是oled显示

int main(void)
{	
	
	u16 adc_A0,adc_A1,adc_A2,adc_A3;
	float adc_V_A0,adc_V_A1,adc_V_A2,adc_V_A3;
    u16 adc_mV_A0,adc_mV_A1,adc_mV_A2,adc_mV_A3;
	
//	char adcx_A0[80] = {0}; 
//  char adcx_A1[80] = {0}; 
//	char adcx_A2[80] = {0};
//	char adcx_A3[80] = {0};	
//	
//	char value_V_A0[80] = {0}; 
//  char value_V_A1[80] = {0}; 
//	char value_V_A2[80] = {0};
//	char value_V_A3[80] = {0};
//	
//	char value_mV_A0[80] = {0}; 
//  char value_mV_A1[80] = {0}; 
//	char value_mV_A2[80] = {0};
//	char value_mV_A3[80] = {0};
	
	delay_init();	//延时函数初始化	  
	uart_init(115200);//串口初始化为9600
	OLED_Init()	;
	OLED_Clear() ;
 	LED_Init();//LED端口初始化
	ADC1_Init();
	while(1)
	{	
	   
//		OLED_ShowString(0,0,"A0:",16);OLED_ShowString(75,0,"V:",16);
//      OLED_ShowString(0,2,"A1:",16);OLED_ShowString(75,2,"V:",16);
//      OLED_ShowString(0,4,":",16);OLED_ShowString(75,4,"V:",16);
//      OLED_ShowString(0,6,"A3:",16);OLED_ShowString(75,6,"V:",16);	
		
		adc_A0=Get_Adc_Average(0,5);
		adc_A1=Get_Adc_Average(1,5);		
		adc_A2=Get_Adc_Average(2,5);		
		adc_A3=Get_Adc_Average(3,5);
		
		adc_V_A0=Get_Adc_Value(0);
		adc_V_A1=Get_Adc_Value(1);		
		adc_V_A2=Get_Adc_Value(2);		
		adc_V_A3=Get_Adc_Value(3);
		
		adc_mV_A0=Get_Adc_Value_mV(0);
		adc_mV_A1=Get_Adc_Value_mV(1);		
		adc_mV_A2=Get_Adc_Value_mV(2);		
		adc_mV_A3=Get_Adc_Value_mV(3);	


		printf("ADC1_CH0=%dmV\t", adc_A0);		printf("ADC1_CH1=%dmV\t", adc_A1);		printf("ADC1_CH2=%dmV\t", adc_A2);		printf("ADC1_CH3=%dmV\n", adc_A3);
		printf("ADC1_CH0=%.2fV\t\t", adc_V_A0);	printf("ADC1_CH1=%.2fV\t\t", adc_V_A1);	printf("ADC1_CH2=%.2fV\t\t", adc_V_A2);	printf("ADC1_CH3=%.2fV\t\n", adc_V_A3);
		printf("ADC1_CH0=%dmV\t", adc_mV_A0);	printf("ADC1_CH1=%dmV\t", adc_mV_A1);	printf("ADC1_CH2=%dmV\t", adc_mV_A2);	printf("ADC1_CH3=%dmV\n", adc_mV_A3);
		
		
//		sprintf(adcx_A0,"%d", adc_A0); //整形转换成字符串	
//		sprintf(adcx_A1,"%d", adc_A1); //整形转换成字符串	
//      sprintf(adcx_A2,"%d", adc_A2); //整形转换成字符串	
//		sprintf(adcx_A3,"%d", adc_A3); //整形转换成字符串	
//		
//		sprintf(value_V_A0,"%0.2f", adc_V_A0); //浮点型转换成字符串	
//		sprintf(value_V_A1,"%0.2f", adc_V_A1); //浮点型转换成字符串	
//      sprintf(value_V_A2,"%0.2f", adc_V_A2); //浮点型转换成字符串	
//		sprintf(value_V_A3,"%0.2f", adc_V_A3); //浮点型转换成字符串
//		
//		sprintf(value_mV_A0,"%d", adc_mV_A0); //浮点型转换成字符串	
//		sprintf(value_mV_A1,"%d", adc_mV_A1); //浮点型转换成字符串	
//      sprintf(value_mV_A2,"%d", adc_mV_A2); //浮点型转换成字符串	
//		sprintf(value_mV_A3,"%d", adc_mV_A3); //浮点型转换成字符串
		
		
		
//		OLED_ShowString(30,0,(uint8_t *)adcx_A0,16);OLED_ShowString(90,0,(uint8_t *)value_V_A0,16);
//		OLED_ShowString(30,2,(uint8_t *)adcx_A1,16);OLED_ShowString(90,2,(uint8_t *)value_V_A1,16);
//	    OLED_ShowString(90,4,(uint8_t *)adcx_A2,16);OLED_ShowString(90,4,(uint8_t *)value_V_A2,16);
//		OLED_ShowString(30,6,(uint8_t *)adcx_A3,16);OLED_ShowString(90,6,(uint8_t *)value_V_A3,16);
		
		delay_s(1);

	}
}


