## 单片机嵌入式小仓库

### 一、将本地项目添加到gitee仓库的操作流程

* 1、先进入项目文件夹,通过命令 git init 把这个目录变成git可以管理的仓库 

```c++
git init
```

* 2、把文件添加到版本库中，使用命令 git add .添加到暂存区里面去 

```c++
git remote add origin https://gitee.com/xiaoshidigitee/MCU-LINUX.git
```

* 3、使用 git pull origin master 命令，将码云上的仓库pull到本地 

```c++
git pull origin master 
```

* 4、 将文件保存到缓存区 

```c++
git add . 
```

* 5、用命令 git commit告诉Git，把文件提交到仓库 

```c++
git commit -m "zhiguoxin"
```

* 6、 把本地库的内容推送到远程库

```c++
git push origin master
```
如果远程仓库和本地库的文件不一样，比如我在远程库增加了文件，而本地库没有则需要执行
```c++
git push --rebase origin master
```
之后在执行 git push origin master 命令即可
果果小师弟
