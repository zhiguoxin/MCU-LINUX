#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 
#include "wkup.h"
#include "rtc.h"

//注意：下载程序必须把key1按着，再点击LOAD下载程序，因为处于待机状态不能下载程序

void Messages(void);
int main(void)
{		
	delay_init();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	uart_init(115200);	
	LED_Init();
	WKUP_Init();
	LCD_Init();//LCD_Init初始化函数要放在WKUP_Init初始化函数之后，防止下载完程序后，屏幕会闪一下
	Messages();	
	while(1)
	{	
		time_show();
		delay_ms(1000);	
	}

}

void Messages(void)
{
	u8 lcd_id[12];//存放LCD ID字符串	
	while(RTC_Init())//RTC初始化，一定要初始化成功
	{ 
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC ERROR!   ");	
		delay_ms(800);
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC Trying...");	
	}	
	//显示提示信息
	POINT_COLOR=RED;
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Wkup Test");	
	LCD_ShowString(30,90,200,16,16,lcd_id);//显示LCD ID
	LCD_ShowString(10,150,200,16,16,(u8*)"Press key1 and then,downl");	
	LCD_ShowString(10,170,200,16,16,(u8*)"oad the program,otherwise");	
	LCD_ShowString(10,190,200,16,16,(u8*)"it will not be downloaded");	
	LCD_ShowString(10,210,200,16,16,(u8*)"while you are asleep!!!");	
	LCD_ShowString(10,300,200,16,16,(u8*)"Copyright@liuyao-blog.cn");	
	POINT_COLOR=BLUE;//设置字体为蓝色					 
	LCD_ShowString(0,0,200,16,16,(u8*)"  :  :  ");
}
