/*STM32的串口支持查询和中断两种方法
此处用的是中断的方法，主要是中断服务函数的编写，这里
有三种不同的中断服务函数的编写方法：正点原子、野火、计小新
注意：只有PC向单片机发送数据才会用到串口的的接收中断和中断服务函数，
stm32向pc打印数据不需要用到中断，直接应printf函数即可*/

#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"

int main(void)
{	
	u8 i,t,len;
	delay_init();//延时函数初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组2	
    LED_Init();	
	uart_init(115200);
    while(1)
	{	
		#if EN_ALIENTEK_USART1_IRQHandler
			if(USART_RX_STA&0x8000)
			{					   
				len=USART_RX_STA&0x3fff;//得到此次接收到的数据长度
				printf("\r\n您发送的消息为:\r\n");
				for(t=0;t<len;t++)
				{
					USART1->DR=USART_RX_BUF[t];
					while((USART1->SR&0X40)==0);//等待发送结束
				}
				printf("\r\n\r\n");//插入换行
				USART_RX_STA=0;
			}
		#endif
		
		#if EN_yeFire_USART1_IRQHandler 
			//野火的串口回显函数在中断服务函数中，所以在主函数的while函数中不写任何代码
		#endif
		
		#if EN_JiXiaoXin_USART1_IRQHandler
			if(ReceiveState==1)//如果接收到1帧数据
			{
				ReceiveState=0;
				i=0;
				while(RxCounter--)// 把接收到数据发送回串口
				{
					USART_SendData(USART1, aRxBuffer[i++]);	
					while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
				}
				RxCounter=0;
			}
		#endif		
	}	
}


