/*
	1、STM32 的实时时钟（RTC）是一个独立的定时器。STM32 的 RTC 模块拥有一组连续计数的计数器，在相\应软件配置下，可提供时钟日历的功能。修改计数器的值可以重新设置系统当
前的时间和日期。

	2、RTC模块和时钟配置系统是在后备区域，即在系统复位或从待机模式唤醒后 RTC 的设置和时间维持不变。
但是在系统复位后，会自动禁止访问后备寄存器和 RTC，以防止对后备区域(BKP)的意外写操作。所以你要想操
作RTC模块的时间，在要设置时间之前， 先要取消备份区域（BKP）写保护。不然默认情况下，
他的值是不会改变的。

	3、RTC核心由一组可编程计数器组成， RTC的预分频模块+一个32位的可编程计数器
		3.1、RTC的预分频模块：包含了一个20位的可编程分频器(RTC 预分频器)，如果在RTC_CR寄存器中设置了
			相应的允许位，则在每个TR_CLK 周期中 RTC 产生一个中断(秒中断)
		3.2、一个32位的可编程计数器：可被初始化为当前的系统时间，一个 32 位的时钟计数器，按秒钟计算，
			可以记录 4294967296 秒，约合 136 年左右。	
	
	4、RTC 还有一个闹钟寄存器RTC_ALR，用于产生闹钟。RTC内核完全独立于RTC APB1 接口，而软件是通过APB1 
接口访问RTC的预分频值、计数器值和闹钟值的。

	5、备份寄存器是42个16位的寄存器（RCT6就是大容量的），可用来存储84个字节的用户应用程序数据。他们
处在备份域里，当VDD电源被切断，他们仍然由VBAT维持供电。即使系统在待机模式下被唤醒，或系统复位或电源
复位时，他们也不会被复位。


注意；板子的时间已经确定了，如果要想修改时间。请把rtc.c文件中的101-102和133-140行注释掉，把正确的时
	  间填入125行的RTC_Set()中下载即可修改。下载完成后再去掉注释即可。

*/
#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h" 
#include "lcd.h"

void Messages(void);
int main(void)
{	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	delay_init();
	uart_init(115200);	
	LED_Init();	
 	LCD_Init();				 	
	RTC_Init();
	POINT_COLOR=RED;
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Rtc Test");
	//闹钟在此时刻加上10秒,也就是说10秒钟唤醒一次单片机。反正不管你单片机什么时候休眠，(你可以在代码的任何地方进行休眠)
	//但是只要你休眠了，10秒钟后就唤醒你。
	RTC_SetAlarm(RTC_GetCounter()+10-1); 
	RTC_WaitForLastTask();//等待最近一次对RTC寄存器的写操作完成
	while(1)
	{	
		POINT_COLOR=BLUE;
		LCD_ShowString(30,160,200,16,16,(u8*)"RTC register value:");	
		LCD_ShowNum(200,160,RTC_GetCounter(),2,16);			
		
		delay_ms(1000);//让屏幕亮5秒，用delay_ms();比用delay_s();更精确
		delay_ms(1000);
		delay_ms(1000);
		delay_ms(1000);
		delay_ms(1000);
		PWR_EnterSTANDBYMode();//进入待机模式。屏幕就会熄灭
					  
	}											    	
}
/*
实验现象：

打开单片机，屏幕亮5秒钟然后进入待机模式屏幕熄灭。5秒钟过后屏幕再一次亮起。
这里设置的是RTC_SetAlarm(RTC_GetCounter()+10); 但是确是屏幕熄灭后5秒钟就
亮了，因为屏幕点亮的5秒钟也计算再内。



*/

