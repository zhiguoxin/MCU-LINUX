#ifndef __RTC_H
#define __RTC_H	    

#include "sys.h" 

//使用外部时钟还是内部时钟（在rtc.h 文件定义）
//使用外部时钟32.768K晶振时，在有些情况下晶振不起振,批量产品的时候，很容易出现外部晶振不起振的情况，不太可靠
//但是我现在的这个板子外部晶振是好的，所以就用外部晶振了，使用条件编译，使用哪个就把哪个变为1

#define  USE_RCC_LSE    1 //使用外部32.768K晶振
#define  USE_RCC_LSI    0 //当外部晶振32.768k晶振出问题了使用内部8Mhz晶振
//时间结构体
typedef struct 
{
	uint8_t hour;
	uint8_t min;
	uint8_t sec;			
	//公历日月年周
	uint16_t w_year;
	uint8_t  w_month;
	uint8_t  w_date;
	uint8_t  week;		 
}_calendar_obj;					 
extern _calendar_obj calendar;	//日历结构体

u8 RTC_Init(void);        					//初始化RTC,返回0,失败;1,成功;
u8 RTC_Set(u16 syear,u8 smon,u8 sday,u8 hour,u8 min,u8 sec);//设置时间
u8 RTC_Get(void);         					//更新时间 


u8 Is_Leap_Year(u16 year);					//判断该年份是不是闰年.1,是.0,不是。在RTC_Set()函数中被调用
u8 RTC_Get_Week(u16 year,u8 month,u8 day);	//获得现在是星期几。在RTC_Get()函数中被调用


void time_show(void);//在主函数中显示时间
	
#endif


