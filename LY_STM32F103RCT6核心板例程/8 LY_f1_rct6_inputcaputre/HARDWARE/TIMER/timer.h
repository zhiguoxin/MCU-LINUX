#ifndef __TIMER_H
#define __TIMER_H
#include "stdio.h"	
#include "sys.h"

#define EN_method1 	0	
#define EN_method2 	1		
#define EN_method3	0		

extern uint16_t Edge_Flag,test;
extern uint16_t Rising_fisrt,Falling_first,Rising_second; 
extern uint32_t Capture,Htime,Ltime;
extern float TIM2Freq,Duty;

void TIM2_Input_Capture_Init(void);

#endif
