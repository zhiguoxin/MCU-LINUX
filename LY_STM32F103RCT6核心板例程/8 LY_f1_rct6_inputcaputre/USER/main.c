/* -----------------------------------------------------------------------
STM32F103C8T6一共有4个定时器，都是16位的。TIM1是高级定时器，TIM2、TIM3、TIM4
高级定时器
TIM1_CH1--PA8
TIM1_CH2--PA9
TIM1_CH3--PA10
TIM1_CH4--PA11

通用定时器
TIM2_CH1--PA0		TIM3_CH1--PA6		TIM4_CH1--PB6
TIM2_CH2--PA1		TIM3_CH2--PA7		TIM4_CH2--PB7
TIM2_CH3--PA2		TIM3_CH3--PB0		TIM4_CH3--PB8
TIM2_CH4--PA3		TIM3_CH4--PB1		TIM4_CH4--PB9

本实验利用
TIM1的通道1用来产生频率 即PA8引脚
TIM2的通道1用来测量频率 即PA0引脚

TIM2的PA0引脚连接到PA8 TIM2就把TIM1的频率测到了 然后发送到串口显示

TIM1输出频率 = TIM1 counter clock/(ARR + 1) = 72 MHz / 7200 = 10KHz
----------------------------------------------------------------------- */

#include "sys.h"
#include "led.h"
#include "pwm.h"
#include "delay.h"
#include "usart.h"
#include "timer.h"
#include "rtc.h"
#include "lcd.h"

void time_show(void);
int main(void)
{	
	u8 lcd_id[12];			//存放LCD ID字符串	
	delay_init();//延时函数初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组2	
	uart_init(115200);
	LCD_Init();		
	
	while(RTC_Init())//RTC初始化，一定要初始化成功
	{ 
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC ERROR!   ");	
		delay_ms(800);
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC Trying...");	
	}
	
	//显示提示信息
	POINT_COLOR=RED;
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Freq Test");	
	LCD_ShowString(30,90,200,16,16,lcd_id);//显示LCD ID
	LCD_ShowString(10,300,200,16,16,(u8*)"Copyright@liuyao-blog.cn");	
	POINT_COLOR=BLUE;//设置字体为蓝色					 
	LCD_ShowString(0,0,200,16,16,(u8*)"  :  :  ");	
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(30,130,200,16,16,(u8*)"Freq:    KHz");	      
	LCD_ShowString(30,150,200,16,16,(u8*)"Duty:    %");
	LCD_ShowString(30,170,200,16,16,(u8*)"Htime:   us");	
	LCD_ShowString(30,190,200,16,16,(u8*)"Ltime:   us");	
	
	TIM2_Input_Capture_Init();//定时器输入捕获测频率
 	TIM1_Output_PWM_Init(3600-1,1-1);//不分频。PA8输出PWM频率=72000K/3600=200Khz 
    while(1)
	{
		time_show();
		#if EN_method1
		printf("TIM2_PA0_method1 Freq=:%d HZ\r\n",1000000/test);//输入捕获测频率方法1
		#endif	
		
		#if EN_method2
		printf("TIM2_PA0_Freq=:%0.3f KHz\t", TIM2Freq);//输入捕获测频率单位KHz
		printf("Duty=:%0.2f %%\t",Duty);//占空比
		printf("Htime=:%d us\t",Htime);//高电平时间
		printf("Ltime=:%d us\r\n",Ltime);//低电平时间
		
		LCD_ShowxNum(80,130,1000/(Rising_second - Rising_fisrt),2,16,0);//频率
		LCD_ShowxNum(80,150,(float)(Falling_first-Rising_fisrt)/(Rising_second - Rising_fisrt)*100,2,16,0);//占空比
		LCD_ShowxNum(80,170,(Falling_first-Rising_fisrt),2,16,0);//高电平时间
		LCD_ShowxNum(80,190,(Rising_second-Rising_fisrt),2,16,0);//低电平时间
		#endif
		
		#if EN_method3
		printf("TIM2_PA0_method3 Freq=:%d HZ\r\n",1000000/(Rising));//输入捕获测频率方法3 
		#endif
		
	    delay_ms(1000);	
	}
}

void time_show(void)
{
	u8 t;
	if(t!=calendar.sec)
	{
		t=calendar.sec;
		switch(calendar.week)
		{
				case 0:	LCD_ShowString(180,0,200,16,16,(u8*)"Sunday  ");break;
				case 1:	LCD_ShowString(180,0,200,16,16,(u8*)"Monday   ");break;
				case 2: LCD_ShowString(180,0,200,16,16,(u8*)"Tuesday  ");break;
				case 3:	LCD_ShowString(180,0,200,16,16,(u8*)"Wednesday");break;
				case 4:	LCD_ShowString(180,0,200,16,16,(u8*)"Thursday ");break;
				case 5: LCD_ShowString(180,0,200,16,16,(u8*)"Friday   ");break;
				case 6: LCD_ShowString(180,0,200,16,16,(u8*)"Saturday ");break;  
		}
		LCD_ShowNum(0,0,calendar.hour,2,16);									  
		LCD_ShowNum(24,0,calendar.min,2,16);									  
		LCD_ShowNum(48,0,calendar.sec,2,16);
	}	
}

