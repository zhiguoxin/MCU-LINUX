#include "dma.h"


u16 DMA1_MEM_LEN;//保存DMA每次数据传送的长度 	
//DMA1的各通道配置  从存储器->外设模式	/8位数据宽度/存储器增量模式

void USART_DMA_Init(DMA_Channel_TypeDef* DMA_CHx,u32 Source_address,u32 target_address,u16 BufferSize_len)
{
	DMA_InitTypeDef DMA_InitStructure;
	DMA1_MEM_LEN=BufferSize_len;//这一句话必须要有
	// 1、使能DMA传输
 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	// 2、将DMA的通道1寄存器重设为缺省值
    DMA_DeInit(DMA1_Channel4); 	
	// 3、设置 DMA 外设基地址：串口数据寄存器地址((u32)&USART1->DR)
	DMA_InitStructure.DMA_PeripheralBaseAddr = Source_address;
	// 4、设置 DMA 内存基地址：要传输的变量的指针SendBuff
	DMA_InitStructure.DMA_MemoryBaseAddr = target_address;
	// 5、方向：从内存到外设，从内存读取发送到外设(存储器到外设)
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	// 6、传输大小
	DMA_InitStructure.DMA_BufferSize = BufferSize_len;
	// 外设地址不增
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; 
	// 内存地址自增
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; 
	// 外设数据单位，数据宽度为8位
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 内存数据单位，数据宽度为8位
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte; 
	// DMA 模式，一次或者循环模式，这里是单次模式。就是DMA只发送一次
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	// 优先级：中
	DMA_InitStructure.DMA_Priority = DMA_Priority_Medium; 		
	// 禁止内存到内存的传输
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	// 配置 DMA1通道4
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);
	  	
} 
//开启一次DMA传输
void DMA_Enable(void)
{     
 	DMA_Cmd(DMA1_Channel4, DISABLE );  					//关闭USART1 TX DMA1 所指示的通道      
 	DMA_SetCurrDataCounter(DMA1_Channel4,DMA1_MEM_LEN);	//DMA通道的DMA缓存的大小
 	DMA_Cmd(DMA1_Channel4, ENABLE);  					//使能USART1 TX DMA1 所指示的通道 
}	  

 

























