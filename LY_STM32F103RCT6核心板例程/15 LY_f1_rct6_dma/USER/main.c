#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "tsensor.h"
#include "dma.h"

/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT3_DMA_Test 存储器->外设"
#define EXAMPLE_DATE	"2020-12-29"

static void PrintfLogo(void);
static void PrintfHelp(void);
static void LcdPrintf(void);

//定义一个常量字符数组，关于字符数组的知识请看：https://blog.csdn.net/guanyasu/article/details/51980692
const   u8 TEXT_TO_SEND[]={"STM32F103RCT3_DMA_Test!"};//要发送的字符串数据，字符串数组最后都有一个结束符\0
#define TEXT_LENTH  	sizeof(TEXT_TO_SEND)-1  //TEXT_TO_SEND字符串长度(不包含结束符\0)
uint8_t SendBuff[(TEXT_LENTH+2)];//+2是加上回车符，否则发送的数据不会换行是连在一起的，而回车换行符是0x0d和0x0a

int main(void)
{		
	u8 t,i,mask;
	delay_init();		//延时函数初始化 
	uart_init(115200);	//初始化串口，波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口
	LCD_Init();			//初始化与LCD连接的硬件接口
	USART_DMA_Init(DMA1_Channel4,(u32)&USART1->DR,(u32)SendBuff,(TEXT_LENTH+2));	
	PrintfLogo();		//串口打印例程名称和版本等信息
	PrintfHelp();		//串口打印操作提示
	LcdPrintf();		//LCD打印操作提示
	/*--------------复制字符串到SendBuff[]中开始---------------*/
	for(i=0;i<(TEXT_LENTH+2);i++)//填充数据到SendBuff
    {
		if(t>=TEXT_LENTH)//加入回车换行符(0x0d 0x0a)
		{
			if(mask)
			{
				SendBuff[i]=0x0a;
				t=0;
			}else 
			{
				SendBuff[i]=0x0d;
				mask++;
			}	
		}else//复制TEXT_TO_SEND语句
		{
			mask=0;
			SendBuff[i]=TEXT_TO_SEND[t];
			t++;
		}    	   
    }

	/*--------------复制字符串到SendBuff[]中结束----------------*/
	while(1)
	{	
		t=KEY_Scan(0);
		if(t==WKUP_PRES)//WKUP按下
		{		
			USART_DMACmd(USART1,USART_DMAReq_Tx,ENABLE);
			DMA_Enable();
			while(1)//这个while的作用是让DMA把字符串发送完成，发送完了就会跳出while循环，继续向下执行
			{
				if(DMA_GetFlagStatus(DMA1_FLAG_TC4)!=RESET)	//判断通道4传输完成
				{
					DMA_ClearFlag(DMA1_FLAG_TC4);//清除通道4传输完成标志
					break; 
				}				
			}
			LCD_ShowString(30,170,200,16,16,(u8 *)"Transimit Finished!");//提示传送完成	
		}
		LED0=!LED0;
		delay_ms(200);
	}
	
}

//打印操作提示
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("DMA_Test 存储器->外设,按下WKUP，开始传输！\r\n");	
}

//功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
static void PrintfLogo(void)
{
	printf("\n*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

//功能说明: LCD显示屏显示提示信息
static void LcdPrintf(void)
{
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"DAM TEST"); 					 
	LCD_ShowString(30,140,200,16,16,(u8*)"WKUP:Start");
}

/*
	1、关于为什么要将要发送的字符串数组TEXT_TO_SEND[i]，再一一赋值给SendBuff[i]数组的原因？
		
	利用DMA传输，将存储器（自定义的变量buffer）发送到MDA外设（串口数据存储器）中。自定义的
变量buffer就是TEXT_TO_SEND[i]，因为我们在传输的过程中，要计算自定义的变量buffer的数据长度，
而且我们还希望发送到串口数据存储器的数据，最终打印在串口调试助手上可以换行，而不是数据连在
一起。所以我们用了一个for()循环将数据从TEXT_TO_SEND[i]复制到SendBuff[i]数组，同时还在后面加
上了回车换行符\n(0x0d和0x0a)，同时也可以得到数据的长度。
	当然如果你也可以不拷贝转换，直接把(u32)TEXT_TO_SEND最为DMA存储器的地址。

	2、要想数据传输正确，源和目标地址存储的数据宽度还必须一致，串口数据寄存器是8位的，所以我
们定义的要发送的数据也必须是8位。

	3、熟悉字符串数组转换拷贝增加字符的编程思路。

	const   u8 TEXT_TO_SEND[]={"STM32F103RCT3_DMA_Test!"};	//字符串数组最后都有一个结束符\0
	#define TEXT_LENTH  	sizeof(TEXT_TO_SEND)-1  		//TEXT_TO_SEND字符串长度(不包含结束符\0)
	uint8_t SendBuff[(TEXT_LENTH+2)];						//+2是加上回车符，否则发送的数据不会换行是连在一起的，而回车换行符是0x0d和0x0a
	
	u8 t,i,mask;
	for(i=0;i<(TEXT_LENTH+2);i++)//填充数据到SendBuff
    {
		if(t>=TEXT_LENTH)//加入回车换行符(0x0d 0x0a)
		{
			if(mask)
			{
				SendBuff[i]=0x0a;
				t=0;
			}else 
			{
				SendBuff[i]=0x0d;
				mask++;
			}	
		}else//复制TEXT_TO_SEND语句
		{
			mask=0;
			SendBuff[i]=TEXT_TO_SEND[t];
			t++;
		}    	   
    }
	
*/