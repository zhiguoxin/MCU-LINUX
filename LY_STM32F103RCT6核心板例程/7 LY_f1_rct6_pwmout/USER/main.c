/* -----------------------------------------------------------------------
STM32F103C8T6一共有4个定时器，都是16位的。TIM1是高级定时器，TIM2、TIM3、TIM4
高级定时器
TIM1_CH1--PA8
TIM1_CH2--PA9
TIM1_CH3--PA10
TIM1_CH4--PA11

通用定时器
TIM2_CH1--PA0		TIM3_CH1--PA6		TIM4_CH1--PB6
TIM2_CH2--PA1		TIM3_CH2--PA7		TIM4_CH2--PB7
TIM2_CH3--PA2		TIM3_CH3--PB0		TIM4_CH3--PB8
TIM2_CH4--PA3		TIM3_CH4--PB1		TIM4_CH4--PB9

本实验利用
TIM1的通道1用来产生频率 即PA8引脚

TIM1输出频率 = TIM1 counter clock/(ARR + 1) = 72 MHz / 7200 = 10KHz
----------------------------------------------------------------------- */
#include "sys.h"
#include "led.h"
#include "pwm.h"
#include "delay.h"
#include "usart.h"

int main(void)
{	
	delay_init();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组2	
    LED_Init();	
	uart_init(115200);
	//1k   100-1,720-1
	//2k   50-1,720-1
	//3k   30-1,800-1
	//4k   20-1,900-1
	//8k   9000-1,1-1 	
	//10k  7200-1,1-1
	//20k  3600-1,1-1
	//30k  300-1,8-1
	//40k  200-1,9-1
	//50k  20-1,72-1
	//60k  200-1,6-1	
    //80k  10-1,90-1
	//90k  10-1,80-1
    //100k 720-1,1-1
 	TIM1_Output_PWM_Init(7200-1,1-1);//不分频。PA8输出PWM频率=72000K/(7199+1)=10Khz 
    while(1)
	{
	
	}	
	
}


