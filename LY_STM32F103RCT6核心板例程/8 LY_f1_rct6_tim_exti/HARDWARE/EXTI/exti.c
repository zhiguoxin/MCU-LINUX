#include "exti.h"
#include "delay.h"
#include "sys.h" 	  
//定时器2
void TIM2_Init()
{	 

	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
 	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	//使能TIM2时钟

	//初始化定时器2 TIM2	 
	TIM_TimeBaseStructure.TIM_Period = 0xffff; //当计数到65536个数时，产生一次中断，中断一次时间为1ms
	TIM_TimeBaseStructure.TIM_Prescaler =11; 	//预分频器  说白了就是计数器的频率 6M 则计数一次的时间为1/6 us
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);//允许更新中断

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  //TIM2中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  //先占优先级2级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  //从优先级0级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器	
	
    //TIM_Cmd(TIM2,ENABLE); 	//使能定时器2
}
 
//外部中断
void EXTIA0_Init(void)
{
 	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);//外部中断，需要使能AFIO时钟
 	

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;  //PA0 清除之前设置  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD; //PA0 输入  
	GPIO_Init(GPIOA, &GPIO_InitStructure);

    //GPIOA.0	  中断线以及中断初始化配置
  	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource0);
	
   	EXTI_InitStructure.EXTI_Line=EXTI_Line0;
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;//上升沿触发
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);
 
  	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;	
  	NVIC_Init(&NVIC_InitStructure);
 
}
uint32_t TimeCntValue;
uint32_t TimeCntValue0;
uint32_t CaptureNumber;
float Capture;

void EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line0)!= RESET)
	{		
		if(CaptureNumber == 0)//第1次上升沿触发
		{
			TIM_Cmd(TIM2,ENABLE);//使能定时器2
			TIM_SetCounter(TIM2,0);//清零计数器的值，因为一开始就开始计数了
			CaptureNumber++;
		}
		else if(CaptureNumber>0&& CaptureNumber<100)
		{	
            TimeCntValue0 = TIM_GetCounter(TIM2);			
			CaptureNumber++;			
		}
		else if(CaptureNumber==100)//第100次上升沿触发
		{
			
			TimeCntValue = TIM_GetCounter(TIM2);
		    Capture = TimeCntValue/100;
			CaptureNumber = 0;	
            TIM_Cmd(TIM2,DISABLE);//使能定时器2				
		}
	}
	EXTI_ClearITPendingBit(EXTI_Line0);//清除EXTI0线路挂起位		
}
