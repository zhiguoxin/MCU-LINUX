#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"

//RTC实时时钟 驱动代码

//RTC 正常工作的一般配置步骤如下：
/*第一步：使能电源时钟和备份区域时钟。
		  
		  我们要访问 RTC 和备份区域就必须先使能电源时钟和备份区域时钟。
		  
		  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
  
  第二步：取消备份区写保护。
         
		 要向备份区域写入数据，就要先取消备份区域写保护（写保护在每次硬复位之后被使能），
		 否则是无法向备份区域写入数据的。我们需要用到向备份区域写入一个字节，来标记时钟已经
		 配置过了，这样避免每次复位之后重新配置时钟。
		 
		 取消备份区域写保护的库函数实现方法：PWR_BackupAccessCmd(ENABLE); //使能RTC和后备寄存器访问
  
  第三步：复位备份区域，开启外部低速振荡器。
		 
		在取消备份区域写保护之后，我们可以先对这个区域复位，以清除前面的设置，当然这个
		操作不要每次都执行，因为备份区域的复位将导致之前存在的数据丢失，所以要不要复位，要
		看情况而定。然后我们使能外部低速振荡器，注意这里一般要先判断 RCC_BDCR 的 LSERDY
		位来确定低速振荡器已经就绪了才开始下面的操作。
		
		备份区域复位的函数是： 		BKP_DeInit();				//复位备份区域
		开启外部低速振荡器的函数是：RCC_LSEConfig(RCC_LSE_ON);	// 开启外部低速振荡器 
		
   第四步：选择RTC时钟，并使能。
   
		这里我们将通过RCC_BDCR的RTCSEL来选择外部LSI作为RTC的时钟。然后通过RTCEN位使能RTC时钟。		
		
		库函数中，选择 RTC 时钟的函数是：RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE); //选择LSE作为RTC时钟
		对于 RTC 时钟的选择，还有RCC_RTCCLKSource_LSI和RCC_RTCCLKSource_HSE_Div128
		两个，顾名思义，前者为 LSI，后者为 HSE 的 128 分频。		
		
		使能RTC 时钟的函数是：RCC_RTCCLKCmd(ENABLE); //使能 RTC 时钟
		 
	第五步：设置RTC的分频，以及配置RTC时钟。 
		 
		 在开启了 RTC 时钟之后，要做的就是设置RTC时钟的分频数，通过RTC_PRLH和RTC_PRLL来设置，
		 然后等待 RTC 寄存器操作完成，并同步之后，设置秒钟中断。然后设置RTC 的允许配置位(RTC_CRH的CNF 位)，
		 设置时间（其实就是设置 RTC_CNTH 和 RTC_CNTL两个寄存器）。
		  
		  5.1、在进行 RTC 配置之前首先要打开允许配置位(CNF)：	 RTC_EnterConfigMode();/// 允许配置
		  5.2、在配置完成之后，千万别忘记更新配置同时退出配置模式：RTC_ExitConfigMode();//退出配置模式，更新配置
		  5.3、设置 RTC 时钟分频数，库函数是：					void RTC_SetPrescaler(uint32_t PrescalerValue);
		  5.4、设置秒中断允许，RTC 使能中断的函数是：			void RTC_ITConfig(uint16_t RTC_IT, FunctionalState NewState)；
		  5.5、使能秒中断：										RTC_ITConfig(RTC_IT_SEC, ENABLE); //使能 RTC 秒中断
		  5.6、设置时间，实际上就是设置RTC的计数值，时间与计数值之间是需要换算的：void RTC_SetCounter(uint32_t CounterValue)；
		  
	
	
	第六步：设置完时钟之后，配置更新同时退出配置模式。
		   
		   RTC_ExitConfigMode();//退出配置模式，更新配置
		   
	第七步：在退出配置模式更新配置之后我们在备份区域 BKP_DR1 中写入 0X5050 代表我们已经初始化
			过时钟了，下次开机（或复位）的时候，先读取 BKP_DR1 的值，然后判断是否是 0X5050 来
			决定是不是要配置。
	
	第八步：设置RTC中断优先级分组。
	        
			有中断就要编写中断优先级分组。
	
	第九步：编写中断服务函数。
            
			有中断溢出就要编写中断服务函数。 
		 
*/

_calendar_obj calendar;//时钟结构体

//实时时钟配置
//初始化RTC时钟,同时检测时钟是否工作正常
//BKP->DR1用于保存是否第一次配置的设置
//返回0:正常
//其他:错误代码
/*
	RTC_Init();该函数用来初始化 RTC 时钟，但是只在第一次的时候设置时间，以后如果重新上电/复位都不会再进
行时间设置了(前提是备份电池有电)，在第一次配置的时候，我们是按照上面介绍的RTC 初始化步骤来做的，这里就
不在多说了，这里我们设置时间是通过时间设置函数RTC_Set(2020,12,27,14,47,00);来实现的，这里我们默认将时
间设置为2020年12月27日14点47分00秒。在设置好时间之后，我们通过语句BKP_WriteBackupRegister(BKP_DR1, 0X5050);
向BKP->DR1 写入标志字 0X5050，用于标记时间已经被设置了。这样，再次发生复位的时候，该函数通过语句
if (BKP_ReadBackupRegister(BKP_DR1) != 0x5050)判断 BKP->DR1的值，来决定是不是需要重新设置时间，如果不
需要设置，则跳过时间设置，仅仅使能秒钟中断一下，就进行中断分组，然后返回了。这样不会重复设置时间，使得
我们设置的时间不会因复位或者断电而丢失。
*/
u8 RTC_Init(void)
{
	//检查是不是第一次配置时钟
	u8 temp=0;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);//使能PWR和BKP外设时钟   
	//默认情况下，RTC 所属的备份域禁止访问，可使用库函数 PWR_BackupAccessCmd 使能访问
	PWR_BackupAccessCmd(ENABLE);//允许访问备份区域，后备区域解锁
	if (BKP_ReadBackupRegister(BKP_DR1) != 0x5050)		//从指定的后备寄存器中读出数据:读出了与写入的指定数据不相等就重新配置
	{	 			

		BKP_DeInit();//复位备份区域 使用此函数必须调用RCC_APB1PeriphClockCmd()函数	
		RCC_LSEConfig(RCC_LSE_ON);//设置外部低速晶振(LSE),外部32.768KHZ晶振开启
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)	//检查指定的RCC标志位设置与否,等待低速晶振就绪
		{
			temp++;
			delay_ms(10);
		}
		if(temp>=250)
			return 1;//初始化时钟失败,晶振有问题	    
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);		////设置RTC时钟(RTCCLK),选择LSE(晶振频率为 32.768KHz)作为RTC时钟   	
		RCC_RTCCLKCmd(ENABLE);						//使能RTC时钟  
		RTC_WaitForLastTask();						//等待最近一次对RTC寄存器的写操作完成，因为RTC时钟是低速的，内环时钟是高速的，所以要同步
		RTC_WaitForSynchro();						//等待RTC寄存器同步，写寄存器之前要确保上一次RTC的操作完成	
		
		//下面这两条语句是开启RTC秒中断的函数，每过1秒钟产生一次中断，就进入了中断服务函数
		RTC_ITConfig(RTC_IT_SEC, ENABLE);			//使能RTC秒中断
		RTC_WaitForLastTask();						//等待最近一次对RTC寄存器的写操作完成，确保上一次 RTC 的操作完成	
		
		RTC_EnterConfigMode();						//进入RTC配置模式
		RTC_SetPrescaler(32767); 					//设置RTC预分频的值，使 RTC周期为1s  RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) = 1HZ	在使用本函数前必须先调用函数RTC_WaitForLastTask();等待标志位RTOFF被设置
		RTC_WaitForLastTask();						//等待最近一次对RTC寄存器的写操作完成，确保上一次 RTC 的操作完成
		RTC_Set(2020,12,28,14,29,00);  				//设置时间，初始值设定为2020年12月27号14点47分00秒 在使用本函数前必须先调用函数RTC_WaitForLastTask();等待标志位RTOFF被设置	
		
		RTC_ExitConfigMode(); 						//退出RTC配置模式
		
		//在退出配置模式更新配置之后我们在备份区域BKP_DR1中写入0X5050代表我们已经初始化
		//过时钟了，下次开机(或复位)的时候，先读取 BKP_DR1的值，然后判断是否是0X5050来
		//决定是不是要配置
		BKP_WriteBackupRegister(BKP_DR1, 0X5050);	//向指定的后备寄存器中写入用户程序数据
	}
	else//系统继续计时
	{

		RTC_WaitForSynchro();//等待最近一次对RTC寄存器的写操作完成
		RTC_ITConfig(RTC_IT_SEC, ENABLE);//使能RTC秒中断
		RTC_WaitForLastTask();//等待最近一次对RTC寄存器的写操作完成
	}
	
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;		
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;		
	NVIC_Init(&NVIC_InitStructure);	 
	
	RTC_Get();			//更新时间	
	return 0; 			//ok

}  

//判断是否是闰年函数
//月份   1  2  3  4  5  6  7  8  9  10 11 12
//闰年   31 29 31 30 31 30 31 31 30 31 30 31
//非闰年 31 28 31 30 31 30 31 31 30 31 30 31
//输入:年份
//输出:该年份是不是闰年.1,是.0,不是
u8 Is_Leap_Year(u16 year)
{			  
	if(year%4==0) //必须能被4整除
	{ 
		if(year%100==0) 
		{ 
			if(year%400==0)return 1;//如果以00结尾,还要能被400整除 	   
			else return 0;   
		}else return 1;   
	}else return 0;	
}	 			   
//设置时钟
//把输入的时钟转换为秒钟
//以1970年1月1日为基准
//1970~2099年为合法年份
//返回值:0,成功;其他:错误代码.
//月份数据表											 
u8 const table_week[12]={0,3,3,6,1,4,6,2,5,0,3,5}; //月修正数据表	  
//平年的月份日期表
const u8 mon_table[12]={31,28,31,30,31,30,31,31,30,31,30,31};
u8 RTC_Set(u16 syear,u8 smon,u8 sday,u8 hour,u8 min,u8 sec)
{
	u16 t;
	u32 seccount=0;
	if(syear<1970||syear>2099)return 1;	   
	for(t=1970;t<syear;t++)	//把所有年份的秒钟相加
	{
		if(Is_Leap_Year(t))seccount+=31622400;//闰年的秒钟数
		else seccount+=31536000;			  //平年的秒钟数
	}
	smon-=1;
	for(t=0;t<smon;t++)	   //把前面月份的秒钟数相加
	{
		seccount+=(u32)mon_table[t]*86400;//月份秒钟数相加
		if(Is_Leap_Year(syear)&&t==1)seccount+=86400;//闰年2月份增加一天的秒钟数	   
	}
	seccount+=(u32)(sday-1)*86400;//把前面日期的秒钟数相加 
	seccount+=(u32)hour*3600;//小时秒钟数
    seccount+=(u32)min*60;	 //分钟秒钟数
	seccount+=sec;//最后的秒钟加上去

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);	//使能PWR和BKP外设时钟  
	PWR_BackupAccessCmd(ENABLE);	//使能RTC和后备寄存器访问 
	RTC_SetCounter(seccount);	//设置RTC计数器的值

	RTC_WaitForLastTask();	//等待最近一次对RTC寄存器的写操作完成  	
	RTC_Get();
	return 0;	    
}

/*
RTC_Get();函数其实就是将存储在秒钟寄存器 RTC->CNTH 和 RTC->CNTL 中的秒钟数据转换为真正的
时间和日期。该代码还用到了一个 calendar 的结构体，calendar 是我们在 rtc.h 里面将要定义的
一个时间结构体，用来存放时钟的年月日时分秒等信息。因为 STM32 的 RTC 只有秒钟计数器，而年
月日，时分秒这些需要我们自己软件计算。我们把计算好的值保存在 calendar 里面，方便其他程序调用。
*/
//得到当前的时间
//返回值:0,成功;其他:错误代码.
u8 RTC_Get(void)
{
	static u16 daycnt=0;
	u32 timecount=0; 
	u32 temp=0;
	u16 temp1=0;	  
    timecount=RTC_GetCounter();	 
 	temp=timecount/86400;   //得到天数(秒钟数对应的)
	if(daycnt!=temp)//超过一天了
	{	  
		daycnt=temp;
		temp1=1970;	//从1970年开始
		while(temp>=365)
		{				 
			if(Is_Leap_Year(temp1))//是闰年
			{
				if(temp>=366)temp-=366;//闰年的秒钟数
				else {temp1++;break;}  
			}
			else temp-=365;	  //平年 
			temp1++;  
		}   
		calendar.w_year=temp1;//得到年份
		temp1=0;
		while(temp>=28)//超过了一个月
		{
			if(Is_Leap_Year(calendar.w_year)&&temp1==1)//当年是不是闰年/2月份
			{
				if(temp>=29)temp-=29;//闰年的秒钟数
				else break; 
			}
			else 
			{
				if(temp>=mon_table[temp1])temp-=mon_table[temp1];//平年
				else break;
			}
			temp1++;  
		}
		calendar.w_month=temp1+1;	//得到月份
		calendar.w_date=temp+1;  	//得到日期 
	}
	temp=timecount%86400;     		//得到秒钟数   	   
	calendar.hour=temp/3600;     	//小时
	calendar.min=(temp%3600)/60; 	//分钟	
	calendar.sec=(temp%3600)%60; 	//秒钟
	calendar.week=RTC_Get_Week(calendar.w_year,calendar.w_month,calendar.w_date);//获取星期   
	return 0;
}	 
//获得现在是星期几
//功能描述:输入公历日期得到星期(只允许1901-2099年)
//输入参数：公历年月日 
//返回值：星期号																						 
u8 RTC_Get_Week(u16 year,u8 month,u8 day)
{	
	u16 temp2;
	u8 yearH,yearL;
	
	yearH=year/100;	yearL=year%100; 
	// 如果为21世纪,年份数加100  
	if (yearH>19)yearL+=100;
	// 所过闰年数只算1900年之后的  
	temp2=yearL+yearL/4;
	temp2=temp2%7; 
	temp2=temp2+day+table_week[month-1];
	if (yearL%4==0&&month<3)temp2--;
	return(temp2%7);
}			  

//RTC时钟中断
//每秒触发一次  
//extern u16 tcnt; 
void RTC_IRQHandler(void)
{		 
	if (RTC_GetITStatus(RTC_IT_SEC) != RESET)//1秒钟中断
	{							
		RTC_Get();//更新时间，获取现在的时间
 	}
	if(RTC_GetITStatus(RTC_IT_ALR)!= RESET)//闹钟中断
	{
		RTC_ClearITPendingBit(RTC_IT_ALR);//清闹钟中断	  	   
  	} 				  								 
	RTC_ClearITPendingBit(RTC_IT_SEC|RTC_IT_OW);////清除秒中断标志位和溢出位
	RTC_WaitForLastTask();//这个操作必须有，保证上一次操作成功   						 	   	 
}














