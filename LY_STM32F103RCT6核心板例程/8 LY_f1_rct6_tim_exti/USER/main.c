/* -----------------------------------------------------------------------
STM32F103C8T6一共有4个定时器，都是16位的。TIM1是高级定时器，TIM2、TIM3、TIM4
高级定时器
TIM1_CH1--PA8
TIM1_CH2--PA9
TIM1_CH3--PA10
TIM1_CH4--PA11

通用定时器
TIM2_CH1--PA0		TIM3_CH1--PA6		TIM4_CH1--PB6
TIM2_CH2--PA1		TIM3_CH2--PA7		TIM4_CH2--PB7
TIM2_CH3--PA2		TIM3_CH3--PB0		TIM4_CH3--PB8
TIM2_CH4--PA3		TIM3_CH4--PB1		TIM4_CH4--PB9

本实验利用
TIM1的通道1用来产生频率 即PA8引脚
TIM2的通道1用来测量频率 即PA0引脚

TIM2的PA0引脚连接到PA8 TIM2就把TIM1的频率测到了 然后发送到串口显示

TIM1输出频率 = TIM1 counter clock/(ARR + 1) = 72 MHz / 7200 = 10KHz
----------------------------------------------------------------------- */

#include "sys.h"
#include "led.h"
#include "pwm.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"
#include "lcd.h"
#include "exti.h"

void time_show(void);
int main(void)
{	
	delay_init();//延时函数初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组2	
	uart_init(115200);
	LCD_Init();		
	
	while(RTC_Init())//RTC初始化，一定要初始化成功
	{ 
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC ERROR!   ");	
		delay_ms(800);
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC Trying...");	
	}
	
	//显示提示信息
	POINT_COLOR=RED;
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Freq Test");	
	LCD_ShowString(10,300,200,16,16,(u8*)"Copyright@liuyao-blog.cn");	
	POINT_COLOR=BLUE;//设置字体为蓝色					 
	LCD_ShowString(0,0,200,16,16,(u8*)"  :  :  ");	
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(30,130,200,16,16,(u8*)"Freq:    KHz");	      
	
	TIM2_Init();
 	TIM1_Output_PWM_Init(3600-1,1-1);//不分频。PA8输出PWM频率=72000K/3600=20Khz 
	EXTIA0_Init();
    while(1)
	{
		time_show();		
		LCD_ShowxNum(80,130,6000/Capture,2,16,0);//频率		
	    delay_ms(1000);	
	}
}

void time_show(void)
{
	u8 t;
	if(t!=calendar.sec)
	{
		t=calendar.sec;
		switch(calendar.week)
		{
				case 0:	LCD_ShowString(180,0,200,16,16,(u8*)"Sunday  ");break;
				case 1:	LCD_ShowString(180,0,200,16,16,(u8*)"Monday   ");break;
				case 2: LCD_ShowString(180,0,200,16,16,(u8*)"Tuesday  ");break;
				case 3:	LCD_ShowString(180,0,200,16,16,(u8*)"Wednesday");break;
				case 4:	LCD_ShowString(180,0,200,16,16,(u8*)"Thursday ");break;
				case 5: LCD_ShowString(180,0,200,16,16,(u8*)"Friday   ");break;
				case 6: LCD_ShowString(180,0,200,16,16,(u8*)"Saturday ");break;  
		}
		LCD_ShowNum(0,0,calendar.hour,2,16);									  
		LCD_ShowNum(24,0,calendar.min,2,16);									  
		LCD_ShowNum(48,0,calendar.sec,2,16);
	}	
}

