//ROM是“Read Only Memory”的缩写，意为只能读的存储器。由于技术的发展，后来设计出了可以方便写入数据的 ROM，
//而这个“Read Only Memory”的名称被沿用下来了。

//EEPROM(Electrically Erasable Programmable ROM)是电可擦除存储器。EEPROM 可以重复擦写，EEPROM 是一种掉电
//后数据不丢失的存储器，常用来存储一些配置信息，以便系统重新上电的时候加载之。它的擦除和写入都是直接使用
//电路控制，不需要再使用外部设备来擦写。而且可以按字节为单位修改数据，无需整个芯片擦除。现在主要使用的ROM
//芯片都是EEPROM。24C02是一个2K Bit的串行EEPROM存储器（掉电不丢失），内部含有256个字节。在24C02里面有一个8
//字节的页写缓冲器。

//EEPOM 芯片最常用的通讯方式就是 I2C协议。
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "tsensor.h"
#include "dma.h"
#include "myiic.h"
#include "24cxx.h"


/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT6 IIC TEST"
#define EXAMPLE_DATE	"2020-12-30"

static void PrintfLogo(void);
static void PrintfHelp(void);
static void LcdPrintf(void);

//要写入到24c02的字符串数组
const u8 TEXT_Buffer[]={"STM32F103RCT6_IIC_TEST"};//这里放了22个字节，AT24C02一共有256个字节。
#define SIZE sizeof(TEXT_Buffer)	

int main(void)
{	
	u8 key;
	u8 datatemp[SIZE];
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	delay_init();		//延时函数初始化 
	uart_init(115200);	//初始化串口，波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口
	LCD_Init();			//初始化与LCD连接的硬件接口
	PrintfLogo();		//串口打印例程名称和版本等信息
	PrintfHelp();		//串口打印操作提示
	LcdPrintf();		//LCD打印操作提示
	AT24CXX_Init();		//IIC初始化 
	
 	while(AT24CXX_Check())//检测不到24c02
	{
		LCD_ShowString(60,150,200,16,16,(u8*)"24C02 Check Failed!");
		delay_ms(500);
		LCD_ShowString(60,150,200,16,16,(u8*)"Please Check!      ");
		delay_ms(500);
		LED0=!LED0;//DS0闪烁
	}
	LCD_ShowString(60,170,200,16,16,(u8*)"24C02 Ready!");    
 	POINT_COLOR=BLUE;//设置字体为蓝色	  
	while(1)
	{
		key=KEY_Scan(0);
		if(key==WKUP_PRES)//WK_UP 按下,写入24C02
		{
			LCD_Fill(0,170,239,319,WHITE);//清除半屏    
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Write 24C02....");
			AT24CXX_Write(0,(u8*)TEXT_Buffer,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"24C02 Write Finished!");//提示传送完成
		}
		if(key==KEY0_PRES)//KEY0 按下,读取字符串并显示
		{
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Read 24C02.... ");
			AT24CXX_Read(0,datatemp,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"The Data Readed Is:  ");//提示传送完成
			LCD_ShowString(30,230,200,16,16,datatemp);//显示读到的字符串
		}		   
	}
}

//打印操作提示
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("WK_UP:Write  KEY0:Read\r\n");	
}

//功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
static void PrintfLogo(void)
{
	printf("\n*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

//功能说明: LCD显示屏显示提示信息
static void LcdPrintf(void)
{
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"DAM TEST"); 					 
	LCD_ShowString(30,140,200,16,16,(u8*)"WK_UP:Write  KEY0:Read");
}

