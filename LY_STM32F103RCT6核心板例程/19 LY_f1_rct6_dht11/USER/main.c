#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"
#include "lcd.h"
#include "dht11.h"

int main(void){			    
 
	delay_init();	    	//延时函数初始化	  
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
 	LCD_Init();
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"TFTLCD TEST");
	LCD_ShowString(30,90,200,16,16,(u8*)"GOUGUO");
	while(DHT11_Init())	//DHT11初始化	
	{
		LCD_ShowString(30,130,200,16,16,(u8*)"DHT11 Error");
		delay_ms(200);
		LCD_Fill(30,130,239,130+16,WHITE);
		delay_ms(200);
	}								   
	LCD_ShowString(30,130,200,16,16,(u8*)"DHT11 OK");
	
	POINT_COLOR=BLUE;//设置字体为蓝色 
	LCD_ShowString(30,150,200,16,16,(u8*)"Humi:  .  RH");	 
	LCD_ShowString(30,170,200,16,16,(u8*)"Temp:  .  C");	 	
  	while(1) 
	{ 		
		DHT11_Read_Data();	//读取温湿度值					    
		LCD_ShowNum(30+40,150,Hum_H,2,16);	//显示温度	   		   
		LCD_ShowNum(30+60,150,Hum_L,2,16);	//显示湿度
		LCD_ShowNum(30+40,170,Tem_H,2,16);	//显示温度	   		   
		LCD_ShowNum(30+60,170,Tem_L,2,16);	//显示湿度		
		
		printf("Humidity = %d.%d%%RH\n", Hum_H, Hum_L);
		printf("Temperature = %d.%d℃\n\n", Tem_H, Tem_L);
		delay_ms(500);
	}
}


