#ifndef __DHT11_H
#define __DHT11_H 
#include "sys.h"  

#define USE_register  0

/* 定义DHT11连接的GPIO端口, 用户只需要修改下面3行代码即可任意改变引脚 */
#define DHT11_GPIO_PORT		GPIOC					/* GPIO端口 */
#define DHT11_GPIO_CLK 		RCC_APB2Periph_GPIOC	/* GPIO端口时钟 */
#define DHT11_GPIO_PIN		GPIO_Pin_0			/* 连接到数据线的GPIO */
 

//IO操作函数									   
#define	DHT11_DQ_OUT PCout(0) //数据端口	PC0 
#define	DHT11_DQ_IN  PCin(0)  //数据端口	PC0 

extern  u8 Hum_H, Hum_L, Tem_H, Tem_L;
u8 DHT11_Init(void);//初始化DHT11
u8 DHT11_Read_Data(void);//读取温湿度
u8 DHT11_Read_Byte(void);//读出一个字节
u8 DHT11_Read_Bit(void);//读出一个位
u8 DHT11_Check(void);//检测是否存在DHT11
void DHT11_Start(void);//复位DHT11    
#endif















