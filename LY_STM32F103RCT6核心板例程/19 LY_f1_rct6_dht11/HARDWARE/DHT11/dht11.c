#include "dht11.h"
#include "delay.h"

//初始化DHT11的IO口 DQ 同时检测DHT11的存在
//返回1:不存在  返回0:存在    	 
u8 DHT11_Init(void)
{	 
 	GPIO_InitTypeDef  GPIO_InitStructure; 	
	RCC_APB2PeriphClockCmd(DHT11_GPIO_CLK, ENABLE);	 	//使能PG端口时钟
	GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;		//PG11端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 	//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStructure);	//初始化IO口
	GPIO_SetBits(DHT11_GPIO_PORT,DHT11_GPIO_PIN);			//PG11 输出高
			    
	DHT11_Start();  	 //复位DHT11
	return DHT11_Check();//等待DHT11的回应
}
//----------------------------------------IO方向设置-----------------------------------------------
//GPIOC->CRL&=0XFFFFFFF0;
//GPIOx->CRL,这句话表示要操作GPIOx的低8位，就是Px0~Px7,(CRL表示操作高8位，就是Px8~Px15);
//所以GPIOC->CRL,这句话表示要操作GPIOC的第0位，就是PC0
//后面的0XFFFFFFF0,表示操作PC0;0XFFFFFF0F,表示操作PC1;0XFFFFF0FF,表示操作PC2;0XFFFF0FFF,表示操作PC3;
//		0XFFF0FFFF,表示操作PC4;0XFF0FFFFF,表示操作PC5;0XF0FFFFFF,表示操作PC3;0X0FFFFFFF,表示操作PC7;
//		合起来的意思就是：利用“与”运算，把这个位清0，同时不影响其他的位的设置。

//GPIOC->CRL|=8<<0;
//意思就是将1000左移0位(不移位)，然后再与GPIOC->CRL进行“或”运算。再根据原子的寄存器开发手册可以知道
//CNF0[10]、MODEO[00],对应的就是设置为上拉/输入模式。
//-------------------------------------------------------------------------------------------=-----

#if USE_register

#define DHT11_IO_IN()  {GPIOC->CRL&=0XFFFFFFF0;GPIOC->CRL|=8<<0;}//上拉输入
#define DHT11_IO_OUT() {GPIOC->CRL&=0XFFFFFFF0;GPIOC->CRL|=3<<0;}//推挽输出

#else 

void DHT11_IO_IN(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;//上拉输入
	GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStructure);
}
void DHT11_IO_OUT(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;//推挽输出
	GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStructure);
}
#endif

//DHT11起始信号
void DHT11_Start(void)	   
{                 
	DHT11_IO_OUT(); 	//SET OUTPUT
    DHT11_DQ_OUT=0; 	//拉低DQ
    delay_ms(20);    	//保持拉低至少18ms
    DHT11_DQ_OUT=1; 	//DQ=1 
	delay_us(30);     	//保持拉高20~40us
}
//等待DHT11的回应
//返回1:未检测到DHT11的存在 返回0:存在
u8 DHT11_Check(void) 	   
{   
	u8 timeout;
	DHT11_IO_IN();		//SET INPUT
	
	timeout=80;
	/*------等待DHT11将数据线拉低80us开始---------------*/	
    while ((DHT11_DQ_IN==0)&&(timeout>0))//DHT11会拉低40~80us，实际上会拉低47us
	{
		timeout--;
		delay_us(1);
	};
	/*------等待DHT11将数据线拉低80us结束------------*/	
	//如果没有传感器，DHT11_DQ_IN是默认拉高的为1，DHT11_DQ_IN==0不成立，就会跳出循环
	//timeout的值就为80，!timeout=0
	if(timeout==80)	return 1;	
	
	else timeout=80;
	/*------等待DHT11将数据线拉高80us开始---------------*/	
    while ((DHT11_DQ_IN==1)&&(timeout>0))//DHT11拉低后会再次拉高40~80us
	{
		timeout--;
		delay_us(1);
	};
	/*------等待DHT11将数据线拉高80us结束---------------*/
	if(timeout==80)return 1;  
	return 0;
	
	
}
//从DHT11读取一个位 返回值：1/0 
u8 DHT11_Read_Bit(void) 			 
{
 	u8 timeout=0;
	while(DHT11_DQ_IN&&timeout<50)//等待变为低电平
	{
		timeout++;
		delay_us(1);
	}
	timeout=0;
	while(!DHT11_DQ_IN&&timeout<50)//等待变高电平
	{
		timeout++;
		delay_us(1);
	}
	delay_us(40);//等待40us
	if(DHT11_DQ_IN)return 1;
	else return 0;		   
}
//从DHT11读取一个字节  返回值：读到的数据
u8 DHT11_Read_Byte(void)    
{        
    u8 i,dat;
    dat=0;
	for (i=0;i<8;i++) 
	{
   		dat<<=1; 
	    dat|=DHT11_Read_Bit();
    }						    
    return dat;
}
//从DHT11读取一次数据
//temp:温度值(范围:0~50.9°)
//humi:湿度值(范围:20%~90%)
//返回值：0,正常;1,读取失败
u8 Hum_H, Hum_L, Tem_H, Tem_L;
u8 DHT11_Read_Data(void)    
{        
 	
	u8 buf[5];
	u8 i;
	DHT11_Start();
	if(DHT11_Check()==0)
	{
		//连续读取5个Byte，共计40bit，内含湿度8位整数位，湿度8位小数位，温度8位整数位，温度8位小数位，校验和
		for(i=0;i<5;i++)
		{
			buf[i]=DHT11_Read_Byte();
		}
		if((buf[0]+buf[1]+buf[2]+buf[3])==buf[4])
		{
			Hum_H=buf[0];//温度的整数位
			Hum_L=buf[1];//温度的小数值
			Tem_H=buf[2];//熟读的整数值
			Tem_L=buf[3];//湿度的小数值
		}
	}
	else 
		return 1;
	return 0;	    
}
