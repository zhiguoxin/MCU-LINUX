#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"
#include "lcd.h"

//RTC实时时钟 驱动代码

//RTC 正常工作的一般配置步骤如下：
/*第一步：使能电源时钟和备份区域时钟。
		  
		  我们要访问 RTC 和备份区域就必须先使能电源时钟和备份区域时钟。
		  
		  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
  
  第二步：取消备份区写保护。
         
		 要向备份区域写入数据，就要先取消备份区域写保护（写保护在每次硬复位之后被使能），
		 否则是无法向备份区域写入数据的。我们需要用到向备份区域写入一个字节，来标记时钟已经
		 配置过了，这样避免每次复位之后重新配置时钟。
		 
		 取消备份区域写保护的库函数实现方法：PWR_BackupAccessCmd(ENABLE); //使能RTC和后备寄存器访问
  
  第三步：复位备份区域，开启外部低速振荡器。
		 
		在取消备份区域写保护之后，我们可以先对这个区域复位，以清除前面的设置，当然这个
		操作不要每次都执行，因为备份区域的复位将导致之前存在的数据丢失，所以要不要复位，要
		看情况而定。然后我们使能外部低速振荡器，注意这里一般要先判断 RCC_BDCR 的 LSERDY
		位来确定低速振荡器已经就绪了才开始下面的操作。
		
		备份区域复位的函数是： 		BKP_DeInit();				//复位备份区域
		开启外部低速振荡器的函数是：RCC_LSEConfig(RCC_LSE_ON);	// 开启外部低速振荡器 
		
   第四步：选择RTC时钟，并使能。
   
		这里我们将通过RCC_BDCR的RTCSEL来选择外部LSI作为RTC的时钟。然后通过RTCEN位使能RTC时钟。		
		
		库函数中，选择 RTC 时钟的函数是：RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE); //选择LSE作为RTC时钟
		对于 RTC 时钟的选择，还有RCC_RTCCLKSource_LSI和RCC_RTCCLKSource_HSE_Div128
		两个，顾名思义，前者为 LSI，后者为 HSE 的 128 分频。		
		
		使能RTC 时钟的函数是：RCC_RTCCLKCmd(ENABLE); //使能 RTC 时钟
		 
	第五步：设置RTC的分频，以及配置RTC时钟。 
		 
		 在开启了 RTC 时钟之后，要做的就是设置RTC时钟的分频数，通过RTC_PRLH和RTC_PRLL来设置，
		 然后等待 RTC 寄存器操作完成，并同步之后，设置秒钟中断。然后设置RTC 的允许配置位(RTC_CRH的CNF 位)，
		 设置时间（其实就是设置 RTC_CNTH 和 RTC_CNTL两个寄存器）。
		  
		  5.1、在进行 RTC 配置之前首先要打开允许配置位(CNF)：	 RTC_EnterConfigMode();/// 允许配置
		  5.2、在配置完成之后，千万别忘记更新配置同时退出配置模式：RTC_ExitConfigMode();//退出配置模式，更新配置
		  5.3、设置 RTC 时钟分频数，库函数是：					void RTC_SetPrescaler(uint32_t PrescalerValue);
		  5.4、设置秒中断允许，RTC 使能中断的函数是：			void RTC_ITConfig(uint16_t RTC_IT, FunctionalState NewState)；
		  5.5、使能秒中断：										RTC_ITConfig(RTC_IT_SEC, ENABLE); //使能 RTC 秒中断
		  5.6、设置时间，实际上就是设置RTC的计数值，时间与计数值之间是需要换算的：void RTC_SetCounter(uint32_t CounterValue)；
		  
	
	
	第六步：设置完时钟之后，配置更新同时退出配置模式。
		   
		   RTC_ExitConfigMode();//退出配置模式，更新配置
		   
	第七步：在退出配置模式更新配置之后我们在备份区域 BKP_DR1 中写入 0X5050 代表我们已经初始化
			过时钟了，下次开机（或复位）的时候，先读取 BKP_DR1 的值，然后判断是否是 0X5050 来
			决定是不是要配置。
	
	第八步：设置RTC中断优先级分组。
	        
			有中断就要编写中断优先级分组。
	
	第九步：编写中断服务函数。
            
			有中断溢出就要编写中断服务函数。 
		 
*/

_calendar_obj calendar;//时钟结构体

//实时时钟配置
//初始化RTC时钟,同时检测时钟是否工作正常
//BKP->DR1用于保存是否第一次配置的设置
//返回0:正常
//其他:错误代码
/*
	RTC_Init();该函数用来初始化 RTC 时钟，但是只在第一次的时候设置时间，以后如果重新上电/复位都不会再进
行时间设置了(前提是备份电池有电)，在第一次配置的时候，我们是按照上面介绍的RTC 初始化步骤来做的，这里就
不在多说了，这里我们设置时间是通过时间设置函数RTC_Set(2020,12,27,14,47,00);来实现的，这里我们默认将时
间设置为2020年12月27日14点47分00秒。在设置好时间之后，我们通过语句BKP_WriteBackupRegister(BKP_DR1, 0X5050);
向BKP->DR1 写入标志字 0X5050，用于标记时间已经被设置了。这样，再次发生复位的时候，该函数通过语句
if (BKP_ReadBackupRegister(BKP_DR1) != 0x5050)判断 BKP->DR1的值，来决定是不是需要重新设置时间，如果不
需要设置，则跳过时间设置，仅仅使能秒钟中断一下，就进行中断分组，然后返回了。这样不会重复设置时间，使得
我们设置的时间不会因复位或者断电而丢失。

	RTC区域的时钟比APB时钟慢，访问前需要进行时钟同步，只要调用库函数RTC_WaitForSynchro即可 ， 而如果修改了
RTC的寄存器，又需要调用RTC_WaitForLastTask函数确保数据已写入


*/
u8 RTC_Init(void)
{
	//检查是不是第一次配置时钟
	u8 temp=0;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);//使能PWR和BKP外设时钟   
	//默认情况下，RTC 所属的备份域禁止访问，可使用库函数 PWR_BackupAccessCmd 使能访问
	PWR_BackupAccessCmd(ENABLE);//允许访问备份区域，后备区域解锁
	if(BKP_ReadBackupRegister(BKP_DR1)!=0X1001) //
	{	 			

		BKP_DeInit();//复位备份区域 使用此函数必须调用RCC_APB1PeriphClockCmd()函数	
		#if USE_RCC_LSE
		RCC_LSEConfig(RCC_LSE_ON);//设置外部低速晶振(LSE),外部32.768KHZ晶振开启
		while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)	//检查指定的RCC标志位设置与否,等待低速晶振LSE就绪
		{
			temp++;
			delay_ms(10);
		}
		if(temp>=250)
			return 1;//初始化时钟失败,晶振有问题	    
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);//设置RTC时钟(RTCCLK),选择LSE(晶振频率为 32.768KHz)作为RTC时钟 
		RCC_RTCCLKCmd(ENABLE);						//使能RTC时钟  
		RTC_WaitForSynchro();						//等待 RTC 寄存器 同步，因为RTC区域的时钟比APB时钟慢，所以要同步
		RTC_WaitForLastTask();						//确保上一次RTC的操作完成

		//开启RTC秒中断的函数，每过1秒钟产生一次中断，就进入了中断服务函数
		//RTC_ITConfig(RTC_IT_SEC, ENABLE);			//使能RTC秒中断
		//RTC_WaitForLastTask();						//确保上一次RTC的操作完成
			
		RTC_EnterConfigMode();						//进入RTC配置模式，这句话可以不要，因为在RTC_SetPrescaler();中第一句话就是RTC_EnterConfigMode();
		RTC_SetPrescaler(32768-1); 					//设置RTC预分频的值，使 RTC周期为1s  RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) = 1HZ	在使用本函数前必须先调用函数RTC_WaitForLastTask();等待标志位RTOFF被设置
		RTC_WaitForLastTask();						//确保上一次 RTC 的操作完成

        #endif
		
		#if USE_RCC_LSI
		RCC_LSICmd(ENABLE);//使能 LSI(不使用外部时钟，使用内部时钟。外部32.768KHZ晶振关闭)
		while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)	//检查指定的RCC标志位设置与否,等待 LSI 准备好
		{
			temp++;
			delay_ms(10);
		}
		if(temp>=250)
			return 1;//初始化时钟失败,晶振有问题	    
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);		//选择 LSI 作为 RTC 时钟源		
		RCC_RTCCLKCmd(ENABLE);						//使能RTC时钟  
		RTC_WaitForSynchro();						//等待 RTC 寄存器 同步，因为RTC区域的时钟比APB时钟慢，所以要同步
		RTC_WaitForLastTask();						//确保上一次RTC的操作完成

		//下面这两条语句是开启RTC秒中断的函数，每过1秒钟产生一次中断，就进入了中断服务函数
		RTC_ITConfig(RTC_IT_SEC, ENABLE);			//使能RTC秒中断
		RTC_WaitForLastTask();						//确保上一次RTC的操作完成
			
		RTC_EnterConfigMode();						//进入RTC配置模式
		RTC_SetPrescaler(40000-1); 					//设置RTC预分频的值，使 RTC周期为1s  RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) = 1HZ	在使用本函数前必须先调用函数RTC_WaitForLastTask();等待标志位RTOFF被设置
		RTC_WaitForLastTask();						//确保上一次 RTC 的操作完成
		
		#endif
		
		BKP_WriteBackupRegister(BKP_DR1,0x1001);
		
		//在退出配置模式更新配置之后我们在备份区域BKP_DR1中写入0X5050代表我们已经初始化
		//过时钟了，下次开机(或复位)的时候，先读取 BKP_DR1的值，然后判断是否是0X5050来
		//决定是不是要配置
	}
	return 0;

}  
//RTC时钟中断
//每秒触发一次  
extern u16 tcnt; 
void RTC_IRQHandler(void)
{		 
	if (RTC_GetITStatus(RTC_IT_SEC) != RESET)//1秒钟中断
	{	
//        static int i=0;
//		POINT_COLOR=BLUE;
//		LCD_ShowString(30,130,200,16,16,(u8*)"Normal working hours:");	
//		LCD_ShowNum(200,130,i++,2,16);
//		LCD_ShowString(220,130,200,16,16,(u8*)"s");	

 	}
	if(RTC_GetITStatus(RTC_IT_ALR)!= RESET) //闹钟中断
	{
//		RTC_ClearITPendingBit(RTC_IT_ALR);  //清闹钟中断
	}
	//这里也可以不用清除溢出中断RTC_IT_OW，因为溢出时间是136年，一般普通系统不会工作136年，也就不用清除它，当然清除了更好，使程序更加严谨
	RTC_ClearITPendingBit(RTC_IT_OW|RTC_IT_SEC);
	RTC_WaitForLastTask();//这个操作必须有，保证上一次操作成功   						 	   	 
}

#define leapyear(a) (((a%4==0)&&(a%100!=0))||(a%400==0))
#define days_in_year(i) (leapyear(i)?366:365)
unsigned int days_in_month[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};

void Tim_Display(u32 TimeVal)
{
	unsigned  int days,hms;
	unsigned  int i;
	
	days = TimeVal/86400;
	hms =  TimeVal%86400;
	calendar.hour =  hms/3600;
	calendar.min  = (hms%3600)/60;
	calendar.sec  = (hms%3600)%60;
	
	for(i=1970;days>=days_in_year(i);i++)
	{
	   days-=days_in_year(i);	
	}
	calendar.w_year=i;
	
	if(leapyear(i))
		days_in_month[2]=29;
	else
		days_in_month[2]=28;
	
	for(i=1;days>=days_in_month[i];i++)
	{
	   days-=days_in_month[i];	
	}
	calendar.w_month=i;
	
	calendar.w_date= days+1;
    printf("%d-%d-%d  %d:%d:%d\n",calendar.w_year,calendar.w_month,calendar.w_date,calendar.hour,calendar.min,calendar.sec);
	
}
	





