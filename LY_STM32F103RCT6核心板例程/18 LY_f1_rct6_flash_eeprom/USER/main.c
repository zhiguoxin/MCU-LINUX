//FLSAH 存储器又称闪存，它与 EEPROM 都是掉电后数据不丢失的存储器，但 FLASH
//存储器容量普遍大于 EEPROM，现在基本取代了它的地位。我们生活中常用的 U 盘、SD
//卡、SSD 固态硬盘以及我们 STM32 芯片内部用于存储程序的设备，都是 FLASH 类型的存
//储器。在存储控制上，最主要的区别是 FLASH 芯片只能一大片一大片地擦写，而在“I2C
//章节”中我们了解到 EEPROM可以单个字节擦写。

/*
	参考野火STM32F103RCT6迷你板第43章节内容。

	在STM32 芯片内部有一个 FLASH 存储器，它主要用于存储代码，我们在电脑上编写
好应用程序后，使用下载器把编译后的代码文件烧录到该内部 FLASH 中，由于 FLASH 存
储器的内容在掉电后不会丢失，芯片重新上电复位后，内核可从内部 FLASH 中加载代码
并运行。

	除了使用外部的工具（如下载器）读写内部 FLASH 外，STM32 芯片在运行的时候，
也能对自身的内部 FLASH 进行读写，因此，若内部 FLASH 存储了应用程序后还有剩余的
空间，我们可以把它像外部 SPI-FLASH 那样利用起来，存储一些程序运行时产生的需要掉
电保存的数据。

	由于访问内部 FLASH 的速度要比外部的 SPI-FLASH 快得多，所以在紧急状态下常常
会使用内部 FLASH 存储关键记录；为了防止应用程序被抄袭，有的应用会禁止读写内部
FLASH 中的内容，或者在第一次运行时计算加密信息并记录到某些区域，然后删除自身的
部分加密代码，这些应用都涉及到内部 FLASH 的操作。

	STM32的内部 FLASH 包含主存储器、系统存储器以及选项字节区域。
	
	主存储器:
			一般我们说 STM32 内部 FLASH 的时候，都是指这个主存储器区域，它是存储用
			户应用程序的空间，芯片型号说明中的 256K FLASH、512K FLASH 都是指这个
			区域的大小。主存储器分为 256 页，每页大小为 2KB，共 512KB。这个分页的
			概念，实质就是FLASH 存储器的扇区，与其它 FLASH 一样，在写入数据前，要
			先按页（扇区）擦除。上一节中W25Q128，容量为16M字节（即 128M bit），分
			为256块(Block)，每一块的大小为64K字节，每块又分为16个扇区(Sector),每
			个扇区就是4K个字节。

			
	系统存储区:
			系统存储区是用户不能访问的区域，它在芯片出厂时已经固化了启动代码，它负
			责实现串口、USB以及 CAN等 ISP 烧录功能。
			
	选项字节:
			选项字节用于配置 FLASH 的读写保护、待机/停机复位、软件/硬件看门狗等功能，
			这部分共 16字节。可以通过修改 FLASH 的选项控制寄存器修改。
			
	加载及执行空间的基地址(Base)都是0x08000000，它正好是 STM32 内部 FLASH 的首地址，即 
STM32 的程序存储空间就直接是执行空间；它们的大小(Size)分别为 0x0000178c 及 0x00001760，
执行空间的 ROM 比较小的原因就是因为部分 RW-data 类型的变量被拷贝到 RAM 空间了；它们的
最大空间(Max)均为 0x00040000，即 256K字节，它指的是内部 FLASH 的最大空间。

--------------------------------------------------------------------------------------

本实验只有一个smartflash文件，其他的头文件现在用不到

--------------------------------------------------------------------------------------
*/


#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "tsensor.h"
#include "dma.h"
#include "myiic.h"
#include "24cxx.h"
#include "flash.h" 
#include "spi.h"
#include "stmflash.h" 
/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT6 SPI TEST"
#define EXAMPLE_DATE	"2020-12-30"

static void PrintfLogo(void);
static void PrintfHelp(void);
static void LcdPrintf(void);

//要写入到stm32内部flash的字符串数组
const u8 TEXT_Buffer[]={"STM32F103RCT6_FLASH_EEPROM_TEST"};//这里放了31个字节，AT24C02一共有256个字节。
#define SIZE sizeof(TEXT_Buffer)	
#define FLASH_SAVE_ADDR  0X08020000 //设置FLASH 保存地址(必须为偶数，且其值要大于本代码所占用FLASH的大小+0X08000000)

int main(void)
{	
	u8 key;
	u8 datatemp[SIZE];
	u32 FLASH_SIZE;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	delay_init();		//延时函数初始化 
	uart_init(115200);	//初始化串口，波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口
	LCD_Init();			//初始化与LCD连接的硬件接口
	PrintfLogo();		//串口打印例程名称和版本等信息
	PrintfHelp();		//串口打印操作提示
	LcdPrintf();		//LCD打印操作提示	 

  	POINT_COLOR=BLUE;			//设置字体为蓝色	  
	while(1)
	{
		key=KEY_Scan(0);
		if(key==WKUP_PRES)	//WK_UP 按下,写入STM32 FLASH
		{
			LCD_Fill(0,200,239,319,WHITE);//清除半屏    
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Write W25Q128....");
			STMFLASH_Write(FLASH_SAVE_ADDR,(u16*)TEXT_Buffer,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"W25Q64 Write Finished!");	//提示传送完成
		}
		if(key==KEY0_PRES)	//KEY0 按下,读取字符串并显示
		{
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Read W25Q128.... ");
			STMFLASH_Read(FLASH_SAVE_ADDR,(u16*)datatemp,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"The Data Readed Is:  ");	//提示传送完成
			LCD_ShowString(30,200,200,16,16,datatemp);						//显示读到的字符串
		}		   
	}
 	
}

//打印操作提示
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("WK_UP:Write  KEY0:Read\r\n");	
}

//功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
static void PrintfLogo(void)
{
	printf("\n*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

//功能说明: LCD显示屏显示提示信息
static void LcdPrintf(void)
{
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"FLASH_EEPROM_TEST"); 					 
	LCD_ShowString(30,140,200,16,16,(u8*)"WK_UP:Write  KEY0:Read");
}

//对内部 FLASH 的写入过程
//1. 解锁
//		由于内部 FLASH 空间主要存储的是应用程序，是非常关键的数据，为了防止误操作
//		修改了这些内容，芯片复位后默认会给控制寄存器 FLASH_CR 上锁，这个时候不允许设置
//		FLASH 的控制寄存器，从而不能修改 FLASH 中的内容。
//		所以对 FLASH 写入数据前，需要先给它解锁。解锁的操作步骤如下：
//
//		(1) 往 FPEC 键寄存器 FLASH_KEYR中写入 KEY1 = 0x45670123
//		(2) 再往 FPEC键寄存器 FLASH_KEYR中写入 KEY2 = 0xCDEF89AB
//
//2. 页擦除
//
//
//
//
//
//
//










