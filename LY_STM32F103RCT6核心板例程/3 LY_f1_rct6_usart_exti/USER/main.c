/*STM32的串口支持查询和中断两种方法
此处用的是中断的方法，主要是中断服务函数的编写，这里
有三种不同的中断服务函数的编写方法：正点原子、野火、计小新
注意：只有PC向单片机发送数据才会用到串口的的接收中断和中断服务函数，
stm32向pc打印数据不需要用到中断，直接应printf函数即可*/

#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"

/* 定义例程名和例程发布日期 */
#define EXAMPLE_NAME	"STM32F103RCT6_串口中断"
#define EXAMPLE_DATE	"2020-12-29"
#define DEMO_VER		"1.0"

static void PrintfLogo(void);
static void PrintfHelp(void);

int main(void)
{	
	u8 i,t,len;
	delay_init();//延时函数初始化
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组2	
    LED_Init();				//初始化与LED连接的硬件接口
	uart_init(115200);		//初始化串口，波特率为115200
	PrintfLogo();			//打印例程名称和版本等信息
	PrintfHelp();			//打印操作提示
    while(1)
	{	
		#if EN_ALIENTEK_USART1_IRQHandler
			if(USART_RX_STA&0x8000)
			{					   
				len=USART_RX_STA&0x3fff;//得到此次接收到的数据长度
				printf("\r\n您发送的消息为:\r\n");
				for(t=0;t<len;t++)
				{
					USART1->DR=USART_RX_BUF[t];
					while((USART1->SR&0X40)==0);//等待发送结束
				}
				printf("\r\n\r\n");//插入换行
				USART_RX_STA=0;
			}
		#endif
		
		#if EN_yeFire_USART1_IRQHandler 
			//野火的串口回显函数在中断服务函数中，所以在主函数的while函数中不写任何代码
		#endif
		
		#if EN_JiXiaoXin_USART1_IRQHandler
			if(ReceiveState==1)//如果接收到1帧数据
			{
				ReceiveState=0;
				i=0;
				while(RxCounter--)// 把接收到数据发送回串口
				{
					USART_SendData(USART1, aRxBuffer[i++]);	
					while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
				}
				RxCounter=0;
			}
		#endif		
	}	
}

/*
**************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参：无
*	返 回 值: 无
**************************************************************
*/
static void PrintfHelp(void)
{
	printf("串口中断:\r\n");
	printf("出口发送回显实验\r\n");
	
}
/*
***************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
*	形    参：无
*	返 回 值: 无
***************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印例程名称 */
	printf("* 例程版本   : %s\r\n", DEMO_VER);		/* 打印例程版本 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印例程日期 */

	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);

	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \n\r");	/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟 \r\n");
	printf("* Copyright www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}


