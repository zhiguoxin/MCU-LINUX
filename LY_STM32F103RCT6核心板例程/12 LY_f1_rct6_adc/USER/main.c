/**************************************************************************
STM32F103有两个ADC  ADC1和ADC2
		通道0	通道1	通道2	通道3	通道4	通道5	通道6	通道7	通道8	通道9
ADC1     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
ADC2     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
**************************************************************************************/
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"

int main(void)
{	
	
	u16 adc_A0,adc_integer;	
	float adc_V_A0;
	u8 lcd_id[12];			//存放LCD ID字符串	
	delay_init();	//延时函数初始化	  
	uart_init(115200);//串口初始化为9600
 	LED_Init();//LED端口初始化
	LCD_Init();
	ADC1_Init();	
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"TFTLCD TEST");
	LCD_ShowString(30,90,200,16,16,(u8*)"GOUGUO");
	LCD_ShowString(30,110,200,16,16,lcd_id);//显示LCD ID	      					 
	
	//显示提示信息
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(30,130,200,16,16,(u8*)"ADC_CH0_VAL:");	      
	LCD_ShowString(30,150,200,16,16,(u8*)"ADC_CH0_VOL:0.000V");
	
	while(1)
	{		
		adc_A0=Get_Adc_Average(ADC_Channel_0,10);		
		adc_V_A0=Get_Adc_Value(ADC_Channel_0);	//adc转换后得到的float
		adc_integer=adc_V_A0;					//float的整数给adc_integer 
		adc_V_A0-=adc_integer;					//得到小数点后的位数
		adc_V_A0*=1000;       					//将小数点后的位数变成整数

		LCD_ShowxNum(126,130,adc_A0,4,16,0);//显示ADC的值
		LCD_ShowxNum(126,150,adc_integer,1,16,0);//显示电压整数
		LCD_ShowxNum(142,150,adc_V_A0,3,16,0x80);//显示电压值

	}
}


