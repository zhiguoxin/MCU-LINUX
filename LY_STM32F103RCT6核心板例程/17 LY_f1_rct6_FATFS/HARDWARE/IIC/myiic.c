#include "myiic.h"
#include "delay.h"
 
 //关于IIC的资料，建议看野火STM32RCT6迷你版开发手册，讲解的比较清楚。
/* GPIO 模式采用I2C协议必须的开漏输出模式，开漏输出模式在输出高电平时实际
输出高阻态，当I2C该总线上所有设备都输出高阻态时，由外部的上拉电阻上拉为高
电平。另外，当STM32的GPIO配置成开漏输出模式时，它仍然可以通过读取GPIO 的输
入数据寄存器获取外部对引脚的输入电平，也就是说它同时具有浮空输入模式的功能，
因此在后面控制 SDA线对外输出电平或读取 SDA线的电平信号时不需要切换 GPIO的模式。*/

//初始化IIC
void IIC_Init(void)
{					     
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_I2C_PORT , ENABLE);		   
	GPIO_InitStructure.GPIO_Pin = I2C_SCL_PIN|I2C_SDA_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;//开漏输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIO_PORT_I2C, &GPIO_InitStructure);
 

}
//产生IIC起始信号
void IIC_Start(void)
{
	/* 当SCL高电平时，SDA出现一个下跳沿表示I2C总线启动信号 */
	I2C_SDA_1();
	I2C_SCL_1();
	delay_us(4);
	I2C_SDA_0();
	delay_us(4);
	I2C_SCL_0();
	delay_us(4);
}	  
//产生IIC停止信号
void IIC_Stop(void)
{
	/* 当SCL高电平时，SDA出现一个上跳沿表示I2C总线停止信号 */
	I2C_SDA_0();
	I2C_SCL_1();
	delay_us(4);
	I2C_SDA_1();						   	
}
					 				     
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答			  
void IIC_Send_Byte(u8 _ucByte)
{                        
 	uint8_t i;

	/* 先发送字节的高位bit7 */
	for (i = 0; i < 8; i++)
	{		
		if (_ucByte & 0x80)//程序对输入参数_ucByte和0x80“与”运算，判断其最高位的逻辑值，为1时控制SDA输出高电平
		{
			I2C_SDA_1();
		}
		else//为0则控制SDA输出低电平
		{
			I2C_SDA_0();
		}
		delay_us(2);//延时，以此保证 SDA 线输出的电平已稳定，再进行后续操作
		I2C_SCL_1();//控制 SCL线产生高低电平跳变，也就是产生 I2C协议中 SCL线的通讯时钟
		delay_us(2);//在SCL线高低电平之间有个延时，该延时期间SCL线维持高电平，根据I2C协议，此时数据有效，通讯的另一方会在此时读取 SDA 线的电平逻辑，高电平时接收到该位为数据 1，否则为 0
		I2C_SCL_0();
		if (i == 7)//在最后一位发送完成后(此时循环计数器i=7)，控制SDA线输出1（即高阻态），也就是说发送方释放SDA总线，等待接收方的应答信号
		{
			 I2C_SDA_1(); // 释放总线
		}
		_ucByte <<= 1;//一次循环体执行结束，_ucByte 左移一位以便下次循环发送下一位的数据
		delay_us(2);
	}
} 	    
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
u8 IIC_Read_Byte(void)
{
	uint8_t i;
	uint8_t value;//使用一个变量 value 暂存要接收的数据
	/* 读到第1个bit为数据的bit7 */
	value = 0;
	for (i = 0; i < 8; i++)
	{
		value <<= 1;//每次循环开始前先对value的值左移1位，以给value变量的bit0腾出空间，bit0将用于缓存最新接收到的数据位，一位一位地接收并移位，最后拼出完整的 8位数据；
		I2C_SCL_1();//控制 SCL线进行高低电平切换，输出 I2C 协议通讯用的时钟
		delay_us(2);
		if (I2C_SDA_READ())//读取SDA信号线的电平，若信号线为1，则value++，即把它的bit0置1
		{
			value++;
		}
		I2C_SCL_0();
		delay_us(2);//SCL线切换成低电平后，加入延时，以便接收端根据需要切换SDA线输出数据
	}
	return value;//循环结束后，value 变量中包含有1个字节数据，使用return把它作为函数返回值返回
}

//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
u8 IIC_Wait_Ack(void)
{
	uint8_t re;
	I2C_SDA_1();	/* CPU释放SDA总线 */
	delay_us(1);
	I2C_SCL_1();	/* CPU驱动SCL = 1, 此时器件会返回ACK应答 */
	delay_us(1);
	/*控制 SCL 信号线切换高低电平，产生一个时钟信号，根据I2C协议，
	此时接收方若把 SDA 设置为低电平，就表示返回一个“应答”信号，
	若 SDA 保持为高电平，则表示返回一个“非应答 ”信号；*/
	if (I2C_SDA_READ())	/* CPU读取SDA口线状态 */
	{
		re = 1;
	}
	else
	{
		re = 0;
	}
	I2C_SCL_0();
	delay_us(1);//在SCL切换高低电平之间，有个延时确保给予了足够的时间让接收方返回应答信号
	return re;//函数的最后返回 re的值，接收到响应时返回 0，未接收到响应时返回 1
} 
//产生ACK应答
//当STM32作为数据接收端，调用IIC_ReadByte 函数后，需要给发送端返回应答或非应答信号
void IIC_Ack(void)
{
	I2C_SDA_0();	/* CPU驱动SDA = 0 */
	delay_us(2);
	I2C_SCL_1();	/* CPU产生1个时钟 */
	delay_us(2);
	I2C_SCL_0();
	delay_us(2);
	I2C_SDA_1();	/* CPU释放SDA总线 */
}
//不产生ACK应答		    
void IIC_NAck(void)
{
	I2C_SDA_1();	/* CPU驱动SDA = 1 */
	delay_us(2);
	I2C_SCL_1();	/* CPU产生1个时钟 */
	delay_us(2);
	I2C_SCL_0();
	delay_us(1);	
}
