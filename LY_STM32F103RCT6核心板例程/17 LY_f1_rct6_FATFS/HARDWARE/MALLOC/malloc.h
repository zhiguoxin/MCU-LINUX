#ifndef __MALLOC_H
#define __MALLOC_H
#include "stm32f10x.h"


 
#ifndef NULL
#define NULL 0
#endif

//内存参数设定.
#define MEMORY_BLOCK_SIZE			32  	  						//内存块大小为32字节
#define MEMORY_MAX_SIZE				42*1024  						//最大管理内存 42K
#define MEMORY_ALLOC_TABLE_SIZE		MEMORY_MAX_SIZE/MEMORY_BLOCK_SIZE 	//内存表大小 1344字节
 
		 
//内存管理控制器
struct _m_mallco_device
{
	void (*init)(void);					//初始化
	u8 (*perused)(void);		  		//内存使用率
	u8 	*memorybase;					//内存池 
	u16 *memorymap; 					//内存管理状态表
	u8  memoryrdy; 						//内存管理是否就绪
};
extern struct _m_mallco_device mallco_device;	//在mallco.c里面定义

void lymemset(void *s,u8 c,u32 count);	//设置内存
void lymemcpy(void *des,void *src,u32 n);//复制内存     
void memory_init(void);					 //内存管理初始化函数(外/内部调用)
u32 mem_malloc(u32 size);		 		//内存分配(内部调用)
u8 mem_free(u32 offset);		 		//内存释放(内部调用)
u8 mem_perused(void);					//得内存使用率(外/内部调用) 
////////////////////////////////////////////////////////////////////////////////

//用户调用函数
void lyfree(void *ptr);  				//内存释放(外部调用)
void *lymalloc(u32 size);				//内存分配(外部调用)
void *lyrealloc(void *ptr,u32 size);	//重新分配内存(外部调用)
#endif













