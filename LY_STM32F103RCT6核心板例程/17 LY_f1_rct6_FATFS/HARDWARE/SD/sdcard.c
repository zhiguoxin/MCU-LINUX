#include "sys.h"
#include "sdcard.h"			   
#include "spi.h"
#include "usart.h"	
#include "lcd.h"

u8  SD_Type=0;//SD卡的类型 

/*******************************************************************************
* Function Name  : SPI_ControlLine
* Description    : SPI1模块初始化，
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SD_SPI_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(SD_SPI_CS_CLK, ENABLE );	 //PORTA时钟使能
	//配置SPI的CS引脚，普通IO即可
	GPIO_InitStructure.GPIO_Pin = SD_SPI_CS_PIN;	//SD卡的CS引脚接在PA3PA3
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SD_SPI_CS_PORT, &GPIO_InitStructure);	
	SPI1_Init();	
	SPI_SD_CS=1;
	
}
/*******************************************************************************
* Function Name  : SD_SPI_WriteByte
* Description    : 向SPI总线写一个字节
* Input          : None
* Output         : None
* Return         : None
	使用 SPI 收发一个字节时，利用库函数SPI_I2S_GetFlagStatus 检查收发缓冲区，当发
送缓冲区为空的时候调用 SPI_I2S_SendData发送一个字节，由于 SPI 协议是全双工的，即发
送一个字节的时候会同时接收到从机返回的一个字节内容，这时再调用 SPI_I2S_GetFlagStatus 
检查接收缓冲区，当数据存在的时候，调用 SPI_I2S_ReceiveData函数读取并返回。
*******************************************************************************/
u8 SD_SPI_WriteByte(u8 Data)
{		
	//等待至发送缓冲区为空	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); //检查指定的SPI标志位设置与否:发送缓存空标志位	
	SPI_I2S_SendData(SPI1, Data);									//通过SPI1这个模块发送一个字节的数据
	//SPI 是全双工协议，发送的时候同时会接收到数据,等待接收完成
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);//检查指定的SPI标志位设置与否:接受缓存非空标志位	  						    
	return SPI_I2S_ReceiveData(SPI1); 								//返回通过SPI1返回接收的数据					    
}
/*******************************************************************************
* Function Name  : SD_SPI_ReadByte
* Description    : 从SPI总线上读一个字节
* Input          : None
* Output         : None
* Return         : None
	SD_WriteByte 和 SD_ReadByte 这两个函数的主体区别不大，只是在发送数据时SD_WriteByte
发送的是输入参数 ，而SD_ReadByte 发送的是无意义的 0XFF。对于这个读取数据过程也要调用库函
数SPI_I2S_SendData发送一个无意义字节的原因是：SPI 通讯协议的时钟由主机产生(即STM32)，而
STM32通过调用 SPI_I2S_SendData 发送一个字节即可产生一个字节的时钟，SPI_I2S_SendData 函
数通过 MOSI 信号线发送的数据会被 SD 卡忽略，而发送数据的同时从设备(即 SD卡)在该时钟的驱动
下通过 MISO 信号线返回数据，所以此处的0xff是无意义的，可以替换成其它数据。可以尝试直接把
本代码SD_ReadByte 函数里发送的 0XFF直接改成其它数据进行实验测试，如把
SPI_I2S_SendData(SD_SPI, 0XFF);改成SPI_I2S_SendData(SD_SPI, 0x00);，这样修改程序是能正常
运行的。
*******************************************************************************/
u8 SD_SPI_ReadByte(void)
{ 
	//等待至发送缓冲区为空	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); //检查指定的SPI标志位设置与否:发送缓存空标志位	
	SPI_I2S_SendData(SPI1, 0XFF);									//通过SPI1这个模块发送一个字节的数据
	//SPI 是全双工协议，发送的时候同时会接收到数据,等待接收完成
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);//检查指定的SPI标志位设置与否:接受缓存非空标志位	  						    
	return SPI_I2S_ReceiveData(SPI1); 								//返回通过SPI1返回接收的数据	
}

/*******************************************************************************
* Function Name  : SD_SPI_SpeedLow
* Description    : SPI1设置速度为高速
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
void SD_SPI_SpeedLow(void)//SD卡初始化的时候,需要低速
{
 	SPI1_SetSpeed(SPI_BaudRatePrescaler_256);//设置到低速模式	
}
/*******************************************************************************
* Function Name  : SD_SPI_SpeedLow
* Description    : SPI1设置速度为高速
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
void SD_SPI_SpeedHigh(void)//SD卡正常工作的时候,可以高速了
{
 	SPI1_SetSpeed(SPI_BaudRatePrescaler_2);//设置到高速模式	
}
/*******************************************************************************
* Function Name  : SD_DisSelect
* Description    : 取消选择,释放SPI总线
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
void SD_DisSelect(void)
{
	SPI_SD_CS=1;
 	SD_SPI_WriteByte(0xff);//由我们的主控器 STM32 这款单片机连续发送 8 脉冲，来唤醒 SD
}
/*******************************************************************************
* Function Name  : SD_Select
* Description    : 选择sd卡,并且等待卡准备OK
* Input          : None 
* Output         : None
* Return         : u8
				   0,成功;
				   1,失败;
*******************************************************************************/
u8 SD_Select(void)
{
	SPI_SD_CS=0;//片选端置低，选中 SD 卡
	if(SD_WaitReady()==0)
		return 0;//等待成功
	SD_DisSelect();
		return 1;//等待失败
}
/*******************************************************************************
* Function Name  : SD_WaitReady
* Description    : 等待SD卡Ready准备好
* Input          : None
* Output         : None
* Return         : u8 
*                  0： 准备好了
*                  1： 失败;
*******************************************************************************/
u8 SD_WaitReady(void)
{
	u32 t=0;
	do
	{
		if(SD_SPI_ReadByte()==0XFF)return 0;//OK
		t++;		  	
	}while(t<0XFFFFFF);//等待 
	return 1;
}
/*******************************************************************************
* Function Name  : SD_GetResponse
* Description    : 等待SD卡回应
* Input          : u8 Response  要得到的回应值
* Output         : None
* Return         : u8 
*                  0： 		成功得到了该回应值
*                  other： 得到回应值失败
*******************************************************************************/
u8 SD_GetResponse(u8 Response)
{
	u16 Count=0xFFFF;//等待次数	   						  
	while ((SD_SPI_ReadByte()!=Response)&&Count)
		Count--;//等待得到准确的回应  	  
	if (Count==0)
		return MSD_RESPONSE_FAILURE;//得到回应失败  0xFF 
	else 
		return MSD_RESPONSE_NO_ERROR;//正确回应	0x00
}
/*******************************************************************************
* Function Name  : SD_RecvData
* Description    : 从sd卡读取一个数据包的内容
* Input          : u8   buf:数据缓存区
				   u16  len:要读取的数据长度.
* Output         : None
* Return         : u8 
*                  0： 	   成功
*                  other： 失败
*******************************************************************************/
u8 SD_RecvData(u8*buf,u16 len)
{			  	  
	if(SD_GetResponse(0xFE))
		return 1;//等待SD卡发回数据起始0xFE
    while(len--)//开始接收数据
    {
        *buf=SD_SPI_ReadByte();
        buf++;
    }
    //下面是2个伪CRC（dummy CRC）
    SD_SPI_WriteByte(0xFF);
    SD_SPI_WriteByte(0xFF);									  					    
    return 0;//读取成功
}
/*******************************************************************************
* Function Name  : SD_SendBlock
* Description    : 向sd卡写入一个数据包的内容 512字节
* Input          : u8   buf:数据缓存区
				   u8   cmd:指令
* Output         : None
* Return         : u8 
*                  0： 	   成功
*                  other： 失败
*******************************************************************************/	
u8 SD_SendBlock(u8*buf,u8 cmd)
{	
	u16 t;		  	  
	if(SD_WaitReady())return 1;//等待准备失效
	SD_SPI_WriteByte(cmd);
	if(cmd!=0XFD)//不是结束指令
	{
		for(t=0;t<512;t++)
		{
			SD_SPI_WriteByte(buf[t]);//提高速度,减少函数传参时间
		}
	    SD_SPI_WriteByte(0xFF);//忽略crc
	    SD_SPI_WriteByte(0xFF);
		t=SD_SPI_WriteByte(0xFF);//接收响应
		if((t&0x1F)!=0x05)
			return 2;//响应错误									  					    
	}						 									  					    
    return 0;//写入成功
}
/*******************************************************************************
* Function Name  : SD_SendCmd
* Description    : 向SD卡发送一个命令
* Input          : u8  cmd   命令 
*                  u32 arg   命令参数: 使用SPI总线驱动时，通过片选引脚来选择不同的卡，所以使用这些命令时地址可填
*									   充任意值。我们程序中设置的是0
*                  u8  crc   crc校验值:使用 SPI驱动时，命令中的 CRC7 校验默认是关闭的，即这 CRC7校验位中可以写
*						               入任意值而不影响通讯，仅在发送 CMD0 命令时需要强制带标准的 CRC7 校验。
*										我们程序中设置的是0X01
* Output         : None
* Return         : u8 r1 SD卡返回的响应
*******************************************************************************/
u8 SD_SendCmd(u8 cmd, u32 arg, u8 crc)
{
    u8 r1;	
	u8 Retry=0; 
	SD_DisSelect();//取消片选，把片选拉高
	if(SD_Select())//如果片选失败
		return 0XFF;//片选失效 
	//发送
    SD_SPI_WriteByte(cmd | 0x40);//分别写入命令
    SD_SPI_WriteByte(arg >> 24);
    SD_SPI_WriteByte(arg >> 16);
    SD_SPI_WriteByte(arg >> 8);
    SD_SPI_WriteByte(arg);	  
    SD_SPI_WriteByte(crc); 
	if(cmd==CMD12)//如果发送的是命令12，停止数据传输命令
		SD_SPI_WriteByte(0xff);//Skip a stuff byte when stop reading
    //等待响应，或超时退出
	Retry=31;
	do
	{
		r1=SD_SPI_ReadByte();//读SD的响应状态值
	}while((r1&0X80) && Retry--);	 
	//返回状态值
    return r1;
}
/*******************************************************************************
* Function Name  : SD_GetCID
* Description    : 获取SD卡的CID信息，包括制造商信息
* Input          : u8 *cid_data(存放CID的内存，至少16Byte）
* Output         : None
* Return         : u8 
*                  0：NO_ERR
*                  1：错误
*******************************************************************************/													   
u8 SD_GetCID(u8 *cid_data)
{
    u8 r1;	   
    //发CMD10命令，读CID
    r1=SD_SendCmd(CMD10,0,0x01);
    if(r1==0x00)
	{
		r1=SD_RecvData(cid_data,16);//接收16个字节的数据	 
    }
	SD_DisSelect();//取消片选
	if(r1)return 1;
	else return 0;
}

/*******************************************************************************
* Function Name  : SD_GetCSD
* Description    : 获取SD卡的CSD信息，包括容量和速度信息
* Input          : u8 *cid_data(存放CID的内存，至少16Byte）
* Output         : None
* Return         : u8 
*                  0：NO_ERR
*                  1：错误
*******************************************************************************/														   
u8 SD_GetCSD(u8 *csd_data)
{
    u8 r1;	 
    r1=SD_SendCmd(CMD9,0,0x01);//发CMD9命令，读CSD
    if(r1==0)
	{
    	r1=SD_RecvData(csd_data, 16);//接收16个字节的数据 
    }
	SD_DisSelect();//取消片选
	if(r1)return 1;
	else return 0;
}  
/*******************************************************************************
* Function Name  : SD_GetSectorCount
* Description    : 获取SD卡的总扇区数（扇区数）
* Input          : None
* Output         : None
* Return         : u8 
*                  0：取容量出错
*                  other：SD卡的容量(扇区数/512字节)  每扇区的字节数必为512，因为如果不是512，则初始化不能通过.		
*******************************************************************************/											  
u32 SD_GetSectorCount(void)
{
    u8 csd[16];
    u32 Capacity;  
    u8 n;
	u16 csize;  					    
	//取CSD信息，如果期间出错，返回0
    if(SD_GetCSD(csd)!=0) return 0;	    
    //如果为SDHC卡，按照下面方式计算
    if((csd[0]&0xC0)==0x40)	 //V2.00的卡
    {	
		csize = csd[9] + ((u16)csd[8] << 8) + 1;
		Capacity = (u32)csize << 10;//得到扇区数	 		   
    }else//V1.XX的卡
    {	
		n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
		csize = (csd[8] >> 6) + ((u16)csd[7] << 2) + ((u16)(csd[6] & 3) << 10) + 1;
		Capacity= (u32)csize << (n - 9);//得到扇区数   
    }
    return Capacity;
}
/*******************************************************************************
* Function Name  : SD_Init
* Description    : 初始化SD卡
* Input          : None
* Output         : None
* Return         : u8 
*                  0：NO_ERR
*                  1：TIME_OUT
*                  99：NO_CARD
*******************************************************************************/
u8 SD_Init(void)
{
    u8 r1;      // 存放SD卡的返回值
    u16 retry;  // 用来进行超时计数
    u8 buf[4];  
	u16 i;
	SD_SPI_Init();		//初始化IO
 	SD_SPI_SpeedLow();	//设置到低速模式 
	
	 //先产生至少74个脉冲，让SD卡自己初始化完成
 	for(i=0;i<10;i++)
	{
	    SD_SPI_WriteByte(0XFF);////80clks
	}	
	//-----------------SD卡复位到idle开始-----------------
	//循环连续发送CMD0，直到SD卡返回0x01,进入IDLE状态
	//超时则直接退出
	retry=0;
	do
	{
		r1=SD_SendCmd(CMD0,0,0x95);//进入IDLE状态，作用是让SD卡进入SPI模式。这里的CRC校验位0x95是固定的，不能修改
		retry++;
	}while((r1!=0X01) && (retry<20));//如果 SD 卡有正确的回应，代码就继续执行，如果没有回应程序就终止执行。
	 //跳出循环后，检查原因：初始化成功？or 重试超时？
	if(retry==20) return 1;   //超时返回1

	SD_Type=0;//默认无卡
	
	//下面是V2.0卡的初始化
    //其中需要读取OCR数据，判断是SD2.0还是SD2.0HC卡
	if(r1==0X01)
	{
		if(SD_SendCmd(CMD8,0x1AA,0x87)==1)//SD V2.0
		{
			//V2.0的卡，CMD8命令后会传回4字节的数据，要跳过再结束本命令
			buf[0]=SD_SPI_ReadByte();	//should be 0x00
			buf[1]=SD_SPI_ReadByte();	//should be 0x00
			buf[2]=SD_SPI_ReadByte();	//should be 0x01
			buf[3]=SD_SPI_ReadByte();	//should be 0xAA
			if(buf[2]==0X01&&buf[3]==0XAA)//判断卡是否支持2.7~3.6V的电压范围
			{
				retry=0XFFFE;
				//发卡初始化指令CMD55+CMD41
				do
				{
					SD_SendCmd(CMD55,0,0X01);	//发送CMD55
					r1=SD_SendCmd(CMD41,0x40000000,0X01);//发送CMD41
				}while(r1&&retry--);				
				
				//初始化指令发送完成，接下来获取OCR信息		   
				//-----------鉴别SD2.0卡版本开始-----------
				if(retry&&SD_SendCmd(CMD58,0,0X01)==0)//鉴别SD2.0卡版本开始
				{
					//读OCR指令发出后，紧接着是4字节的OCR信息
					buf[0]=SD_SPI_ReadByte();	
					buf[1]=SD_SPI_ReadByte();	
					buf[2]=SD_SPI_ReadByte();	
					buf[3]=SD_SPI_ReadByte();
					//检查接收到的OCR中的bit30位（CCS），确定其为SD2.0还是SDHC
					//如果CCS=1：为SDV2.0HC的2.0高容量卡   CCS=0：为SDV2.0的2.0版本的标准卡					
					if(buf[0]&0x40)
						SD_Type=SD_TYPE_V2HC;    //检查CCS
					else 
						SD_Type=SD_TYPE_V2;  
				//-----------鉴别SD2.0卡版本结束----------- 					
				}
			}			
		}		
		//如果卡片版本信息是v1.0版本的，即r1=0x05，则进行以下初始化
		else//SD V1.0/ MMC	V3
		{
			//先发CMD55，应返回0x01；否则出错
			r1 = SD_SendCmd(CMD55,0,0X01);		//发送CMD55
			   if(r1 != 0x01)
				return r1;	  
			//得到正确响应后，发ACMD41，应得到返回值0x00
			r1=SD_SendCmd(CMD41,0,0X01);	//发送CMD41
			if(r1<=1)
			{		
				SD_Type=SD_TYPE_V1;
				retry=0XFFFE;
				do //等待退出IDLE模式
				{
					SD_SendCmd(CMD55,0,0X01);	//发送CMD55
					r1=SD_SendCmd(CMD41,0,0X01);//发送CMD41
				}while(r1&&retry--);
			}else//MMC卡不支持CMD55+CMD41识别
			{
				SD_Type=SD_TYPE_MMC;//MMC V3
				retry=0XFFFE;
				do //等待退出IDLE模式
				{											    
					r1=SD_SendCmd(CMD1,0,0X01);//发送CMD1,发送MMC卡初始化命令
				}while(r1&&retry--);  
			}
			if(retry==0||SD_SendCmd(CMD16,512,0X01)!=0)
				SD_Type=SD_TYPE_ERR;//错误的卡
		}
	}
	SD_DisSelect();//取消片选
	SD_SPI_SpeedHigh();//高速
	if(SD_Type)
		return 0;
	else if(r1)
		return r1; 	   
	return 0xaa;//其他错误
}

/*******************************************************************************
* Function Name  : SD_ReadDisk
* Description    : 读SD卡数据
* Input          : buf:数据缓存区
*				   sector:扇区
*				   cnt:扇区数
* Output         : None
* Return         : u8 
*                  0：ok
*                  其他,失败.
*******************************************************************************/
u8 SD_ReadDisk(u8*buf,u32 sector,u8 cnt)
{
	u8 r1;
	if(SD_Type!=SD_TYPE_V2HC)
	{
		sector <<= 9;//转换为字节地址
	}
	if(cnt==1)
	{
		r1=SD_SendCmd(CMD17,sector,0X01);//读命令
		if(r1==0)//指令发送成功
		{
			r1=SD_RecvData(buf,512);//接收512个字节	   
		}
	}else
	{
		r1=SD_SendCmd(CMD18,sector,0X01);//连续读命令
		do
		{
			r1=SD_RecvData(buf,512);//接收512个字节	 
			buf+=512;  
		}while(--cnt && r1==0); 	
		SD_SendCmd(CMD12,0,0X01);	//发送停止命令
	}   
	SD_DisSelect();//取消片选
	return r1;
}
/*******************************************************************************
* Function Name  : SD_WriteDisk
* Description    : 向SD卡写数据
* Input          : buf:数据缓存区
*				   sector:扇区
*				   cnt:扇区数
* Output         : None
* Return         : u8 
*                  0：ok
*                  其他,失败.
*******************************************************************************/
u8 SD_WriteDisk(u8*buf,u32 sector,u8 cnt)
{
	u8 r1;
	if(SD_Type!=SD_TYPE_V2HC)
	{
		sector *= 512;//转换为字节地址
	}
	if(cnt==1)
	{
		r1=SD_SendCmd(CMD24,sector,0X01);//读命令
		if(r1==0)//指令发送成功
		{
			r1=SD_SendBlock(buf,0xFE);//写512个字节	   
		}
	}else
	{
		if(SD_Type!=SD_TYPE_MMC)
		{
			SD_SendCmd(CMD55,0,0X01);	
			SD_SendCmd(CMD23,cnt,0X01);//发送指令	
		}
 		r1=SD_SendCmd(CMD25,sector,0X01);//连续读命令
		if(r1==0)
		{
			do
			{
				r1=SD_SendBlock(buf,0xFC);//接收512个字节	 
				buf+=512;  
			}while(--cnt && r1==0);
			r1=SD_SendBlock(0,0xFD);//接收512个字节 
		}
	}   
	SD_DisSelect();//取消片选,释放SPI总线
	return r1;
}	
