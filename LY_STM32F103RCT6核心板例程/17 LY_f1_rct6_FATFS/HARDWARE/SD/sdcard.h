#ifndef _SDCARD_H_
#define _SDCARD_H_		 
#include "sys.h"	 
#include <stm32f10x.h>

/*STM32F10x 系列控制器只支持 SD 卡规范版本 2.0，即只支持标准容量SD 
和高容量 SDHC 标准卡，不支持超大容量 SDXC 标准卡，所以可以支持的最
高卡容量是 32GB。*/

//SD容量有8MB、16MB、32MB、64MB、128MB、256MB、512MB、1GB、2GB (磁盘格式FAT12、FAT16)
//SDHC容量有2GB、4GB、8GB、16GB、32GB(磁盘格式FAT32)
//SDXC容量有32GB、48GB、64GB、128GB、256GB(磁盘格式exFAT)



//CS(NSS)引脚 片选选普通GPIO即可
#define      SD_SPI_CS_CLK 	               	RCC_APB2Periph_GPIOA   
#define      SD_SPI_CS_PORT                 GPIOA
#define      SD_SPI_CS_PIN                  GPIO_Pin_3
 	
//开发板使用的是PA3作为SD卡的CS脚.
#define	SPI_SD_CS  PAout(3) 	//SD卡片选引脚

/*SD卡版本:SD V1.X（即SD标准卡）最大容量2GB
		   SD V2.0   2.0版本的标准卡，最多2GB
		   SD V2.0HC 2.0高容量卡，最多32GB
CMD8是在SD V2.0才出有的，以前V1.1的SD卡不支持，所以可以用来在初始化时识别SD卡版本*/

// SD卡类型定义  
#define SD_TYPE_ERR     0X00
#define SD_TYPE_MMC     0X01 //MMC的卡,比较古老，不多见了
#define SD_TYPE_V1      0X02 //V1.0的卡
#define SD_TYPE_V2      0X04 //SD V2.0   2.0版本的标准卡，最多2GB
#define SD_TYPE_V2HC    0X06 //SD V2.0HC 2.0高容量卡，最多32GB

// SD卡指令表  	 
//-------------------基本命令-----------------------------
#define CMD0    0       //卡复位 (应答格式：R1)
#define CMD1    1		//发送主机支持的电压操作范围
#define CMD8    8       //命令8 ，发送SD卡接口条件，包含主机支持的电压信息，并询问卡是否支持。
#define CMD9    9     	//命令9 ，读CSD数据   	(应答格式：R1)
#define CMD10   10    	//命令10，读CID数据   	(应答格式：R1)
#define CMD12   12      //命令12，停止数据传输  (应答格式：R1b)

//-------------------读取命令-----------------------------
#define CMD16   16      //命令16，设置SectorSize 应返回0x00   			(应答格式：R1)
#define CMD17   17      //命令17，读sector   				 			(应答格式：R1)
#define CMD18   18      //命令18，读Multi sector   						(应答格式：R1)
#define CMD23  	23      //命令23，设置多sector写入前预先擦除N个block    (应答格式：R1)

//-------------------写入命令-----------------------------
#define CMD24   24      //命令24，写sector    	   (应答格式：R1)
#define CMD25   25     //命令25，写Multi sector    (应答格式：R1)

#define CMD41  	41      //命令41，应返回0x00    (应答格式：R1)
#define CMD55   55      //命令55，应返回0x01    (应答格式：R1)
#define CMD58   58      //命令58，读OCR信息     (应答格式：R1)
#define CMD59   59      //命令59，使能/禁止CRC，应返回0x00    (应答格式：R1)

//数据写入回应字意义
#define MSD_DATA_OK                0x05
#define MSD_DATA_CRC_ERROR         0x0B
#define MSD_DATA_WRITE_ERROR       0x0D
#define MSD_DATA_OTHER_ERROR       0xFF
//SD卡回应标记字
#define MSD_RESPONSE_NO_ERROR      0x00
#define MSD_IN_IDLE_STATE          0x01
#define MSD_ERASE_RESET            0x02
#define MSD_ILLEGAL_COMMAND        0x04
#define MSD_COM_CRC_ERROR          0x08
#define MSD_ERASE_SEQUENCE_ERROR   0x10
#define MSD_ADDRESS_ERROR          0x20
#define MSD_PARAMETER_ERROR        0x40
#define MSD_RESPONSE_FAILURE       0xFF
 							   						 	 
				    	  

extern u8  SD_Type;			//SD卡的类型
//函数申明区 
u8 SD_SPI_WriteByte(u8 data);
u8 SD_SPI_ReadByte(void);
void SD_SPI_SpeedLow(void);
void SD_SPI_SpeedHigh(void);
u8 SD_WaitReady(void);							//等待SD卡准备
u8 SD_GetResponse(u8 Response);					//获得相应
u8 SD_Init(void);							//初始化
u8 SD_ReadDisk(u8*buf,u32 sector,u8 cnt);		//读块
u8 SD_WriteDisk(u8*buf,u32 sector,u8 cnt);		//写块
u32 SD_GetSectorCount(void);   					//读扇区数
u8 SD_GetCID(u8 *cid_data);                     //读SD卡CID
u8 SD_GetCSD(u8 *csd_data);                     //读SD卡CSD
 
#endif
