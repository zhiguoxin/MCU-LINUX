#ifndef __LCD_H
#define __LCD_H		
#include "sys.h"	 
#include "stdlib.h"
/***********************************************************************************************************************************
	*	@file  	  lcd_ili9341.h
	*   @date     2020-12-13		
	*	@author   果果
    ********************************************************************************************************************************
	*	【实验平台】 魔女开发板_STM32F103RC + KEIL5.27 + 2.8寸显示屏_ILI9341
	*
	*   【移植说明】 
	*    1：本代码使用在F103RC上，使用IO模块FSMC通信，注意引脚的修改
	*    2：汉字的显示，使用开发板上的外部FLASH中字库    
************************************************************************************************************************************/

//屏幕参数
#define LCD_WIDTH         240            // 屏幕宽度像素，注意：0~239
#define LCD_HIGH          320            // 屏幕高度像素，注意：0~319
#define LCD_DIR           0              // 四种显示方向，0-正竖屏，3-倒竖屏，5-正横屏, 6-倒横屏

//LCD重要参数集
typedef struct  
{										    
	u16 width;			//LCD 宽度
	u16 height;			//LCD 高度
	u16 id;				//LCD ID
	u8  dir;			//横屏还是竖屏控制：0-正竖屏，3-倒竖屏，5-正横屏, 6-倒横屏
	u16	wramcmd;		//开始写gram指令
	u16 setxcmd;		//设置x坐标指令
	u16 setycmd;		//设置y坐标指令	
}_lcd_dev; 	  

extern _lcd_dev lcddev;	//管理LCD重要参数   

//-----------------LCD端口定义---------------- 
#define	LCD_LED PCout(10) 				//LCD背光    		PC10 
 
#define	LCD_CS_HIGH GPIOC->BSRR=1<<9    //片选端口  		PC9
#define	LCD_RS_HIGH	GPIOC->BSRR=1<<8    //数据/命令 		PC8	   
#define	LCD_WR_HIGH	GPIOC->BSRR=1<<7    //写数据			PC7
#define	LCD_RD_HIGH	GPIOC->BSRR=1<<6    //读数据			PC6
								    
#define	LCD_CS_LOW  GPIOC->BRR=1<<9     //片选端口  		PC9
#define	LCD_RS_LOW	GPIOC->BRR=1<<8     //数据/命令			PC8	   
#define	LCD_WR_LOW	GPIOC->BRR=1<<7     //写数据			PC7
#define	LCD_RD_LOW	GPIOC->BRR=1<<6     //读数据			PC6   


//PB0~15,作为数据线
#define DATAOUT(x) GPIOB->ODR=x; //数据输出
#define DATAIN     GPIOB->IDR;   //数据输入	

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GRAY  			 0X8430 //灰色
//GUI颜色
#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
#define LIGHTGREEN     	 0X841F //浅绿色 
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色
#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)
	    															  
void LCD_Init(void);													   	//初始化
void LCD_DisplayOn(void);													//开显示
void LCD_DisplayOff(void);													//关显示
void LCD_Clear(u16 Color);	 												//清屏
void LCD_SetCursor(u16 xStart, u16 yStart,u16 xEnd,u16 yEnd);				//设置光标
void LCD_DrawPoint(u16 x,u16 y, u16 color);									//画点
void LCD_Fast_DrawPoint(u16 x,u16 y,u16 color);								//快速画点
u16  LCD_ReadPoint(u16 x,u16 y); 											//读点
void LCD_Draw_Circle(u16 x0,u16 y0,u8 r,u16 color);					    	//画圆
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2,u16 color);				//画线
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color);		   	//画矩形
void LCD_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color);				//填充指定颜色

void LCD_String(u16 x, u16 y, char* pFont, u8 size, u32 fColor, u32 bColor);
void LCD_Image(u16 x, u16 y, u16 width, u16 height, const u8 *image) ;

void LCD_WriteReg(u16 LCD_Reg, u16 LCD_RegValue);
 u16 LCD_ReadReg(u16 LCD_Reg);
void LCD_WriteRAM_Prepare(void);
void LCD_WriteRAM(u16 RGB_Code);		  
void LCD_Display_Dir(u8 dir);//设置屏幕显示方向


#endif

