/*
	1、STM32 的实时时钟（RTC）是一个独立的定时器。STM32 的 RTC 模块拥有一组连续计数的计数器，在相\应软件配置下，可提供时钟日历的功能。修改计数器的值可以重新设置系统当
前的时间和日期。

	2、RTC模块和时钟配置系统是在后备区域，即在系统复位或从待机模式唤醒后 RTC 的设置和时间维持不变。
但是在系统复位后，会自动禁止访问后备寄存器和 RTC，以防止对后备区域(BKP)的意外写操作。所以你要想操
作RTC模块的时间，在要设置时间之前， 先要取消备份区域（BKP）写保护。不然默认情况下，
他的值是不会改变的。

	3、RTC核心由一组可编程计数器组成， RTC的预分频模块+一个32位的可编程计数器
		3.1、RTC的预分频模块：包含了一个20位的可编程分频器(RTC 预分频器)，如果在RTC_CR寄存器中设置了
			相应的允许位，则在每个TR_CLK 周期中 RTC 产生一个中断(秒中断)
		3.2、一个32位的可编程计数器：可被初始化为当前的系统时间，一个 32 位的时钟计数器，按秒钟计算，
			可以记录 4294967296 秒，约合 136 年左右。	
	
	4、RTC 还有一个闹钟寄存器RTC_ALR，用于产生闹钟。RTC内核完全独立于RTC APB1 接口，而软件是通过APB1 
接口访问RTC的预分频值、计数器值和闹钟值的。

	5、备份寄存器是42个16位的寄存器（RCT6就是大容量的），可用来存储84个字节的用户应用程序数据。他们
处在备份域里，当VDD电源被切断，他们仍然由VBAT维持供电。即使系统在待机模式下被唤醒，或系统复位或电源
复位时，他们也不会被复位。


注意；板子的时间已经确定了，如果要想修改时间。请把rtc.c文件中的101-102和133-140行注释掉，把正确的时
	  间填入125行的RTC_Set()中下载即可修改。下载完成后再去掉注释即可。

*/
#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h" 
#include "lcd.h"

void Messages(void);
int main(void)
{	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	delay_init();
	uart_init(115200);	
	LED_Init();	
 	LCD_Init();				 	
 	Messages();	    
	while(1)
	{								    
		time_show();
		delay_ms(1000);					  
	}											    	
}

void Messages(void)
{
	u8 lcd_id[12];			//存放LCD ID字符串	
	while(RTC_Init())		//RTC初始化	，一定要初始化成功
	{ 
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC ERROR!   ");	
		delay_ms(800);
		LCD_ShowString(60,130,200,16,16,(u8*)"RTC Trying...");	
	}
	//显示提示信息
	POINT_COLOR=RED;
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Rtc Test");	
	LCD_ShowString(30,90,200,16,16,lcd_id);//显示LCD ID		    						
	//显示时间
	POINT_COLOR=BLUE;//设置字体为蓝色					 
	LCD_ShowString(30,130,200,16,16,(u8*)"    -  -     ");	   
	LCD_ShowString(30,170,200,16,16,(u8*)"  :  :  ");	
}


