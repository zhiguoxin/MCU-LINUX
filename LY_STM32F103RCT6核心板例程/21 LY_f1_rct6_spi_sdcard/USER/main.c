//FLSAH 存储器又称闪存，它与 EEPROM 都是掉电后数据不丢失的存储器，但 FLASH
//存储器容量普遍大于 EEPROM，现在基本取代了它的地位。我们生活中常用的 U 盘、SD
//卡、SSD 固态硬盘以及我们 STM32 芯片内部用于存储程序的设备，都是 FLASH 类型的存
//储器。在存储控制上，最主要的区别是 FLASH 芯片只能一大片一大片地擦写，而在“I2C
//章节”中我们了解到 EEPROM可以单个字节擦写。

#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "spi.h"
#include "sdcard.h" 


int main(void)
{	
	u32 sd_size;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	delay_init();		//延时函数初始化 
	KEY_Init();			//初始化与按键连接的硬件接口
	LCD_Init();			//初始化与LCD连接的硬件接口
	
	while(SD_Init())//检测不到SD卡
	{
		LCD_ShowString(30,170,200,16,16,(u8*)"SD Card Error!");
		delay_ms(500);
		LCD_ShowString(30,200,200,16,16,(u8*)"Please Check! ");
		delay_ms(500);
	}	
	//检测SD卡成功 											    
	LCD_ShowString(30,170,200,16,16,(u8*)"SD Card OK");
	LCD_ShowString(30,200,200,16,16,(u8*)"SD Card Size:        MB");
	
	sd_size=SD_GetSectorCount();//得到扇区数
	LCD_ShowNum(150,200,sd_size>>11,5,16);//显示SD卡容量
  
	while(1)
	{		   
	}
 	
}



