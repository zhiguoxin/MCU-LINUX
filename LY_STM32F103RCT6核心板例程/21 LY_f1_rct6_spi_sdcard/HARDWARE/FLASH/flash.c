#include "flash.h" 
#include "spi.h"
#include "delay.h"   

//W25Q128 驱动函数
u16 W25QXX_TYPE=W25Q128;//默认就是25Q128

//初始化SPI FLASH的IO口
void SPI_Flash_Init(void)
{	
	GPIO_InitTypeDef GPIO_InitStructure; 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	
	
	//配置SPI的CS引脚，普通IO即可
	GPIO_InitStructure.GPIO_Pin = FLASH_SPI_CS_PIN;//PA2
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(FLASH_SPI_CS_PORT, &GPIO_InitStructure);
	
	SPI_FLASH_CS=1;			//SPI FLASH不选中,这句话也可以不要，因为硬件上这个CS引脚接了上拉电阻，默认拉高
	SPI1_Init();			//初始化SPI
	SPI1_SetSpeed(SPI_BaudRatePrescaler_2);	//设置为36M时钟,高速模式
	W25QXX_TYPE=SPI_Flash_ReadID();//读取FLASH ID.
}  

//读取SPI_FLASH的状态寄存器
//BIT7  6   5   4   3   2   1   0
//SPR   RV  TB BP2 BP1 BP0 WEL BUSY
//SPR:默认0,状态寄存器保护位,配合WP使用
//TB,BP2,BP1,BP0:FLASH区域写保护设置
//WEL:写使能锁定
//BUSY:忙标记位(1,忙;0,空闲)
//默认:0x00
u8 SPI_Flash_ReadSR(void)   
{  
	u8 byte=0;   
	SPI_FLASH_CS=0;                            //使能器件   
	SPI1_ReadWriteByte(CMD_W25X_ReadStatusReg);    //发送读取状态寄存器命令	
	byte=SPI1_ReadWriteByte(0Xff);             //读取一个字节  
	SPI_FLASH_CS=1;                            //取消片选     
	return byte;   
} 
//写SPI_FLASH状态寄存器
//只有SPR,TB,BP2,BP1,BP0(bit 7,5,4,3,2)可以写!!!
void SPI_FLASH_Write_SR(u8 sr)   
{   
	SPI_FLASH_CS=0;                            //使能器件   
	SPI1_ReadWriteByte(CMD_W25X_WriteStatusReg);   //发送写取状态寄存器命令    
	SPI1_ReadWriteByte(sr);               		//写入一个字节  
	SPI_FLASH_CS=1;                            //取消片选     	      
}   
//SPI_FLASH写使能	
//将WEL置位   
void SPI_FLASH_Write_Enable(void)   
{
	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_WriteEnable);      //发送写使能  
	SPI_FLASH_CS=1;                            //取消片选     	      
} 
//SPI_FLASH写禁止	
//将WEL清零  
void SPI_FLASH_Write_Disable(void)   
{  
	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_WriteDisable);     //发送写禁止指令    
	SPI_FLASH_CS=1;                            //取消片选     	      
} 			    
//读取芯片ID
//返回值如下:				   
//0XEF13,表示芯片型号为W25Q80  
//0XEF14,表示芯片型号为W25Q16    
//0XEF15,表示芯片型号为W25Q32  
//0XEF16,表示芯片型号为W25Q64 
//0XEF17,表示芯片型号为W25Q128 
u16 SPI_Flash_ReadID(void)
{	
	u16 Temp = 0, Temp0 = 0, Temp1 = 0;	
	SPI_FLASH_CS=0;	
	SPI1_ReadWriteByte(0x90);//发送JEDEC指令，读取ID	
	SPI1_ReadWriteByte(0x00);	
	SPI1_ReadWriteByte(0x00);
	SPI1_ReadWriteByte(0x00);
	Temp0 = SPI1_ReadWriteByte(0xFF);//读取一个字节数据	
	Temp1 = SPI1_ReadWriteByte(0xFF);//读取一个字节数据	
	Temp = (Temp0 << 8) | Temp1;//把数据组合起来，作为函数的返回值
	SPI_FLASH_CS=1;				    
	return Temp;
}   		    
//读取SPI FLASH数据  
//在指定地址开始读取指定长度的数据
//pBuffer:存储读出数据的指针
//ReadAddr:开始读取的地址(24bit)
//NumByteToRead:要读取的字节数(最大 16 x 1024 x 1024)
void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead)   
{ 
 	u16 i;    												    
	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_ReadData);         //发送读取命令   
    SPI1_ReadWriteByte((ReadAddr& 0xFF0000)>>16);  	//发送扇区地址的高8bit   
    SPI1_ReadWriteByte((ReadAddr& 0xFF00)>>8);   	//发送扇区地址的中间8bit 
    SPI1_ReadWriteByte( ReadAddr& 0xFF);   			//发送扇区地址的低8bit 
    for(i=0;i<NumByteToRead;i++)
	{ 
        pBuffer[i]=SPI1_ReadWriteByte(0XFF);   //循环读数  
    }
	SPI_FLASH_CS=1;                            //取消片选     	      
}  
//SPI在一页(0~65535)内写入少于256个字节的数据
//在指定地址开始写入最大256字节的数据
//pBuffer:要写入数据的指针
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大16x1024x1024),该数不应该超过该页的剩余字节数!!!	 
void SPI_Flash_Write_Page(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)
{
 	u16 i;  
    SPI_FLASH_Write_Enable();                  //SET WEL 
	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_PageProgram);      //发送写页命令   
    SPI1_ReadWriteByte((WriteAddr& 0xFF0000)>>16);  	//发送扇区地址的高8bit   
    SPI1_ReadWriteByte((WriteAddr& 0xFF00)>>8);   	//发送扇区地址的中间8bit 
    SPI1_ReadWriteByte( WriteAddr& 0xFF);   			//发送扇区地址的低8bit 
    for(i=0;i<NumByteToWrite;i++)SPI1_ReadWriteByte(pBuffer[i]);//循环写数  
	SPI_FLASH_CS=1;                            //取消片选 
	SPI_FLASH_WaitForWriteEnd();			   //等待写入结束
} 
//无检验写SPI FLASH 
//必须确保所写的地址范围内的数据全部为0XFF,否则在非0XFF处写入的数据将失败!
//具有自动换页功能 
//在指定地址开始写入指定长度的数据,但是要确保地址不越界!
//pBuffer:数据存储区
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大65535)
//CHECK OK
void SPI_Flash_Write_NoCheck(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
{ 			 		 
	u16 pageremain;	   
	pageremain=256-WriteAddr%256; //单页剩余的字节数		 	    
	if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//不大于256个字节
	while(1)
	{	   
		SPI_Flash_Write_Page(pBuffer,WriteAddr,pageremain);
		if(NumByteToWrite==pageremain)break;//写入结束了
	 	else //NumByteToWrite>pageremain
		{
			pBuffer+=pageremain;
			WriteAddr+=pageremain;	

			NumByteToWrite-=pageremain;			  //减去已经写入了的字节数
			if(NumByteToWrite>256)pageremain=256; //一次可以写入256个字节
			else pageremain=NumByteToWrite; 	  //不够256个字节了
		}
	};	    
} 
//不定量数据写入
//应用的时候我们常常要写入不定量的数据，直接调用“页写入”函数并不是特别方便，
//所以我们在它的基础上编写了“不定量数据写入”的函数。对FLASH写入数据，调用本
//函数写入数据前需要先擦除扇区

//在指定地址开始写入指定长度的数据.该函数带擦除操作!

//pBuffer:要写入数据的指针
//WriteAddr:开始写入的地址(24bit)
//NumByteToWrite:要写入的字节数(最大16 x 1024 x 1024) 

//此处用了条件编译 1为正点原子的SPI_Flash_Write代码，0为野火SPI_Flash_Write代码
//对于代码理解难度来看，野火的代码更容易理解,参考野火mini板读写串行FLASH章节代码
#if 0 
u8 SPI_FLASH_BUF[4096];
void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite)   
{ 
	u32 secpos;
	u16 secoff;
	u16 secremain;	   
 	u16 i;
	/*mod运算求余，若writeAddr是4096整数倍，运算结果secpos值为0*/
	secpos=WriteAddr/4096;//扇区地址 0~16x1024x1024 for W25Q128
	/*差secoff个数据值，刚好可以对齐到页地址,也就是在扇区内的偏移*/
	secoff=WriteAddr%4096;//在扇区内的偏移
	/*mod运算求余，计算出剩余不满一页的字节数*/
	secremain=4096-secoff;//扇区剩余空间大小
	if(NumByteToWrite<=secremain)secremain=NumByteToWrite;//不大于4096个字节
	while(1) 
	{	
		SPI_Flash_Read(SPI_FLASH_BUF,secpos*4096,4096);//读出整个扇区的内容
		for(i=0;i<secremain;i++)//校验数据
		{
			if(SPI_FLASH_BUF[secoff+i]!=0XFF)break;//需要擦除  	  
		}
		if(i<secremain)//需要擦除
		{
			SPI_Flash_Erase_Sector(secpos);//擦除这个扇区
			for(i=0;i<secremain;i++)	   //复制
			{
				SPI_FLASH_BUF[i+secoff]=pBuffer[i];	  
			}
			SPI_Flash_Write_NoCheck(SPI_FLASH_BUF,secpos*4096,4096);//写入整个扇区  

		}else SPI_Flash_Write_NoCheck(pBuffer,WriteAddr,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   
		if(NumByteToWrite==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;//扇区地址增1
			secoff=0;//偏移位置为0 	 

		   	pBuffer+=secremain;  //指针偏移
			WriteAddr+=secremain;//写地址偏移	   
		   	NumByteToWrite-=secremain;				//字节数递减
			if(NumByteToWrite>4096)secremain=4096;	//下一个扇区还是写不完
			else secremain=NumByteToWrite;			//下一个扇区可以写完了
		}	 
	};	 	 
}
#else
void SPI_Flash_Write(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
	u8 NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;	
	
	Addr = WriteAddr % 4096;//mod运算求余，若writeAddr是4096整数倍，运算结果Addr值为0	
	NumOfPage =  NumByteToWrite / 4096;//计算出要写多少整数扇区	
	NumOfSingle = NumByteToWrite % 4096;//mod运算求余，计算出剩余不满一扇区的字节数	
	count = 4096 - Addr;//差count个数据值，刚好可以对齐到扇区地址	
	if (Addr == 0)//Addr=0,则WriteAddr刚好按扇区对齐或者说小于一个扇区
	{	
		//NumByteToWrite < 4096，写入的字符串大小长度小于一个扇区(4096个字节)的大小，如22
		if (NumOfPage == 0) 
		{
			SPI_Flash_Write_Page(pBuffer, WriteAddr, NumByteToWrite);
		}
		else //NumByteToWrite > 4096，写入的字符串大小长度大与一个扇区(4096个字节)的大小，如4098
		{ 
			//先把整数扇区都写了
			while (NumOfPage--)
			{
				SPI_Flash_Write_Page(pBuffer, WriteAddr, 4096);
				WriteAddr +=  4096;
				pBuffer += 4096;
			}
			//若有多余的不满一扇区的数据，把它写完
			SPI_Flash_Write_Page(pBuffer, WriteAddr, NumOfSingle);
		}
	}
	//若地址与 4096 不对齐
	else //Addr不等于0,则要写入的WriteAddr地址与4096不对齐
	{
		//NumByteToWrite < 4096
		if (NumOfPage == 0)//大小不够一个扇区，如22
		{
			//当前页剩余的count个位置比NumOfSingle小，一扇区写不完
			if (NumOfSingle > count) 
			{
				temp = NumOfSingle - count;
				//先写满当前扇区
				SPI_Flash_Write_Page(pBuffer, WriteAddr, count);
				
				WriteAddr +=  count;
				pBuffer += count;
				//再写剩余的数据
				SPI_Flash_Write_Page(pBuffer, WriteAddr, temp);
			}
			else //当前扇区剩余的count个位置能写完NumOfSingle个数据
			{
				SPI_Flash_Write_Page(pBuffer, WriteAddr, NumByteToWrite);
			}
		}
		else //NumByteToWrite > 4096  //大小够一个扇区，而且还超出一点点，如4098
		{
			//地址不对齐多出的count分开处理，不加入这个运算
			NumByteToWrite -= count;
			NumOfPage =  NumByteToWrite / 4096;
			NumOfSingle = NumByteToWrite % 4096;
			
			//先写完count个数据，为的是让下一次要写的地址对齐
			SPI_Flash_Write_Page(pBuffer, WriteAddr, count);
			
			//接下来就重复地址对齐的情况 */
			WriteAddr +=  count;
			pBuffer += count;
			//把整数扇区都写了*/
			while (NumOfPage--)
			{
				SPI_Flash_Write_Page(pBuffer, WriteAddr, 4096);
				WriteAddr +=  1096;
				pBuffer += 4096;
			}
			//若有多余的不满一扇区的数据，把它写完
			if (NumOfSingle != 0)
			{
				SPI_Flash_Write_Page(pBuffer, WriteAddr, NumOfSingle);
			}
		}
	}
}
#endif

//擦除整个芯片
//整片擦除时间:
//W25X16:25s 
//W25X32:40s 
//W25X64:40s 
//等待时间超长...
void SPI_Flash_Erase_Chip(void)   
{                                             
    SPI_FLASH_Write_Enable();                  //SET WEL 
    SPI_FLASH_WaitForWriteEnd();   
  	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_ChipErase);        //发送片擦除命令  
	SPI_FLASH_CS=1;                            //取消片选     	      
	SPI_FLASH_WaitForWriteEnd();   				   //等待芯片擦除结束
}   
//擦除一个扇区，一个扇区4K=4096个字节，2^12=4096
//Dst_Addr:扇区地址 0~16 x 1024 x 1024 for W25Q128
//擦除一个山区的最少时间:150ms
void SPI_Flash_Erase_Sector(u32 Dst_Addr)   
{   
	Dst_Addr*=4096;
    SPI_FLASH_Write_Enable();                  //SET WEL 	 
    SPI_FLASH_WaitForWriteEnd();   
  	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_SectorErase);      //发送扇区擦除指令 
    SPI1_ReadWriteByte((Dst_Addr& 0xFF0000)>>16);  	//发送扇区地址的高8bit   
    SPI1_ReadWriteByte((Dst_Addr& 0xFF00)>>8);   	//发送扇区地址的中间8bit 
    SPI1_ReadWriteByte( Dst_Addr& 0xFF);   			//发送扇区地址的低8bit 
	SPI_FLASH_CS=1;                            //取消片选     	      
    SPI_FLASH_WaitForWriteEnd();   				//等待擦除完成
}  
//等待空闲，等待WIP(BUSY)标志被置0，即等待到FLASH内部数据写入完毕
void SPI_FLASH_WaitForWriteEnd(void)   
{   
	while ((SPI_Flash_ReadSR()&0x01)==0x01);   // 等待BUSY位清空
}  
//进入掉电模式
void SPI_Flash_PowerDown(void)   
{ 
  	SPI_FLASH_CS=0;                            //使能器件   
    SPI1_ReadWriteByte(CMD_W25X_PowerDown);    //发送掉电命令  
	SPI_FLASH_CS=1;                            //取消片选     	      
    delay_us(3);                               //等待TPD  
}   
//唤醒
void SPI_Flash_WAKEUP(void)   
{  
	SPI_FLASH_CS=0;                            //使能器件   
	SPI1_ReadWriteByte(CMD_W25X_ReleasePowerDown);   //  send W25X_PowerDown command 0xAB    
	SPI_FLASH_CS=1;                            //取消片选     	      
	delay_us(3);                               //等待TRES1
}

