#ifndef __FLASH_H
#define __FLASH_H			    
#include "sys.h" 

//CS(NSS)引脚 片选选普通GPIO即可
#define      FLASH_SPI_CS_APBxClock_FUN        RCC_APB2PeriphClockCmd
#define      FLASH_SPI_CS_CLK 	               RCC_APB2Periph_GPIOA   
#define      FLASH_SPI_CS_PORT                 GPIOA
#define      FLASH_SPI_CS_PIN                  GPIO_Pin_2

#define	SPI_FLASH_CS PAout(2) //选中FLASH	

//1、FLASH 芯片中还有 WP(IO2) 和 HOLD(IO)引脚。WP 引脚可控制写保护功能，当该引脚为低电
//平时，禁止写入数据。我们直接接电源3.3V，不使用写保护功能。HOLD 引脚可用于暂停通讯，
//该引脚为低电平时，通讯暂停，数据输出引脚输出高阻抗状态，时钟和数据输入引脚无效。
//我们直接接电源3.3V，不使用通讯暂停功能。


//2、4Kbytes为一个Sector,16个扇区为1个Block
//W25Q64  容量为8M字节（即 64M bit）,  分为128块(Block)，每一块的大小为64K字节，每块又分为16个扇区(Sector),那么每个扇区就是4K个字节
//W25Q128 容量为16M字节（即 128M bit），分为256块(Block)，每一块的大小为64K字节，每块又分为16个扇区(Sector),那么每个扇区就是4K个字节

//3、W25Qxx的最小擦除单位为一个扇区，也就是每一次必须擦除4K字节。这样我们需要给W25Qxx开辟至少4K的缓冲区，这样对SRAM的要求
//比较高，要求芯片必须有4K以上的SRAM才能很好的操作。

//4、所有的FLASH我们在写之前都要擦出对应的扇区，擦除后的数据是0XFF，如果不是0XFF，就需要擦除。

//W25X系列/Q系列芯片列表
#define W25Q80 	0XEF13 	
#define W25Q16 	0XEF14
#define W25Q32 	0XEF15
#define W25Q64 	0XEF16
#define W25Q128	0XEF17



extern u16 W25QXX_TYPE;//定义我们使用的flash芯片型号

//8个bit位为一个字节(B),1024个字节(B)就是1KB。上面我们说了W25Qxx的最小擦除单位为一个扇区，也就是每一次必须擦除4K字节。
//这样我们需要给W25Qxx开辟至少4K的缓冲区,也就是4096（u8 SPI_FLASH_BUF[4096];）。
extern u8 SPI_FLASH_BUF[4096];

//5、为什么在SPI读字节的时候要发送0XFF?
//因为SPI的时钟是来自于主设备，而且SPI是收发双向的。发送0XFF，就是为了在接受数据的时候，
//给从设备提供8个时钟，这样，从设备才能够发出信息给主设备。至于主设备发0XFF，还是别
//的信息，都无所谓，只要不是从设备的命令就行。

//指令表
#define CMD_W25X_WriteEnable		0x06 //发送写使能  
#define CMD_W25X_WriteDisable		0x04 //发送写禁止指令    
#define CMD_W25X_ReadStatusReg		0x05 //发送读取状态寄存器命令	
#define CMD_W25X_WriteStatusReg		0x01 //发送写取状态寄存器命令
#define CMD_W25X_ReadData			0x03 //发送读取命令   
#define CMD_W25X_FastReadData		0x0B ////快速读数据
#define CMD_W25X_PageProgram		0x02 //发送写页命令   
#define CMD_W25X_BlockErase			0xD8 //发送写页擦除 
#define CMD_W25X_SectorErase		0x20 //发送扇区擦除指令 
#define CMD_W25X_ChipErase			0xC7 //发送片擦除命令  
#define CMD_W25X_PowerDown			0xB9 //发送掉电命令  
#define CMD_W25X_ReleasePowerDown	0xAB //解除低功耗或高性能模式 
#define CMD_W25X_DeviceID			0xAB 
#define CMD_W25X_ManufactDeviceID	0x90 //芯片ID(校验用)，W25Q64为EF16，W25Q128为EF17
#define CMD_W25X_JedecDeviceID		0x9F //JEDECID，不知道什么东西



void SPI_Flash_Init(void);
u16  SPI_Flash_ReadID(void);  	    //读取FLASH ID
u8	 SPI_Flash_ReadSR(void);        //读取状态寄存器 
void SPI_FLASH_Write_SR(u8 sr);  	//写状态寄存器
void SPI_FLASH_Write_Enable(void);  //写使能 
void SPI_FLASH_Write_Disable(void);	//写保护
void SPI_Flash_Read(u8* pBuffer,u32 ReadAddr,u16 NumByteToRead);   //读取flash
void SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);//写入flash
void SPI_Flash_Erase_Chip(void);    	  //整片擦除
void SPI_Flash_Erase_Sector(u32 Dst_Addr);//扇区擦除
void SPI_FLASH_WaitForWriteEnd(void);     //等待空闲
void SPI_Flash_PowerDown(void);           //进入掉电模式
void SPI_Flash_WAKEUP(void);			  //唤醒
#endif
















