#include "spi.h"

SPI_InitTypeDef  SPI_InitStructure;	//这个一定要设置为全局变量，因为SPI1_SetSpeed()函数以及MMC_SD.C中的设置SPI速度的函数都没有加这一句话

//以下是SPI模块的初始化代码，配置成主机模式，访问SD Card/W25X16/24L01/JF24C							  
//这里针是对SPI1的初始化 不包括片选引脚CS，用哪个设备在哪个设备的.c文件中初始化片选引脚
void SPI1_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_SPI1, ENABLE);	

	//配置SPI的 SCK引脚
	GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;//复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(FLASH_SPI_SCK_PORT, &GPIO_InitStructure);

	//配置SPI的 MISO引脚
	GPIO_InitStructure.GPIO_Pin = FLASH_SPI_MISO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;//复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(FLASH_SPI_MISO_PORT, &GPIO_InitStructure);

	//配置SPI的 MOSI引脚
	GPIO_InitStructure.GPIO_Pin = FLASH_SPI_MOSI_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;//复用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(FLASH_SPI_MOSI_PORT, &GPIO_InitStructure);

	GPIO_SetBits(GPIOA,FLASH_SPI_SCK_PIN|FLASH_SPI_MISO_PIN|FLASH_SPI_MOSI_PIN);
	
	//设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex; 
	//设置SPI工作模式:设置为主SPI	
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	//设置SPI的数据大小:SPI发送接收8位帧结构（一次传8位数据）
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	//串行时钟的极性: 时钟悬空高(没有数据传输时时钟的空闲状态电平)
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	//串行时钟的相位：在SCK的第二个边沿采集数据	
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	//NSS信号由硬件(NSS管脚)还是软件(使用SSI位)管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	//定义波特率预分频的值:波特率预分频值为256。初始化默认状态下设置为256分频，速度最小，后面可以再改
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
	//指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始(高位数据在前)
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;	
	//CRC值计算的多项式
	SPI_InitStructure.SPI_CRCPolynomial = 7;	
	SPI_Init(SPI1, &SPI_InitStructure); 
	
	SPI_Cmd(SPI1, ENABLE); //使能SPI外设	
	SPI1_ReadWriteByte(0xff);//启动传输		 
}   
//SPI 速度设置函数
//SpeedSet:
//SPI_BaudRatePrescaler_2   2分频   (SPI 36M@sys 72M)
//SPI_BaudRatePrescaler_4   4分频   (SPI 18M@sys 72M)
//SPI_BaudRatePrescaler_8   8分频   (SPI 9M@sys 72M)
//SPI_BaudRatePrescaler_16  16分频  (SPI 4.5M@sys 72M)
//SPI_BaudRatePrescaler_32  32分频  (SPI 2M@sys 72M)
//SPI_BaudRatePrescaler_256 256分频 (SPI 281.25K@sys 72M)  
void SPI1_SetSpeed(u8 SpeedSet)
{
	SPI_InitStructure.SPI_BaudRatePrescaler = SpeedSet ;
  	SPI_Init(SPI1, &SPI_InitStructure);
	SPI_Cmd(SPI1,ENABLE);
} 


/*******************************************************************************
* Function Name  : SPI1_ReadWriteByte
* Description    : 向SPI总线写一个字节或从SPI总线读出一个字节，SPIx读取和写入都是这一个函数
* Input          : 要写入的字节数据
* Output         : None
* Return         : 要读出的字节数据
	使用 SPI 收发一个字节时，利用库函数SPI_I2S_GetFlagStatus 检查收发缓冲区，当发
送缓冲区为空的时候调用 SPI_I2S_SendData发送一个字节，由于 SPI 协议是全双工的，即发
送一个字节的时候会同时接收到从机返回的一个字节内容，这时再调用 SPI_I2S_GetFlagStatus 
检查接收缓冲区，当数据存在的时候，调用 SPI_I2S_ReceiveData函数读取并返回。
*******************************************************************************/
u8 SPI1_ReadWriteByte(u8 Data)
{			
	//等待至发送缓冲区为空	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); //检查指定的SPI标志位设置与否:发送缓存空标志位	
	SPI_I2S_SendData(SPI1, Data);									//通过SPI1这个模块发送一个字节的数据
	//SPI 是全双工协议，发送的时候同时会接收到数据,等待接收完成
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);//检查指定的SPI标志位设置与否:接受缓存非空标志位	  						    
	return SPI_I2S_ReceiveData(SPI1); 								//返回通过SPI1返回接收的数据				    
}

