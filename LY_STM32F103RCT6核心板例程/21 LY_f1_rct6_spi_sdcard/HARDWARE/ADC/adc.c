/**************************************************************************
* 文 件 名： adc.c
* 使用芯片： STM32F103C8T6
* 硬件连接： ADC--PB0
* 描述功能： ADC采集电压，采集的电压不能超过3.3V否则会烧坏单片机的IO口
* 日    期： 2018/12/22
* 作    者： 刘尧
* B     站： 果果小师弟
* 版    本： V1.0
* 修改记录： 无
**************************************************************************/
#include "adc.h"
#include "delay.h"
void  ADC1_Init(void)
{    
 	ADC_InitTypeDef ADC_InitStructure; 
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE );	  //使能ADC1通道时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE );	  //使能GPIO通道时钟
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);   //设置ADC分频因子6   72M/6=12,ADC最大时间不能超过14M
	
	//PA0作为模拟通道输入引脚(ADC1_CH0)                        
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;		//模拟输入引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	
	ADC_DeInit(ADC1);  //复位ADC1,将外设 ADC1 的全部寄存器重设为缺省值
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;	//ADC工作模式:ADC1和ADC2工作在独立模式
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;	//模数转换工作在单通道模式
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	//模数转换工作在单次转换模式
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;	//转换由软件而不是外部触发启动
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;	//ADC数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel = 1;	//顺序进行规则转换的ADC通道的数目
	ADC_Init(ADC1, &ADC_InitStructure);	//根据ADC_InitStruct中指定的参数初始化外设ADCx的寄存器 
	
	ADC_Cmd(ADC1, ENABLE);						//使能指定的ADC1
	ADC_ResetCalibration(ADC1);					//使能复位校准  	 
	while(ADC_GetResetCalibrationStatus(ADC1));	//等待复位校准结束	
	ADC_StartCalibration(ADC1);	 				//开启AD校准
	while(ADC_GetCalibrationStatus(ADC1));	 	//等待校准结束
}		

/**************************************************************************
函数功能：AD采样
入口参数：ADC1 的通道
返回  值：AD转换结果
**************************************************************************/
u16 Get_Adc(u8 ch)   
{
	//设置指定ADC的规则组通道，一个序列，采样时间
	ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_239Cycles5 );//ADC1,ADC通道,采样时间为239.5周期		
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);								//使能指定的ADC1的软件转换启动功能		 
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC ));						//等待转换结束
	return ADC_GetConversionValue(ADC1);								//返回最近一次ADC1规则组的转换结果
}
/*https://blog.csdn.net/qq_33379514/article/details/85760772
采样频率是可以设置固定值的。
如果设置PLCK2为6分频，那么ADCCLK为 72M/6=12MHz。
最大的采样周期是239.5个周期，那么最小采样频率：12M/(239.5+12.5)=48KHz*/

/**************************************************************************
函数功能：读取ADC的具体值
入口参数：通道数和采集多少次求平均
返回  值：电池电压 单位MV
**************************************************************************/
u16 Get_Adc_Average(u8 ch,u8 times)
{
	u32 temp_val=0;
	u8 t;
	for(t=0;t<times;t++)
	{ 
		temp_val+=Get_Adc(ch);
	    delay_ms(5);
	}
	return temp_val/times;
}
/**************************************************************************
函数功能：读取ADC1的电压
入口参数：无
返回  值：电池电压 单位V
**************************************************************************/
float Get_Adc_Value(u8 ch)
{
	float temp_val=0;

	temp_val=Get_Adc(ch)*(3.3/4096);//STM32的ADC有12位2^12=4096,3.3代表量程是0-3.3V

	return temp_val;
}	

/**************************************************************************
函数功能：读取ADC1的电压
入口参数：无
返回  值：电池电压 单位MV
**************************************************************************/
u16 Get_Adc_Value_mV(u8 ch)
{
	u16 temp_val=0;

	temp_val=Get_Adc(ch)*3.3/4096*1000;//stm32的ADC有12位2^12=4096,3.3代表量程是0-3.3V

	return temp_val;
}




