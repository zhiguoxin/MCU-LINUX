#ifndef __ADC_H
#define __ADC_H	
#include "sys.h"


void ADC1_Init(void);
u16 Get_Adc(u8 ch);
u16 Get_Adc_Average(u8 ch,u8 times);
float Get_Adc_Value(u8 ch);
u16 Get_Adc_Value_mV(u8 ch);
#endif 
