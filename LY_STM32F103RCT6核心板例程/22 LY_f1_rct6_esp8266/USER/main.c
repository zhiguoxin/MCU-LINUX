#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h" 
#include "usart2.h"	
#include "lcd.h"
#include "flash.h" 
#include "spi.h"
#include "common.h"  
#include "touch.h"

int main(void)
{	
	delay_init();		//延时函数初始化 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	
	uart_init(9600);	//初始化串口，波特率为115200
	USART2_Init(9600);  //初始化串口2波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口	
	SPI_Flash_Init();  	//SPI FLASH W25Q128初始化 
	LCD_Init();			//初始化与LCD连接的硬件接口
	TP_Init();
	atk_8266_test();		//进入ATK_ESP8266测试
 	
}


