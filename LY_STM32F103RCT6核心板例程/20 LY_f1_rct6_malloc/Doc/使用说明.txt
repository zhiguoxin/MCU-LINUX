一、memset
void *memset(void *s, int ch, size_t n);

函数解释：将s中当前位置后面的n个字节(typedef unsigned int size_t)用 ch 替换并返回 s 。
memset：作用是在一段内存块中填充某个给定的值，它是对较大的结构体或数组进行清零操作的一种最快方。
memset()函数原型是
               extern void *memset(void *buffer, int c, int count) 
			   buffer：为指针或是数组,
				c：是赋给buffer的值,
				count：是buffer的长度.
memset() 函数常用于内存空间初始化。如：
char str[100];
memset(str,0,100);
memset的作用就是把str这个数组中的值初始化为0

二、sprintf
# include<stdio.h>
int sprintf(char *str, char *format ,...);

功能：
	根据参数format字符串来转换并格式化数据，然后将结果输出到str指定的空间中，直到出现字符串结束符'\0'为止。
参数：

参数列表
	str：字符串首地址
	format：这是字符串，包含了要被写入到字符串str的文本，可以包含嵌入的format标签
    ...：根据不同的format字符串，函数可能需要一系列的附加参数
返回值：
	成功：实际格式化字符的个数
	失败：-1
例子：	
编译环境为VS2017,是在c环境下编译，同时为了不使sprintf函数出错，需要 右键单击工程名——>属性——>C/C++——>常规——>SDL检查（将SDL检查改为否）
	#include<stdio.h>
	#include<string.h>
	#define a "hello"
	#define b "world"
	int main()
	{
		//1.格式化字符串 
		char buf[1024];
		sprintf(buf, "hello %s", "world");
		printf("buf:%s\n", buf);
		printf("len:%d\n\n", strlen(buf));

		//2.拼接字符串 
		char temp[1024];
		char *s1 = "hello";
		char *s2 = "world";
		memset(temp, 0, 1024);
		sprintf(temp, "%s %s", s1,s2);
		printf("buf:%s\n", temp);
		printf("len:%d\n\n", strlen(temp));

		//2.1拼接字符串 
		char str[1024];
		sprintf(str, "%s %s", a, b);
		printf("buf:%s\n", str);
		printf("len%d\n\n", strlen(str));

		//3.数字转换成字符串打印出来
		char num[1024];
		int number = 666;
		memset(num, 0, 1024);
		sprintf(num, "%d", number);
		printf("buf:%s\n", num);
		printf("len:%d\n\n", strlen(num));

		//4.设置宽度右对齐
		char buffer[1024];
		memset(buffer, 0, 1024);
		sprintf(buffer, "%8d", number);
		printf("buf:%s\n", buffer);
		printf("len:%d\n\n", strlen(buffer));

		//5.设置宽度左对齐
		memset(buffer, 0, 1024);
		sprintf(buffer, "%-8d", number);
		printf("buf:%s\n", buffer);
		printf("len:%d\n\n", strlen(buffer));

		//6.转换成16进制字符串 小写
		memset(buffer, 0, 1024);
		sprintf(buffer, "0x%x", number);
		printf("buf:%s\n", buffer);
		printf("len:%d\n\n", strlen(buffer));

		//6.转换成8进制字符串
		memset(buffer, 0, 1024);
		sprintf(buffer, "0%o", number);
		printf("buf:%s\n", buffer);
		printf("len:%d\n\n", strlen(buffer));
	}