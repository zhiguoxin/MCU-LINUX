/*------------------------------------------------------------------------------------
 硬件平台:
 			主控器: STM32F103RCT6 64K RAM 256K ROM
			屏幕器: 2.8寸 触摸显示屏 ILI9341 LCD彩色TFT液晶模块
			分辨率: 240X320 16位色(RGB565)
 软件平台:
 			开发环境: RealView MDK-ARM uVision5.27
			C编译器 : ARMCC
			ASM编译器:ARMASM
			连接器:   ARMLINK
			底层驱动: 各个外设驱动程序
	
	ROM Size = Code + RO-data + RW-data
	RAM Size = RW-data + ZI-data
	Program Size: Code=10656 RO-data=6440 RW-data=60 ZI-data=1052   
	
	=>代码占用 FLASH 大小为17096字节(10656+6440),也就约等于16.69K
			   SRAM  大小为1112字节(60+1052),也就约等于1.08K
	
	uart_init 函数，不能去掉，因为在 LCD_Init 函数里面调用了 printf，所以一旦你去掉这个初始化，就会死机了	
-------------------------------------------------------------------------------------*/   
#include "sys.h"

#include "key.h"
#include "delay.h"
#include "usart.h"
#include "malloc.h" 
#include "lcd.h"

/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT6_内存管理"
#define EXAMPLE_DATE	"2020-01-09"

static void PrintfLogo(void);
static void PrintfHelp(void);

int main(void)
{
	u8 key;		 
	u8 i=0;	    
	u8 *p=0;
	u8 *tp=0;
	u8 paddr[18];			//存放P Addr:+p地址的ASCII值 
	delay_init();	   		//延时函数初始化
	uart_init(115200);		//初始化串口，波特率为115200
	LCD_Init();				//初始化与LCD连接的硬件接口
	KEY_Init();				//初始化与按键连接的硬件接口
	PrintfLogo();			//打印例程名称和版本等信息
	PrintfHelp();			//打印操作提示
	memory_init();			//初始化内存池
	
	POINT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(60,50,200,16,16,(u8 *)"LY_STM32");	
	LCD_ShowString(60,70,200,16,16,(u8 *)"MALLOC TEST");	
	LCD_ShowString(60,130,200,16,16,(u8 *)"KEY0:Malloc");
	LCD_ShowString(60,150,200,16,16,(u8 *)"KEY1:Write Data");
	LCD_ShowString(60,170,200,16,16,(u8 *)"WK_UP:Free");

	POINT_COLOR=BLUE;//设置字体为蓝色  
	LCD_ShowString(60,190,200,16,16,(u8 *)"SRAM USED:   %");  
  	while(1)
	{	
		key=KEY_Scan(0);//不支持连按	
		switch(key)
		{
			case 0:					//没有按键按下	
				break;
			case 1:					//KEY0按下
				p=lymalloc(2048);	//申请2K字节
				if(p!=NULL)
					sprintf((char*)p,"Memory Malloc Test%03d",i);//向p写入一些内容
				break;
			case 2:					//KEY1按下	   
				if(p!=NULL)
				{
					sprintf((char*)p,"Memory Malloc Test%03d",i);//更新显示内容 	 
					LCD_ShowString(60,250,200,16,16,p);			 //显示P的内容
				}
				break;
			case 3:					//WK_UP按下	  
				lyfree(p);			//释放内存
				p=0;				//指向空地址
				break; 
		}
		if(tp!=p)
		{
			tp=p;
			sprintf((char*)paddr,"P Addr:0X%08X",(u32)tp);
			LCD_ShowString(60,230,200,16,16,paddr);	//显示p的地址
			if(p)LCD_ShowString(60,250,200,16,16,p);//显示P的内容
		    else LCD_Fill(60,250,239,266,WHITE);	//p=0,清除显示
		}
		delay_ms(10);   
		i++;
		if((i%20)==0)
		{
			LCD_ShowNum(60+80,190,mem_perused(),3,16);//显示内存使用率
 		}
	}									    	
}

/*
**************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参：无
*	返 回 值: 无
**************************************************************
*/
static void PrintfHelp(void)
{	
}
/*
***************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
*	形    参：无
*	返 回 值: 无
***************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \n\r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

