/**************************************************************************
STM32F103有两个ADC  ADC1和ADC2
		通道0	通道1	通道2	通道3	通道4	通道5	通道6	通道7	通道8	通道9
ADC1     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
ADC2     PA0	 PA1     PA2     PA3     PA4     PA5     PA6     PA7     PB0     PB1
//STM32F103RCT6的DAC引脚资源表：https://blog.csdn.net/muyidian/article/details/79000721

STM32F103RCT6有两个DAC，DAC1对应PA4,DAC2对应PA5  

我们将利用按键控制 STM32内部DAC1(PA4)来输出电压，通过ADC1的通道1(PA1)采集DAC的输出电压，
在LCD模块上面显示ADC获取到的电压值以及DAC的设定输出电压值等信息.

**************************************************************************************/
#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "dac.h"
int main(void)
{	
	u16 adcx;
	float temp;
 	u8 t=0;	 
	u16 dacval=0;//局部变量
	u8 key;
	u8 lcd_id[12];
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
	delay_init();			//延时函数初始化	
	uart_init(115200);		//串口初始化为115200
 	LED_Init();				//初始化与LED连接的硬件接口
	KEY_Init();				//按键初始化
	LCD_Init();				//初始化LCD 	
	ADC1_Init();			//ADC初始化
	Dac1_Init();			//DAC通道1初始化	
	
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"DAC TEST");
	LCD_ShowString(30,110,200,16,16,lcd_id);//显示LCD ID
	LCD_ShowString(30,140,200,16,16,(u8*)"WK_UP:+  KEY0:-");		
	LCD_ShowString(20,300,200,16,16,(u8*)"Copyright@liuyao-blog.cn");	
	//显示提示信息
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(30,170,200,16,16,(u8*)"DAC VAL:");	      
	LCD_ShowString(30,200,200,16,16,(u8*)"DAC VOL:0.000V");	      
	LCD_ShowString(30,230,200,16,16,(u8*)"ADC VOL:0.000V");	  
	
	while(1)
	{		
		t++;
		key=KEY_Scan(0);			  
		if(key==WKUP_PRES)//如果WKUP按下
		{		 
			if(dacval<4000)
				dacval+=200;
			DAC_SetChannel1Data(DAC_Align_12b_R, dacval);
		}else if(key==KEY0_PRES)//如果KEY0按下
		{
			if(dacval>200)
				dacval-=200;
			else dacval=0;
			DAC_SetChannel1Data(DAC_Align_12b_R, dacval);
		}	 
		if(t==10||key==KEY0_PRES||key==WKUP_PRES) 	//如果WKUP或者KEY1两个按键其中有一个按下了,或者定时时间到了
		{	  
 			adcx=DAC_GetDataOutputValue(DAC_Channel_1);//读出DAC的值
			
			LCD_ShowxNum(94,170,adcx,4,16,0);     	 //显示DAC寄存器值 adcx等于dacval(假设adcx=1400)
			temp=(float)adcx*(3.3/4096);			 //得到DAC电压值(temp=1400*3.3/4096=1.127)
			adcx=temp;								 //(adcx=1)
 			LCD_ShowxNum(94,200,adcx,1,16,0);     	//显示电压值整数部分
 			temp-=adcx; 							//(temp=temp-adcx=0.127)
			temp*=1000;								//(temp=temp*1000=127)
			LCD_ShowxNum(110,200,temp,3,16,0X80); 	//显示电压值的小数部分
			
 			adcx=Get_Adc_Average(ADC_Channel_3,10);	//得到ADC转换值	  
			temp=(float)adcx*(3.3/4096);			//得到ADC电压值
			adcx=temp;
 			LCD_ShowxNum(94,230,adcx,1,16,0);     	//显示电压值整数部分
 			temp-=adcx;
			temp*=1000;
			LCD_ShowxNum(110,230,temp,3,16,0X80); 	//显示电压值的小数部分  
			t=0;
		}	    
		delay_ms(10);
	}
}


