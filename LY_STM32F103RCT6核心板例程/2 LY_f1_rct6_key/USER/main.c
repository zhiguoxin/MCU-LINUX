#include "led.h"
#include "delay.h"
#include "sys.h"
#include "key.h"
#include "usart.h"
/* 定义例程名和例程发布日期 */
#define EXAMPLE_NAME	"STM32F103RCT6_按键控制LED小灯"
#define EXAMPLE_DATE	"2020-12-29"
#define DEMO_VER		"1.0"

static void PrintfLogo(void);
static void PrintfHelp(void);

 int main(void)
 {	
	u8 t=0;	  
	delay_init();	    	//延时函数初始化
	uart_init(115200); 		//初始化串口，波特率为115200
	LED_Init();		  	 	//初始化与LED连接的硬件接口
	KEY_Init();          	//初始化与按键连接的硬件接口	 
	LED0=0;					//点亮LED
	PrintfLogo();			//打印例程名称和版本等信息
	PrintfHelp();			//打印操作提示
	while(1)
	{
		t=KEY_Scan(0);		//得到键值
		switch(t)
		{				 
			case KEY0_PRES:
				LED0=!LED0;
				break;
			case KEY1_PRES:
				LED1=!LED1;
				break;
			case WKUP_PRES:				
				LED0=!LED0;
				LED1=!LED1;
				break;
			default:
				delay_ms(10);	
		} 
	}		 
}
/*
**************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参：无
*	返 回 值: 无
**************************************************************
*/
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("按键控制LED小灯\r\n");
	printf("     KEY0_PRES LED0\r\n");
	printf("     KEY1_PRES LED1\r\n");	
	printf("     WKUP_PRES LED1和LED2\r\n");		
}
/*
***************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
*	形    参：无
*	返 回 值: 无
***************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印例程名称 */
	printf("* 例程版本   : %s\r\n", DEMO_VER);		/* 打印例程版本 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印例程日期 */

	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);

	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \n\r");	/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟 \r\n");
	printf("* Copyright www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

