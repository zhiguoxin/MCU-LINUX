#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "tsensor.h"
	
int main(void)
{	
	
	u16 adcx;
	float temp,temperate;	
	u8 lcd_id[12];
	delay_init();
	uart_init(115200);
 	LED_Init();
	LCD_Init();
	ADC1_Init();
 	T_Adc_Init();		  		//ADC初始化	 
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组	
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"Tsensor TEST");
	LCD_ShowString(30,110,200,16,16,lcd_id);//显示LCD ID	      					 
	
	//显示提示信息
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(30,130,200,16,16,(u8*)"TEMP_VAL:");	      
	LCD_ShowString(30,150,200,16,16,(u8*)"TEMP_VOL:0.000V");	      
	LCD_ShowString(30,170,200,16,16,(u8*)"TEMPERATE:00.00C");	
	
	while(1)
	{		
		adcx=T_Get_Adc_Average(ADC_CH_TEMP,10);
		LCD_ShowxNum(102,130,adcx,4,16,0);//显示ADC的值
		
		temp=(float)adcx*(3.3/4096);
		temperate=temp;//保存温度传感器的电压值
		adcx=temp;		
		temp-=(u8)temp;				    			//减掉整数部分
		LCD_ShowxNum(102,150,adcx,1,16,0);     		//显示电压值整数部分		
		LCD_ShowxNum(118,150,temp*1000,3,16,0X80);	//显示电压小数部分
		
 		temperate=(1.43-temperate)/0.0043+25;		//计算出当前温度值	 
		LCD_ShowxNum(110,170,(u8)temperate,2,16,0); //显示温度整数部分
		temperate-=(u8)temperate;	  
		LCD_ShowxNum(134,170,temperate*100,2,16,0X80);//显示温度小数部分
		delay_ms(250);

	}
}


