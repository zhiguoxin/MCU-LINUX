#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "lcd.h"
#include "flash.h" 
#include "spi.h"


extern const unsigned char  gImage_c[78800];

int main(void)
{	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	delay_init();		//延时函数初始化 
	uart_init(115200);	//初始化串口，波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口	
	SPI_Flash_Init();  	//SPI FLASH W25Q128初始化 
	LCD_Init();			//初始化与LCD连接的硬件接口
	
	LCD_Color_Fill(0, 0,  lcddev.width,  lcddev.height, BLACK);	
    LCD_String(55,10,"果果小师弟", 24, WHITE, BLACK);	
	LCD_DrawLine(0,45,239,45, WHITE);
	LCD_Image (20,70, 200, 197, gImage_c); // 图片显示函数
	LCD_String(45,280,"微信扫码关注！", 24, RED ,BLACK);
	while(1)// while函数死循环，不能让main函数运行结束，否则会产生硬件错误
	{
		delay_ms(500);
        LED0=!LED0;//红色LED 每1秒闪灭一次，以监察系统正常工作 		
	}
 	
}


