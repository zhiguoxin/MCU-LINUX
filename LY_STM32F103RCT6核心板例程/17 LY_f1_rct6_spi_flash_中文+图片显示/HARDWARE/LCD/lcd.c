#include "lcd.h"
#include "stdlib.h"
#include "font.h" 
#include "usart.h"
#include "delay.h"	 
#include "flash.h"



_lcd_dev lcddev;
/*******************************************************************************
* Function Name  : LCD_WR_Order
* Description    : 向LCD发送寄存器地址(指令)
* Input          : data:寄存器地址(指令)
* Output         : None
* Return         : None
*******************************************************************************/
void LCD_WR_Order(u16 data)
{ 
	LCD_RS_LOW;		// RS=0:指令，RS=1:数据
 	LCD_CS_LOW; 	// 片选信号拉低，选定设备
	DATAOUT(data);  // 赋值数据
	LCD_WR_LOW; 	// WR=0：设备可以读取数据，WR=1：设备等待读取数据
	LCD_WR_HIGH; 
 	LCD_CS_HIGH; 	// 片选信号拉高，
}

/*******************************************************************************
* Function Name  : LCD_WR_DATA
* Description    : 写数据函数，可以替代LCD_WR_DATAX宏,拿时间换空间.
* Input          : data:寄存器值
* Output         : None
* Return         : None
*******************************************************************************/
#if 1
void LCD_WR_DATA(u16 data)
{
	LCD_RS_HIGH;		// RS高: 数据值， RS低: 寄存器地址值
	LCD_CS_LOW;			// 片选 
	GPIOB->ODR = data;  // PB0~15作为数据线
	LCD_WR_LOW;			// 写数据
	LCD_WR_HIGH;		// 写数据
	LCD_CS_HIGH; 		// 取消片选
}
#else
//写数据函数
#define LCD_WR_DATA(data)\
		{\
			LCD_RS_HIGH;\
			LCD_CS_LOW;\
			GPIOB->ODR = data;\
			LCD_WR_LOW;\
			LCD_WR_HIGH;\
			LCD_CS_HIGH;\
		}
#endif

/*******************************************************************************
* Function Name  : LCD_RD_DATA
* Description    : 读LCD数据
* Input          : None
* Output         : None
* Return         : 读到的值
*******************************************************************************/
u16 LCD_RD_DATA(void)
{										   
	u16 reg;
	//把PB0~15设置为：上拉输入
 	GPIOB->CRL=0X88888888; //PB0-7  上拉输入
	GPIOB->CRH=0X88888888; //PB8-15 上拉输入
	GPIOB->ODR=0X0000;     //全部输出0

	//读取数据 (读寄存器时,并不需要读2次)
	LCD_RS_HIGH;
	LCD_CS_LOW;  //片选
	//读取数据(读寄存器时,并不需要读2次)
	LCD_RD_LOW;				   
	reg=DATAIN;  
	LCD_RD_HIGH;
	LCD_CS_HIGH; //取消片选

	// 把PB0~15设置为：上拉输入
	GPIOB->CRL=0X33333333; //PB0-7  上拉输出
	GPIOB->CRH=0X33333333; //PB8-15 上拉输出
	GPIOB->ODR=0XFFFF;    //全部输出高
	return reg;  
}

/*******************************************************************************
* Function Name  : LCD_WriteReg
* Description    : 写寄存器
* Input          : LCD_Reg:寄存器编号，LCD_RegValue:要写入的值
* Output         : None
* Return         : None
*******************************************************************************/
void LCD_WriteReg(u16 LCD_Reg,u16 LCD_RegValue)
{	
	LCD_WR_Order(LCD_Reg);  
	LCD_WR_DATA(LCD_RegValue);	    		 
}   
/*******************************************************************************
* Function Name  : LCD_ReadReg
* Description    : 读寄存器的值
* Input          : LCD_Reg:寄存器编号
* Output         : None
* Return         : 读到的值
*******************************************************************************/
u16 LCD_ReadReg(u16 LCD_Reg)
{										   
 	LCD_WR_Order(LCD_Reg);  //写入要读的寄存器号  
	return LCD_RD_DATA(); 
} 
/*******************************************************************************
* Function Name  : LCD_BGR2RGB
* Description    : 从ILI93xx读出的数据为GBR格式，而我们写入的时候为RGB格式。通过该函数转换
* Input          : c:GBR格式的颜色值
* Output         : None
* Return         : RGB格式的颜色值
*******************************************************************************/
u16 LCD_BGR2RGB(u16 c)
{
	u16  r,g,b,rgb;   
	b=(c>>0)&0x1f;
	g=(c>>5)&0x3f;
	r=(c>>11)&0x1f;	 
	rgb=(b<<11)+(g<<5)+(r<<0);		 
	return(rgb);
}	
//当mdk -O1时间优化时需要设置
//延时i
void opt_delay(u8 i)
{
	while(i--);
}
/*******************************************************************************
* Function Name  : LCD_ReadPoint
* Description    : 读取个某点的颜色值
* Input          : x,y:坐标
* Output         : None
* Return         : 此点的颜色
*******************************************************************************/
u16 LCD_ReadPoint(u16 x,u16 y)
{
 	u16 r,g,b;
	if(x>=lcddev.width||y>=lcddev.height)
		return 0;	//超过了范围,直接返回		   
	LCD_SetCursor(x,y,x+1, y+1);
	LCD_WR_Order(0X2E);//9341/6804/3510/1963 发送读GRAM指令
	
	GPIOB->CRL=0X88888888; 							//PB0-7  上拉输入
	GPIOB->CRH=0X88888888; 							//PB8-15 上拉输入
	GPIOB->ODR=0XFFFF;     							//全部输出高

	LCD_RS_HIGH;
	LCD_CS_LOW;	
    
	//读取数据(读GRAM时,第一次为假读)	
	LCD_RD_LOW;		   
	opt_delay(2);									//延时		
 	r=DATAIN;  										//实际坐标颜色 
	LCD_RD_HIGH;

 	//dummy READ
	LCD_RD_LOW;					   
	opt_delay(2);//延时					   
 	r=DATAIN;  	//实际坐标颜色
	LCD_RD_HIGH;	
	 
	LCD_RD_LOW;					   
	opt_delay(2);//延时			   
	b=DATAIN;//读取蓝色值  	  
	LCD_RD_HIGH;
	
	g=r&0XFF;//对于9341,第一次读取的是RG的值,R在前,G在后,各占8位
	g<<=8;
	 
	LCD_CS_HIGH;
	GPIOB->CRL=0X33333333; 		//PB0-7  上拉输出
	GPIOB->CRH=0X33333333; 		//PB8-15 上拉输出
	GPIOB->ODR=0XFFFF;    		//全部输出高  
	
	return (((r>>11)<<11)|((g>>10)<<5)|(b>>11));//ILI9341/NT35310/NT35510需要公式转换一下

}		 
/*******************************************************************************
* Function Name  : LCD_DisplayOn
* Description    : LCD开启显示
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void LCD_DisplayOn(void)
{					   
	LCD_WR_Order(0X2900);	//开启显示
	LCD_LED =1;
}	 

/*******************************************************************************
* Function Name  : LCD_DisplayOff
* Description    : LCD关闭显示
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void LCD_DisplayOff(void)
{	   
	LCD_WR_Order(0X28);	//关闭显示
	LCD_LED =0;
}   
/*****************************************************************
 * 函  数：LCD_SetCursor
 * 功  能：设置显示区域，在此区域写点数据自动换行
 * 参  数：Xpos横坐标起点, Ypos纵坐标起点
 *         xEnd横坐标线束点，yEnd纵坐标线束点
 * 返回值：无
 *
 * 备  注： 魔女开发板团队编写   淘宝 https://demoboard.taobao.com
 * 分  享： 不断更新于Q群文件夹：262901124  最后修改_2020年09月014日
******************************************************************/
void LCD_SetCursor(u16 xStart, u16 yStart,u16 xEnd,u16 yEnd)
{
	    LCD_WR_Order(lcddev.setxcmd); //设置x坐标指令 setxcmd参数在LCD_Display_Dir()中被设置
		LCD_WR_DATA(xStart>>8);
		LCD_WR_DATA(xStart&0XFF); 			 
		LCD_WR_DATA(xEnd>>8);
		LCD_WR_DATA(xEnd&0XFF); 
	
		LCD_WR_Order(lcddev.setycmd); //设置y坐标指令 setycmd参数在LCD_Display_Dir()中被设置 
		LCD_WR_DATA(yStart>>8);
		LCD_WR_DATA(yStart&0XFF); 			 
		LCD_WR_DATA(yEnd>>8);
		LCD_WR_DATA(yEnd&0XFF); 
	
		LCD_WR_Order(lcddev.wramcmd);//开始写gram指令 wramcmd参数在LCD_Display_Dir()中被设置 
	
} 		 
     
/*****************************************************************
 * 函  数：LCD_DrawPoint
 * 功  能：画一个点
 * 参  数：x坐标，y坐标, 16位颜色值
 * 返回值：无
 * 
 * 备  注： 魔女开发板团队编写   淘宝 https://demoboard.taobao.com
 * 分  享： 不断更新于Q群文件夹：262901124  最后修改_2020年09月01日
******************************************************************/
void LCD_DrawPoint(u16 x,u16 y, u16 color)
{
	LCD_SetCursor(x,y,x+1, y+1);		//设置光标位置 
	LCD_WR_DATA(color); 
}	 

/******************************************************************
 * 函数名： LCD_Display_Dir
 * 功  能： 设置LCD显示方向
 * 参  数： u8 scanDir  四种显示方向，0-正竖屏，3-倒竖屏，5-正横屏, 6-倒横屏
 *****************************************************************/
void LCD_Display_Dir(u8 scanDir)
{
	u16 regval=0;
	
	if(scanDir==0||scanDir==3)	   // 竖屏
	{
		lcddev.dir=0;	      
		lcddev.width  = LCD_WIDTH;
		lcddev.height = LCD_HIGH ;
		lcddev.wramcmd= 0X2C;
		lcddev.setxcmd= 0X2A;
		lcddev.setycmd= 0X2B; 
	}
	if(scanDir==5 || scanDir==6)   // 横屏
	{	  				
		lcddev.dir=1;	      
		lcddev.width  = LCD_HIGH;
		lcddev.height = LCD_WIDTH;
		lcddev.wramcmd= 0X2C;
		lcddev.setxcmd= 0X2A;
		lcddev.setycmd= 0X2B; 
	} 	
	if(scanDir==0) regval|=(0<<7)|(0<<6)|(0<<5); // 从左到右,从上到下
	if(scanDir==3) regval|=(1<<7)|(1<<6)|(0<<5); // 从右到左,从下到上
	if(scanDir==5) regval|=(0<<7)|(1<<6)|(1<<5); // 从上到下,从右到左
	if(scanDir==6) regval|=(1<<7)|(0<<6)|(1<<5); // 从下到上,从左到右		   
	LCD_WR_Order (0X36);             // 读写方向，颜色模式
	LCD_WR_DATA(regval|0x08);	
}	 

/******************************************************************
 * 函数名： LCD_Clear
 * 功  能： 要清屏的填充色
 *****************************************************************/
void LCD_Clear(u16 color)
{
	u32 index=0;      
	u32 totalpoint=lcddev.width;
	totalpoint*=lcddev.height; 		//得到总点数
    LCD_SetCursor(0x00,0x0000,0,0);	//设置光标位置 
	LCD_WR_Order(lcddev.wramcmd);   //开始写gram指令 wramcmd参数在LCD_Display_Dir()中被设置  	  
	for(index=0;index<totalpoint;index++)
		LCD_WR_DATA(color);	
}  
/******************************************************************
 * 函数名： LCD_Color_Fill
 * 功  能： 在指定区域内填充指定颜色块
 * 参  数： (sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)  
 *          u16  color    颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void LCD_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color)
{  

	u32 CNT=(ex+1-sx)*(ey+1-sy);
	LCD_SetCursor(sx, sy, ex, ey);      			// 设置光标位置 
	
	LCD_RS_HIGH;             // RS高: 数据值， RS低: 寄存器地址值
	LCD_CS_LOW;              // 片选 
	while(CNT--)
	{
	    GPIOB->ODR = color;  // PB0~15作为数据线
	    LCD_WR_LOW;          // 写数据
	    LCD_WR_HIGH;         // 写数据 
	}
	LCD_CS_HIGH;             // 片选	
} 
/******************************************************************
 * 函数名： LCD_DrawLine
 * 功  能： 在指定位置画线
 * 参  数： x1,y1:起点坐标
 *			x2,y2:终点坐标 
 *          u16  color    颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2,u16 color)
{
	u16 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance; 
	int incx,incy,uRow,uCol; 
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1; 
	uRow=x1; 
	uCol=y1; 
	if(delta_x>0)incx=1; //设置单步方向 
	else if(delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;} 
	if(delta_y>0)incy=1; 
	else if(delta_y==0)incy=0;//水平线 
	else{incy=-1;delta_y=-delta_y;} 
	if( delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y; 
	for(t=0;t<=distance+1;t++ )//画线输出 
	{  
		LCD_DrawPoint(uRow,uCol,color);//画点 
		xerr+=delta_x ; 
		yerr+=delta_y ; 
		if(xerr>distance) 
		{ 
			xerr-=distance; 
			uRow+=incx; 
		} 
		if(yerr>distance) 
		{ 
			yerr-=distance; 
			uCol+=incy; 
		} 
	}  
}    
/******************************************************************
 * 函数名： LCD_DrawRectangle
 * 功  能： 在指定位置画一个指定大小的矩形	
 * 参  数： (x1,y1),(x2,y2):矩形的对角坐标
 *          u16  color    颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color)
{
	LCD_DrawLine(x1,y1,x2,y1,color);
	LCD_DrawLine(x1,y1,x1,y2,color);
	LCD_DrawLine(x1,y2,x2,y2,color);
	LCD_DrawLine(x2,y1,x2,y2,color);
}
/******************************************************************
 * 函数名： LCD_Draw_Circle
 * 功  能： 在指定位置画一个指定大小的圆	
 * 参  数： u16 x,y       中心点
 *          u8  r         半径
 *          u16  color    颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void LCD_Draw_Circle(u16 x0,u16 y0,u8 r,u16 color)
{
	int a,b;
	int di;
	a=0;b=r;	  
	di=3-(r<<1);             //判断下个点位置的标志
	while(a<=b)
	{
		LCD_DrawPoint(x0+a,y0-b,color);             //5
 		LCD_DrawPoint(x0+b,y0-a,color);             //0           
		LCD_DrawPoint(x0+b,y0+a,color);             //4               
		LCD_DrawPoint(x0+a,y0+b,color);             //6 
		LCD_DrawPoint(x0-a,y0+b,color);             //1       
 		LCD_DrawPoint(x0-b,y0+a,color);             
		LCD_DrawPoint(x0-a,y0-b,color);             //2             
  		LCD_DrawPoint(x0-b,y0-a,color);             //7     	         
		a++;
		//使用Bresenham算法画圆     
		if(di<0)di +=4*a+6;	  
		else
		{
			di+=10+4*(a-b);   
			b--;
		} 						    
	}
} 

/******************************************************************
 * 函数名： drawAscii
 * 功  能： 在指定位置显示一个字符	
 * 参  数： u16 x,y     起始坐标
 *          u8  num     要显示的字符:" "--->"~"
 *          u8  size    字体大小 12/16/24/32
 *          u32 fColor  字体颜色
 *          u32 bColor  背景颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void drawAscii(u16 x,u16 y,u8 num,u8 size,u32 fColor, u32 bColor)
{  				 
    u8 temp;
	u16 y0=y;
    
	u8 csize=(size/8+((size%8)?1:0))*(size/2);		   // 得到字体一个字符对应点阵集所占的字节数	
 	num=num-' ';                                       // 得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）
	for(u8 t=0;t<csize;t++)
	{   
		if(size==12)         temp=asc2_1206[num][t];   // 调用1206字体
		else if(size==16)    temp=asc2_1608[num][t];   // 调用1608字体
		else if(size==24)    temp=asc2_2412[num][t];   // 调用2412字体
		else if(size==32)    temp=asc2_3216[num][t];   // 调用3216字体
		else return;								   // 没有的字库
		
		for(u8 t1=0; t1<8; t1++)
		{			    
            if(temp&0x80)   LCD_DrawPoint (x, y, fColor);  // 字体 画点 
            else            LCD_DrawPoint (x, y, bColor);  // 背景 画点
            temp<<=1;
			y++;
			if(y>=lcddev.height)    return;		       // 超出屏幕高度(底)
			if((y-y0)==size)
			{
				y=y0;
				x++;
				if(x>=lcddev.width) return;              // 超出屏幕宽度(宽)
				break;
			}
		}  	 
	}  	    	 	  
} 
// code 字符指针开始
// 从字库中查找出字模
// code 字符串的开始地址,GBK码
// mat  数据存放地址 (size/8+((size%8)?1:0))*(size) bytes大小	
// size:字体大小
void GetGBK(u8* code, u8* mat, u8 size)
{		    
	u8 qh,ql;						  
	u32 foffset; 
	u8 csize=(size/8+((size%8)?1:0))*(size);    // 计算汉字点阵大小，单位字节数	 
	
    qh=*code;
	ql=*(++code);    
	
	if(qh<0x81||ql<0x40||ql==0xff||qh==0xff)    // 非常用汉字，将用填充显示整个位置
	{   	          
	    for(u8 i=0; i<csize; i++) *mat++=0x00;  // 填充满格
	    return;                                 // 结束访问
	}     

	if(ql<0x7f)ql-=0x40;                        // 计算要提取的汉字在字库中的偏移位置
	else ql-=0x41;
	qh-=0x81;   
	foffset=((unsigned long)190*qh+ql)*csize;	// 得到汉字在字库中的偏移位置  		

	switch(size)
	{                                                                                 // 按字体的不同，在不同字库读取字体点阵
		case 12:
			SPI_Flash_Read( mat,foffset + GBK_STORAGE_ADDR,            csize);    // 12号字体           
			break;  
		case 16:
            SPI_Flash_Read( mat,foffset + GBK_STORAGE_ADDR+0x0008c460,  csize);    // 12号字体
			break;
		case 24:
            SPI_Flash_Read( mat,foffset + GBK_STORAGE_ADDR+0x001474E0, csize);    // 24号字体
			break;
		case 32:
            SPI_Flash_Read( mat,foffset + GBK_STORAGE_ADDR+0x002EC200, csize);    // 32号字体
			break;			
	}     												    
} 

/******************************************************************
 * 函数名： drawGBK
 * 功  能： 在指定位置显示一个字符	
 * 参  数： u16 x,y     起始坐标
 *          u8  num     要显示的字符:" "--->"~"
 *          u8  size    字体大小 12/16/24/32
 *          u32 fColor  字体颜色
 *          u32 bColor  背景颜色
 * 备  注： 参考原子哥和野火大神的代码后修改  资料存放Q群：262901124 
 *****************************************************************/
void drawGBK( u16 x, u16 y, u8* font, u8 size, u32 fColor, u32 bColor)
{	
    u8 temp;
	u16 y0=y;
	u8 GBK[128];   
	u8 csize=(size/8+((size%8)?1:0))*(size);	// 得到字体一个字符对应点阵集所占的字节数	 
	GetGBK(font, GBK, size);	                // 得到相应大小的点阵数据
	
	//spiInit();                                  // 防止SPI参数被其它设备修改了	
	for(u8 t=0; t<csize; t++)
	{   												   
		temp = GBK[t];			                // 得到GBK点阵数据                          
		for(u8 t1=0; t1<8; t1++)
		{
			if(temp&0x80)   LCD_DrawPoint (x, y, fColor);	
            else            LCD_DrawPoint (x, y, bColor);            
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		} 
	}   
}
/******************************************************************************
 * 函  数： LCD_String
 * 功  能： 在LCD上显示字符串(支持英文、汉字)
 * 描  述： 英文：字模数据保存在font.h，编译后和代码一起保存在芯片内部Flash
 *          汉字：字模保存在外部Flash中，本函数字库在W25Q128中
 *                魔女开发板中W25Q128已烧录宋体4种字号大小字模数据
 * 参  数： u16   x      字体左上角X坐标
 *          u16   y      字体左上角y坐标
 *          char* pFont  要显示的字符串数据
 *          u8    size   字号大小：12 16 24 32
 *          u32   fColor 字体颜色
 *          u32   bColor 背景颜色
 * 返回值:  无
 * 备  注： 魔女开发板-老周   资料更新Q群：262901124    最后修改_2020年05月1８日
 ******************************************************************************/  
void LCD_String(u16 x, u16 y, char* pFont, u8 size, u32 fColor, u32 bColor)
{
    
    u16 xStart = x;    

    if( size!=12 && size!=16 && size!=24 && size!=32 )       // 字体大小控制
        size=24;    
    
    while(*pFont != 0)                // 连续读取字符串数据，直到'\0'时停止
    {    
        if(x>(lcddev.width-size))       // 行位置判断，如果到了行末，就把光标换行
        {
            x=xStart;
            y=y+size;
        }
        if(y>(lcddev.height - size))    // 列位置判断，如果到了列末，就返回，不再输出
            return;        
        
        if(*pFont < 127)              // ASCII字符
        {
            drawAscii (x, y, *pFont, size, fColor, bColor);            
            pFont++;
            x+=size/2;            
        }
        else                          // 汉字显示
        {     
            // 重要: 如果用的不是魔女开发板的字库, 就要修改或注释下面这一行, 这样就不影响ASCII英文字符的输出            
            drawGBK(x, y, (u8*)pFont, size, fColor, bColor);     
            pFont = pFont+2;          // 下一个要显示的数据在内存中的位置              
            x=x+size;                 // 下一个要显示的数据在屏幕上的X位置    
        }
    }      
}

/******************************************************************
 * 函数名： LCD_Image
 * 功  能： 在指定区域内填充指定图片数据
 *          图片数据在font.h文件中.只适合少量图片保存
 *          Image2Lcd转换：水平扫描，16位真彩色
 * 参  数： u16 x,y     左上角起始坐标
 *          u16 width   图片宽度
 *          u16 height  图片高度
 *          u8* image   数据缓存地址
 * 备  注： 魔女开发板团队  资料存放Q群：262901124        最后修改_2020年09月2日
 *****************************************************************/
void LCD_Image(u16 x, u16 y, u16 width, u16 height, const u8 *image) 
{  	
	u16 colorData=0;
	u32 cnt=0;
	
 	for(u16 i=0; i<height; i++)                // 一行一行地显示
	{
 		LCD_SetCursor(x, y+i, x+width, y+height);  // 重新设置光标位置 
		for(u16 j=0; j<width; j++)             // 一行中，从左到事，逐个像素处理
		{		    
		    colorData=(image[cnt*2+1]<<8) | image[cnt*2]; 
		    LCD_WR_DATA(colorData);             // 写入16位颜色数据 
			cnt++;	
		}
	}	  
} 
/*******************************************************************************
* Function Name  : LCD_Init
* Description    : 初始化lcd,该初始化函数只可以初始化各种ili9341的屏幕，其他的已经删除，若需要请参考正点原子的修改
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void LCD_Init(void)
{ 
 	GPIO_InitTypeDef GPIO_InitStructure;
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE); //使能PORTB,C时钟和AFIO时钟
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);//开启SWD，失能JTAG
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10|GPIO_Pin_9|GPIO_Pin_8|GPIO_Pin_7|GPIO_Pin_6;	   ///PORTC6~10复用推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure); //GPIOC	

	GPIO_SetBits(GPIOC,GPIO_Pin_10|GPIO_Pin_9|GPIO_Pin_8|GPIO_Pin_7|GPIO_Pin_6);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;	//  PORTB推挽输出
	GPIO_Init(GPIOB, &GPIO_InitStructure); //GPIOB
 
	GPIO_SetBits(GPIOB,GPIO_Pin_All);

	delay_ms(50); 				// delay 50 ms 
	LCD_WriteReg(0x0000,0x0001);
	
	delay_ms(50); 				// delay 50 ms 
  	lcddev.id = LCD_ReadReg(0x0000);   
	if(lcddev.id<0XFF||lcddev.id==0XFFFF||lcddev.id==0X9300)//读到ID不正确,新增lcddev.id==0X9300判断，因为9341在未被复位的情况下会被读成9300
	{	
 		//尝试9341 ID的读取		
		LCD_WR_Order(0XD3);				   
		LCD_RD_DATA(); 				//dummy read 	
 		LCD_RD_DATA();   	    	//读到0X00
  		lcddev.id=LCD_RD_DATA();   	//读取93								   
 		lcddev.id<<=8;
		lcddev.id|=LCD_RD_DATA();  	//读取41   	
	}
	if(lcddev.id !=0X9341)	// 9341初始化失败
		return;
	
	LCD_WR_Order(0xCF);  
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0xC1); 
	LCD_WR_DATA(0X30); 
	LCD_WR_Order(0xED);  
	LCD_WR_DATA(0x64); 
	LCD_WR_DATA(0x03); 
	LCD_WR_DATA(0X12); 
	LCD_WR_DATA(0X81); 
	LCD_WR_Order(0xE8);  
	LCD_WR_DATA(0x85); 
	LCD_WR_DATA(0x10); 
	LCD_WR_DATA(0x7A); 
	LCD_WR_Order(0xCB);  
	LCD_WR_DATA(0x39); 
	LCD_WR_DATA(0x2C); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x34); 
	LCD_WR_DATA(0x02); 
	LCD_WR_Order(0xF7);  
	LCD_WR_DATA(0x20); 
	LCD_WR_Order(0xEA);  
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x00); 
	LCD_WR_Order(0xC0);    //Power control 
	LCD_WR_DATA(0x1B);   //VRH[5:0] 
	LCD_WR_Order(0xC1);    //Power control 
	LCD_WR_DATA(0x01);   //SAP[2:0];BT[3:0] 
	LCD_WR_Order(0xC5);    //VCM control 
	LCD_WR_DATA(0x30); 	 //3F
	LCD_WR_DATA(0x30); 	 //3C
	LCD_WR_Order(0xC7);    //VCM control2 
	LCD_WR_DATA(0XB7); 
	LCD_WR_Order(0x36);    // Memory Access Control 
	LCD_WR_DATA(0x48); 
	LCD_WR_Order(0x3A);   
	LCD_WR_DATA(0x55); 
	LCD_WR_Order(0xB1);   
	LCD_WR_DATA(0x00);   
	LCD_WR_DATA(0x1A); 
	LCD_WR_Order(0xB6);    // Display Function Control 
	LCD_WR_DATA(0x0A); 
	LCD_WR_DATA(0xA2); 
	LCD_WR_Order(0xF2);    // 3Gamma Function Disable 
	LCD_WR_DATA(0x00); 
	LCD_WR_Order(0x26);    //Gamma curve selected 
	LCD_WR_DATA(0x01); 
	LCD_WR_Order(0xE0);    //Set Gamma 
	LCD_WR_DATA(0x0F); 
	LCD_WR_DATA(0x2A); 
	LCD_WR_DATA(0x28); 
	LCD_WR_DATA(0x08); 
	LCD_WR_DATA(0x0E); 
	LCD_WR_DATA(0x08); 
	LCD_WR_DATA(0x54); 
	LCD_WR_DATA(0XA9); 
	LCD_WR_DATA(0x43); 
	LCD_WR_DATA(0x0A); 
	LCD_WR_DATA(0x0F); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x00); 		 
	LCD_WR_Order(0XE1);    //Set Gamma 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x15); 
	LCD_WR_DATA(0x17); 
	LCD_WR_DATA(0x07); 
	LCD_WR_DATA(0x11); 
	LCD_WR_DATA(0x06); 
	LCD_WR_DATA(0x2B); 
	LCD_WR_DATA(0x56); 
	LCD_WR_DATA(0x3C); 
	LCD_WR_DATA(0x05); 
	LCD_WR_DATA(0x10); 
	LCD_WR_DATA(0x0F); 
	LCD_WR_DATA(0x3F); 
	LCD_WR_DATA(0x3F); 
	LCD_WR_DATA(0x0F); 
	LCD_WR_Order(0x2B); 
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0x3f);
	LCD_WR_Order(0x2A); 
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0xef);	 
	LCD_WR_Order(0x11); //Exit Sleep
	delay_ms(120);
	LCD_WR_Order(0x29); //display on
		
	LCD_Display_Dir(LCD_DIR);	//设置显示方向,在lcd.h文件中设置宏
	LCD_Color_Fill(0,0,lcddev.width-1,lcddev.height-1,BLACK);
	LCD_LED=1;					//点亮背光
}
