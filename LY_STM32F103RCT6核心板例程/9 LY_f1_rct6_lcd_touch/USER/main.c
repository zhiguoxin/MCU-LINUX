/*------------------------------------------------------------------------------------
 硬件平台:
 			主控器: STM32F103RCT6 64K RAM 256K ROM
			屏幕器: 2.8寸 触摸显示屏 ILI9341 LCD彩色TFT液晶模块
			分辨率: 240X320 16位色(RGB565)
 软件平台:
 			开发环境: RealView MDK-ARM uVision5.27
			C编译器 : ARMCC
			ASM编译器:ARMASM
			连接器:   ARMLINK
			底层驱动: 各个外设驱动程序
	
	ROM Size = Code + RO-data + RW-data
	RAM Size = RW-data + ZI-data
	Program Size: Code=10656 RO-data=6440 RW-data=60 ZI-data=1052   
	
	=>代码占用 FLASH 大小为17096字节(10656+6440),也就约等于16.69K
			   SRAM  大小为1112字节(60+1052),也就约等于1.08K
	
	uart_init 函数，不能去掉，因为在 LCD_Init 函数里面调用了 printf，所以一旦你去掉这个初始化，就会死机了	
-------------------------------------------------------------------------------------*/   
#include "sys.h"
#include "led.h"
#include "key.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"
#include "lcd.h"

/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT6_2.8寸TFT显示屏菜单显示"
#define EXAMPLE_DATE	"2020-12-29"

static void PrintfLogo(void);
static void PrintfHelp(void);
//菜单定义
void DisplayMenu(u8 t)
{	
	switch(t)
	{
		case 1:	
			LCD_Clear(WHITE);			
			LCD_ShowString(20,20,200,24,24,(u8*)"Select:");
			LCD_ShowString(20,50,200,24,24,(u8*)"-->");
			LCD_ShowString(60,50,200,24,24,(u8*)"light led0");
			LCD_ShowString(60,80,200,24,24,(u8*)"light led1");
			LCD_ShowString(60,110,200,24,24,(u8*)"light two led");
			break;		
		case 2:
			LCD_Clear(WHITE);			
			LCD_ShowString(20,20,200,24,24,(u8*)"Select:");
			LCD_ShowString(60,50,200,24,24,(u8*)"light led0");
			LCD_ShowString(20,80,200,24,24,(u8*)"-->");
			LCD_ShowString(60,80,200,24,24,(u8*)"light led1");
			LCD_ShowString(60,110,200,24,24,(u8*)"light two led");		
			break;
		case 3:
			LCD_Clear(WHITE);			
			LCD_ShowString(20,20,200,24,24,(u8*)"Select:");
			LCD_ShowString(60,50,200,24,24,(u8*)"light led0");
			LCD_ShowString(60,80,200,24,24,(u8*)"light led1");
			LCD_ShowString(20,110,200,24,24,(u8*)"-->");
			LCD_ShowString(60,110,200,24,24,(u8*)"light two led");
			break;
	}
}

void ExecuteFunction(u8 cur)
{
	static u8 s_LED0=0,s_LED1=0;//静态记住上一个led的状态
	switch(cur)
	{
		case 1:					//菜单1，功能是点亮LED0
			s_LED0=!s_LED0;     //选择一次，切换一次
			if(s_LED0)           //为1，则点亮
				LED0=0;
			else LED0=1;
			break;
		case 2:					//菜单2，功能是点亮LED1
			s_LED1=!s_LED1;
			if(s_LED1) 
				LED1=0;
			else LED1=1;
			break;
		case 3:					//菜单3，功能是点亮LED0和LED1
			s_LED0=!s_LED0;
			s_LED1=!s_LED1;
			if(s_LED0) 
				LED0=0;
			else LED0=1;
			if(s_LED1) 
				LED1=0;
			else LED1=1;
			break;	
	}
}
int main(void)
{	
		u8 cur=1;
		u8 flag,t;
		delay_init();	   		//延时函数初始化
		uart_init(115200);		//初始化串口，波特率为115200
		LED_Init();		  		//初始化与LED连接的硬件接口
		LCD_Init();				//初始化与LCD连接的硬件接口
		KEY_Init();				//初始化与按键连接的硬件接口
		PrintfLogo();			//打印例程名称和版本等信息
		PrintfHelp();			//打印操作提示
		//LCD_Display_Dir(1);	//设置为横屏
		DisplayMenu(1);
		while(1)
		{
			t=KEY_Scan(0);
			switch(t)
			{
				case 1:    			//KEY0
						flag=0;
						break;
				case 2: 			//KEY1
						flag=0;
						ExecuteFunction(cur);
						break;
				case 3: flag=1;		//WK_UP
					   if(cur==3)					   
						   cur=1;
					   else
						   cur++;
					   break;		
			}	
			if(flag)
			{
				DisplayMenu(cur);
				flag=0;
			}
		} 										    	
}

/*
**************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参：无
*	返 回 值: 无
**************************************************************
*/
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("LCD菜单显示\r\n");
	printf("     下移 WKUP\r\n");
	printf("     确认 KEY1\r\n");		
}
/*
***************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
*	形    参：无
*	返 回 值: 无
***************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \n\r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

