//FLSAH 存储器又称闪存，它与 EEPROM 都是掉电后数据不丢失的存储器，但 FLASH
//存储器容量普遍大于 EEPROM，现在基本取代了它的地位。我们生活中常用的 U 盘、SD
//卡、SSD 固态硬盘以及我们 STM32 芯片内部用于存储程序的设备，都是 FLASH 类型的存
//储器。在存储控制上，最主要的区别是 FLASH 芯片只能一大片一大片地擦写，而在“I2C
//章节”中我们了解到 EEPROM可以单个字节擦写。

#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"	 
#include "adc.h"
#include "lcd.h"
#include "tsensor.h"
#include "dma.h"
#include "myiic.h"
#include "24cxx.h"
#include "flash.h" 
#include "spi.h"

/*定义项目名和项目日期 */
#define EXAMPLE_NAME	"STM32F103RCT6 SPI TEST"
#define EXAMPLE_DATE	"2020-12-30"

static void PrintfLogo(void);
static void PrintfHelp(void);
static void LcdPrintf(void);

//要写入到24c02的字符串数组
const u8 TEXT_Buffer[]={"STM32F103RCT6_SPI_TEST"};//这里放了22个字节，AT24C02一共有256个字节。
#define SIZE sizeof(TEXT_Buffer)	

int main(void)
{	
	u8 key;
	u8 datatemp[SIZE];
	u32 FLASH_SIZE;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2	
	delay_init();		//延时函数初始化 
	uart_init(115200);	//初始化串口，波特率为115200
 	LED_Init();			//初始化与LED连接的硬件接口
	KEY_Init();			//初始化与按键连接的硬件接口
	LCD_Init();			//初始化与LCD连接的硬件接口
	PrintfLogo();		//串口打印例程名称和版本等信息
	PrintfHelp();		//串口打印操作提示
	LcdPrintf();		//LCD打印操作提示
	SPI_Flash_Init();  	//SPI FLASH W25Q128初始化 	 
	while(SPI_Flash_ReadID()!=W25Q128)//检测不到W25Q128
	{
		LCD_ShowString(30,170,200,16,16,(u8*)"W25Q128 Check Failed!");
		delay_ms(500);
		LCD_ShowString(30,200,200,16,16,(u8*)"Please Check!      ");
		delay_ms(500);
	}
	
	LCD_ShowString(30,170,200,16,16,(u8*)"W25Q128 Ready!");

	FLASH_SIZE=16*1024*1024;	//FLASH 大小为16M字节
  	POINT_COLOR=BLUE;			//设置字体为蓝色	  
	while(1)
	{
		key=KEY_Scan(0);
		if(key==WKUP_PRES)	//WK_UP 按下,写入W25Q64
		{
			LCD_Fill(0,200,239,319,WHITE);//清除半屏    
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Write W25Q128....");
			//SPI_Flash_Write((u8*)TEXT_Buffer,FLASH_SIZE-100,SIZE);			//从倒数第100个地址处开始,写入SIZE长度的数据
			SPI_Flash_Write((u8*)TEXT_Buffer,0,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"W25Q64 Write Finished!");	//提示传送完成
		}
		if(key==KEY0_PRES)	//KEY0 按下,读取字符串并显示
		{
 			LCD_ShowString(30,200,200,16,16,(u8*)"Start Read W25Q128.... ");
			//SPI_Flash_Read(datatemp,FLASH_SIZE-100,SIZE);					//从倒数第100个地址处开始,读出SIZE个字节
			SPI_Flash_Read(datatemp,0,SIZE);
			LCD_ShowString(30,200,200,16,16,(u8*)"The Data Readed Is:  ");	//提示传送完成
			LCD_ShowString(30,200,200,16,16,datatemp);						//显示读到的字符串
		}		   
	}
 	
}

//打印操作提示
static void PrintfHelp(void)
{
	printf("操作提示:\r\n");
	printf("WK_UP:Write  KEY0:Read\r\n");	
}

//功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开串口调试助手软件可以观察结果
static void PrintfLogo(void)
{
	printf("\n*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印项目名称 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印项目日期 */
	/* 打印ST固件库版本，宏定义在 stm32f3xx.h 文件 */
	printf("* 固件库版本 : %d.%d.%d\r\n", __STM32F10X_STDPERIPH_VERSION_MAIN,
			__STM32F10X_STDPERIPH_VERSION_SUB1,__STM32F10X_STDPERIPH_VERSION_SUB2);
	/* 打印 CMSIS 版本. 宏定义在 core_cm3.h 文件 */
	printf("* CMSIS版本  : %X.%02X\r\n", __CM3_CMSIS_VERSION_MAIN, __CM3_CMSIS_VERSION_SUB);

	printf(" \r");/* 打印一行空格 */
	printf("* QQ   : 2515361695 \r\n");
	printf("* Official Accounts : 果果小师弟\r\n");
	printf("* Copyright@ www.liuyao-blog.cn 小师弟\r\n");
	printf("*************************************************************\n\r");
}

//功能说明: LCD显示屏显示提示信息
static void LcdPrintf(void)
{
	POINT_COLOR=RED;	  
	LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
	LCD_ShowString(30,70,200,16,16,(u8*)"SPI TEST"); 					 
	LCD_ShowString(30,140,200,16,16,(u8*)"WK_UP:Write  KEY0:Read");
}

