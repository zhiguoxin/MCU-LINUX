#include "sys.h"
#include "led.h"
#include "delay.h"
#include "usart.h"
#include "rtc.h"
#include "lcd.h"

int main(void)
{	
	u8 x=0;
	u8 lcd_id[12];			//存放LCD ID字符串	
	delay_init();	    	//延时函数初始化	  
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口
 	LCD_Init();
	LCD_Clear(GRAY);//屏幕的背景色
	sprintf((char*)lcd_id,"LCD ID:%04X",lcddev.id);//将LCD ID打印到lcd_id数组。				 	
  	while(1) 
	{			
		POINT_COLOR=RED;	  
		LCD_ShowString(30,40,200,24,24,(u8*)"LY_STM32 ^_^");	
		LCD_ShowString(30,70,200,16,16,(u8*)"TFTLCD TEST");
		LCD_ShowString(30,90,200,16,16,(u8*)"GOUGUO");
		LCD_ShowString(30,110,200,16,16,lcd_id);//显示LCD ID 
	} 										    	
}


